#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl

#from catmaphelpers.catmap_plotter import _make_plots, _plot_model, _plot_CV, post_process_CV, _compute_tafel_prate, plot_tafel_screening, _get_unit_cell_area, _plot_pH, _plot_map, _plot_pH_bar_coverage
#from catmaphelpers.catmap_output_handler import _get_data, _sum_catmap_data, _get_rate_control, read_catmap_wdir
#from catmaphelpers.extra_tools import make_FEDs

#sys.path.append("../.")
#from post_catmap import read_catmap_wdir
home=os.getcwd()
facet='100'

if __name__ == "__main__":
    data_in = pkl.load(open(facet+'/'+facet+'_py3.pkl','rb'),encoding='latin1')
    data={'rate':{},'cov':{},'taf':{}}
    pots,phs=[],[]
    nsteps=1
    irate=[1,0]
    iads=[3,4]

    for dat in data_in['rate_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['rate']:
            data['rate'][pot] = {}
        data['rate'][pot][ph] = dat[1]
        if pot not in pots:
            pots.append(np.around(pot,2))
        if ph not in phs:
            phs.append(ph)

    for dat in data_in['coverage_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['cov']:
            data['cov'][pot] = {}
        data['cov'][pot][ph] = dat[1]

    pots=sorted(np.unique(pots))
    for pH in data['rate'][pot]:
        if pH not in data['taf']:
            data['taf'][pH]={}
        for ipot,pot in enumerate(pots):
            if ipot==0: continue
            print(pH,pot)
            print(data['rate'][pot].keys())
            print(data['rate'][pots[ipot-1]][pH][1])
            data['taf'][pH][(pot+pots[ipot-1])/2]=\
            -(pot-pots[ipot-1])/(np.log10(data['rate'][pot][pH][1])-np.log10(data['rate'][pots[ipot-1]][pH][1]))

    X=np.array(pots)
    Y=np.array(sorted(phs))
    if nsteps == 1:cols=1
    else: cols=2
    cols=1

    fig,ax=plt.subplots(1,cols)


    for col in range(cols):
      for istep in range(nsteps):
        XY=np.ones((len(X),len(Y)))*0.5
        rate=np.ones((len(X),len(Y)))*0.5
        for ix,x in enumerate(X):
            for iy,y in enumerate(Y):
                            try:
                                if col == 1:
                                    XY[ix][iy]=data['cov'][x][y][iads[istep]]
                                #    if data['cov'][x][y][iads[istep]] < 0:
                                else:
                                    if data['rate'][x][y][irate[istep]] < 1e-13:
                                        rate[ix][iy]=1e-13
                                    else:
                                        rate[ix][iy]=data['rate'][x][y][irate[istep]]#/np.sum(data[x][y][:nsteps])
                            except:
                                XY[ix][iy]=np.nan#data[x][y][istep]#/np.sum(data[x][y][:3])
                                rate[ix][iy]=1e-20#np.nan#data[x][y][istep]#/np.sum(data[x][y][:nsteps])
                                pass

        if col == 0:
         if nsteps == 1: plotax=ax


         b = plotax.imshow(rate.T,
                cmap=cm.jet,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-7,
                    vmax=1e07,#)
                    aspect=0.05)#, vmin=-abs(alldata[:,2]).max())

    plt.gca().set_visible(False)
    cax = plt.axes([0.05, 0.05, 0.05, 0.9])
    fig.colorbar(b, cax=cax,orientation='vertical',fraction=0.07,anchor=(1.50,1.50))#cbar_ax)
    plt.yticks(fontsize=20)
    plt.tick_params(axis='x',length=10, width=1.5)

    fig.subplots_adjust(wspace=0.2,hspace=0.01)
    fig.tight_layout()
    fig.savefig('Colorbar.pdf')


    plt.close()
