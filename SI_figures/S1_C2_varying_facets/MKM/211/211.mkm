from input_master import *

scaler = 'ThermodynamicScaler'
descriptor_names= ['voltage', 'pH']
descriptor_ranges= [[-1.8,-1.00],[3,14]]
temperature = 300.
resolution=[17,12]

gas_thermo_mode = 'frozen_gas'
adsorbate_thermo_mode = 'frozen_adsorbate'
electrochemical_thermo_mode = 'surface_charge_density'

# solver settings
decimal_precision = 100
tolerance = 1e-25
max_rootfinding_iterations = 200
max_bisections = 3

## Reaction network
rxn_expressions=[
'*_211 + ele_g + H2O_g <-> H2O-ele_211 <-> H_211 + OH_g; beta=0.36323579621153623',   #1
'2CO_211 <-> CO-CO_211 <-> OCCO_211; beta=0.2484462125887679',   #4
'OCCO_211 + ele_g + H2O_g <-> OCCO-H2O-ele_211 <-> OCCOH_211 + OH_g; beta=1.082157963228692',   #5
'OCCOH_211 + ele_g <-> CCO_211 + OH_g; beta=0.5 ',   #10
'CCO_211 + ele_g + H2O_g <-> HCCO_211 + OH_g; beta=0.5',   #14
'HCCO_211 + ele_g + H2O_g <-> H2CCO_211 + OH_g; beta=0.3985405767508773',   #17
'H2CCO_211 + 3H2O_g + 4ele_g -> C2H4_g + 4OH_g + 2*_211',
'H2O_g + ele_g + H*_211 -> H2_g + *_211 + OH_g; beta=0.5',     # Heyrovsky 2
'H*_211 + H*_211 -> H2_g + 2*_211',     # Tafel 3
'CO_g + *_211 <-> CO_211',
]

# Standard rate parameter settings - usually not changed
prefactor_list = [1e13]*len(rxn_expressions)  #Heine: prefactor for CO is 1e8
beta = 0.5

# Cu - CO2 reduction input file settings
input_file = '../../../../formation_energies_for_catmap.txt'
surface_names = ['Cu']

# Electrochemical settings - usually not changed
potential_reference_scale = 'SHE'

species_definitions = {}
# pressures
#species_definitions['H_g'] = {'pressure':1.0}
species_definitions['ele_g'] = {'pressure':1.0, 'composition':{}}
species_definitions['CO_g'] = {'pressure':1.0}
species_definitions['H2_g'] = {'pressure':0.0}
species_definitions['H2O_g'] = {'pressure':0.035}
species_definitions['CH4_g'] = {'pressure':0.0}
#species_definitions['CH2O_g'] = {'pressure':0.0}
species_definitions['O2_g'] = {'pressure':0.0}
species_definitions['CH3CH2OH_g'] = {'pressure':0.0}
species_definitions['C2H4_g'] = {'pressure':0.0}
species_definitions['OH_g'] = {'pressure':0.0}

#sites
#frequency_surface_names = ['211','111']
estimate_frequencies = 1



# interaction model

adsorbate_interaction_model = 'first_order' #use "single site" interaction model
#adsorbate_interaction_model = None #use "single site" interaction model

interaction_response_function = 'smooth_piecewise_linear' #use "smooth piecewise linear" interactions

interaction_fitting_mode = None
cross_interaction_mode = 'geometric_mean' #use geometric mean for cross parameters

if 1 == len(surface_names):
	numBeforePt = 0
	numAfterPt = 0
else:
	numBeforePt = len(surface_names)-surface_names.index('Pt')
	numAfterPt = len(surface_names)-numBeforePt-1
	transition_state_cross_interaction_mode = 'transition_state_scaling' #use TS scaling for TS interaction

# site interaction scaling:

eHCO=0.7274*interaction_strength
eCO=2.4670*interaction_strength
eH=0.0

species_definitions['211'] = {'site_names': ['211'], 'total':1.0}
species_definitions['211']['interaction_response_parameters'] = {'cutoff':0.25,'smoothing':0.05}
species_definitions['dl'] = {'site_names': ['dl'], 'total':1.0}
species_definitions['CO_211'] =             {
		'self_interaction_parameter':[None]*numBeforePt+[eCO]+[None]*numAfterPt,
		'cross_interaction_parameters':{
		'H_211': [None]*numBeforePt+[eHCO]+[None]*numAfterPt,
} }

species_definitions['OH_211'] = {
                   'self_interaction_parameter':[None]*numBeforePt+[1.0330]+[None]*numAfterPt,
                   }

species_definitions['H_211'] = {
                   'cross_interaction_parameters':{
		    'CO_211': [None]*numBeforePt+[eHCO]+[None]*numAfterPt,
                            }
                   }

species_definitions['OCCO_211'] = {
                   'self_interaction_parameter':[None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
                   'cross_interaction_parameters':{
                   'CO_211': [None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
                   'H_211': [None]*numBeforePt+[1*eHCO]+[None]*numAfterPt,
                           },
                   'n_sites':2
                   }


for sp in ['H2O-ele_211','CO_211','CO-H2O-ele_211','OC-H2O-ele_211','H_211','CHO_211','CHO-H2O-ele_211','COH_211','COH-ele_211','CHOH_211','CHOH-ele_211','C_211','CH_211','CH2_211','CH3_211','H3CCO_211','O_211','OH_211',]:
    species_definitions[sp] = species_definitions['CO_211'].copy()

for sp in ['COCO_211','CO-CO_211','OCCO_211','OCCOH_211','OCCHO_211','HOCCOH_211','CCO_211','CCOH_211','HCCO_211','HCCO-H2O-ele_211','OHCC-H2O-ele_211','COHC-H2O-ele_211','CCHO_211','CC_211','H2CCO_211','H2CCO-H2O-ele_211','OH2CC-H2O-ele_211','COH2C-H2O-ele_211','HCCOH_211','HCCHO_211','CCH_211','OHCCH2_211','H2CCHO_211','H2CCOH_211','HCCH_211','OCHCH3_211','H2CCH2O_211','H2CCH_211','H3CCH2O_211','OCCO-H2O-ele_211']:
    species_definitions[sp] = species_definitions['OCCO_211'].copy()

sigma_input = ['CH', 1]
Upzc = 0.00
species_definitions['CO_211']['sigma_params']=[-0.000527111148607784,-0.48474645648378445]
species_definitions['H_211']['sigma_params']=[0.02337267525012133,-0.1436253939978989]
species_definitions['CO-CO_211']['sigma_params']=[0.2484462125887679, -1.2148252849517671]
species_definitions['OCCO_211']['sigma_params']=[0.521490984590387,-2.060338339151477]
species_definitions['OCCOH_211']['sigma_params']=[0.13175443433055803,-1.0199329677567506]
species_definitions['CCO_211']['sigma_params']=[0.13143763341072018,-1.5543775336985717]
species_definitions['HCCO_211']['sigma_params']=[-0.11495514953714735,-1.2687903000796124]
species_definitions['H2CCO_211']['sigma_params']=[0.030232528168486327,-1.7403723380305447]

data_file = '211.pkl'
