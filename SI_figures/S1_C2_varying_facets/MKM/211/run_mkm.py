from catmap import ReactionModel
from catmap import analyze
from string import Template
import os,sys
import pickle

print("whats giong on")
include_rate_control = False
FED = False
#facets=['100','110','111','211']
facets=[sys.argv[1]]
#for s in [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]:
for facet in facets:
    model = ReactionModel(setup_file = facet+'.mkm')
    model.output_variables+=['production_rate', 'free_energy']#, 'selectivity', 'interacting_energy', 'interaction_matrix']
    if include_rate_control:
        model.output_variables += ['rate_control']
    model.run()


    vm = analyze.VectorMap(model)
    #figure this out in future!
    ma = analyze.MechanismAnalysis(model)
    ma.energy_type = 'free_energy'#interacting_energy'
    label_size = 10

    vm.plot_variable = 'rate'
    vm.log_scale = True
    vm.min = 1e-10
    vm.max = 1e+10
    fig = vm.plot(save=False)
    fig.savefig('rate'+facet+'.pdf')

    vm.plot_variable = 'production_rate'
    vm.log_scale = True
    vm.colorbar = True
    vm.min = 1e-10
    vm.max = 1e+10
    fig = vm.plot(save=False)
    fig.savefig('production_rate'+facet+'.pdf')

    #ma.kwarg_dict = {
    #        'C2_via_OCCHO-p':{'label_positions':None,'label_args':{'color':'k','rotation':45,'ha':'center','size':label_size}},
    #        'C2_via_OCCOH-p':{'label_positions':None,'label_args':{'color':'b','rotation':45,'ha':'center','size':label_size}},
    #        'C2_via_OCCO-SH':{'label_positions':None,'label_args':{'color':'g','rotation':45,'ha':'center','size':label_size}},
    #        'C2_via_OCCOH-p':{'label_positions':None,'label_args':{'color':'k','rotation':45,'ha':'center','size':label_size}},
    #        'CH4_via_CHO-ele':{'label_positions':None,'label_args':{'color':'b','rotation':45,'ha':'center','size':label_size}},
    #        'CH4_via_COH-ele':{'label_positions':None,'label_args':{'color':'k','rotation':45,'ha':'center','size':label_size}},
    #        'HER_Heyrovsky':{'label_positions':None,'label_args':{'color':'g','rotation':45,'ha':'center','size':label_size}},
    #        }


    vm = analyze.VectorMap(model)
    vm.log_scale = False
    vm.unique_only = False
    vm.plot_variable = 'coverage'
    vm.min = 0
    vm.max = 1
    fig = vm.plot(save=False)
    fig.savefig('coverage'+facet+'.pdf')

    vm = analyze.VectorMap(model)
    vm.log_scale = True
    vm.unique_only = False
    vm.plot_variable = 'coverage'
    vm.min = 1e-20
    vm.max = 1
    fig = vm.plot(save=False)
    fig.savefig('coverageLog'+facet+'.pdf')

    if FED:
#        ma.subplots_adjust_kwargs = {'top': 0.87, 'bottom':0.22}
#        ma.surface_colors = ['k','b','r','yellow','green','orange','cyan']
        ma.include_labels = True
        ma.label_args['size'] = 5
        ma.pressure_correction = False
        ma.coverage_correction = False
        fig = ma.plot(save='FED.png', plot_variants = [-0.7,-1.6])
        #fig = ma.plot(save='FED.png')#, -0.5, -0.7])

        ax = fig.add_subplot(111)
        delta = 0.05
        y0 = 0.92
        x0 = 0.85
        x1 = 0.9
        y1 = 0.9
        #for surf,col in zip(ma.surface_names, ma.surface_colors):
        #    ax.annotate('____',[x0,y0],xycoords='axes fraction',color=col)
        #    ax.annotate(surf,[x1,y1],xycoords='axes fraction',color=col)
        #    y0-= delta
        #    y1-= delta
        fig.savefig('FED.pdf')
    os.system('python ~/scripts/transfering_pickles.py %s.pkl'%facet)



    continue


    vm = analyze.VectorMap(model)
    vm.log_scale = False
    vm.plot_variable = 'my_selectivity'
    vm.include_labels = ['CH4_g', 'H2_g', 'CH3CH2OH_g', 'CH2O_g']#, 'CH4O2_g']
    vm.min = 0
    vm.max = 1
    fig = vm.plot(save=False)
    fig.savefig('my_selectivity'+str(s)+'.pdf')

    vm = analyze.VectorMap(model)
    vm.plot_variable = 'my_selectivity'
    vm.include_labels = ['CH4_g', 'H2_g', 'CH3CH2OH_g', 'CH2O_g']#, 'CH4O2_g']
    vm.min = 1e-10
    vm.max = 1
    vm.log_scale = True
    fig = vm.plot(save=False)
    fig.savefig('my_selectivityLog'+str(s)+'.pdf')

    if include_rate_control:
        mm = analyze.MatrixMap(model)
        mm.plot_variable = 'rate_control'
        mm.log_scale = False
        mm.min = -2
        mm.max = 2
        mm.plot(save='rate_control.pdf')

    os.system('cp C2_pH0.pkl C2_pH0_'+str(s)+'.pkl')
    sys.exit()
    from catmap import analyze
    ma = analyze.MechanismAnalysis(model)
    ma.energy_type = 'free_energy' #can also be free_energy/potential_energy
    ma.surface_colors = ['k','b','r','yellow','green','orange','cyan']
    ma.label_args['size'] = 8
    ma.include_labels = False #way too messy with labels
    ma.pressure_correction = True #assume all pressures are 1 bar (so that energies are the same as from DFT)
    fig = ma.plot(plot_variants=[-1.5], save='FED.png')
    f = open('potential_0p75.pickle', 'wb')
    print(ma.data_dict)  # contains [energies, barriers] for each rxn_mechanism defined
    pickle.dump(ma.data_dict, f)
    f.close()
