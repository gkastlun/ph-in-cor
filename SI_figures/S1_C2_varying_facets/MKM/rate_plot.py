#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl

#from catmaphelpers.catmap_plotter import _make_plots, _plot_model, _plot_CV, post_process_CV, _compute_tafel_prate, plot_tafel_screening, _get_unit_cell_area, _plot_pH, _plot_map, _plot_pH_bar_coverage
#from catmaphelpers.catmap_output_handler import _get_data, _sum_catmap_data, _get_rate_control, read_catmap_wdir
#from catmaphelpers.extra_tools import make_FEDs

#sys.path.append("../.")
#from post_catmap import read_catmap_wdir
home=os.getcwd()
facet=home.split('/')[-1]

if __name__ == "__main__":
    data_in = pkl.load(open(facet+'_py3.pkl','rb'),encoding='latin1')
    data={'rate':{},'cov':{},'taf':{}}
    pots,phs=[],[]
    nsteps=1
    irate=[1,0]
    iads=[3,4]

    for dat in data_in['rate_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['rate']:
            data['rate'][pot] = {}
        data['rate'][pot][ph] = dat[1]
        if pot not in pots:
            pots.append(np.around(pot,2))
        if ph not in phs:
            phs.append(ph)

    for dat in data_in['coverage_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['cov']:
            data['cov'][pot] = {}
        data['cov'][pot][ph] = dat[1]

    pots=sorted(np.unique(pots))
    for pH in data['rate'][pot]:
        if pH not in data['taf']:
            data['taf'][pH]={}
        for ipot,pot in enumerate(pots):
            if ipot==0: continue
            data['taf'][pH][(pot+pots[ipot-1])/2]=\
            -(pot-pots[ipot-1])/(np.log10(data['rate'][pot][pH][1])-np.log10(data['rate'][pots[ipot-1]][pH][1]))

    X=np.array(pots)
    Y=np.array(sorted(phs))
    if nsteps == 1:cols=1
    else: cols=2
    cols=1

    fig,ax=plt.subplots(nsteps,cols)


    for col in range(cols):
      for istep in range(nsteps):
        XY=np.ones((len(X),len(Y)))*0.5
        rate=np.ones((len(X),len(Y)))*0.5
        for ix,x in enumerate(X):
            for iy,y in enumerate(Y):
                            try:
                                if col == 1:
                                    XY[ix][iy]=data['cov'][x][y][iads[istep]]
                                #    if data['cov'][x][y][iads[istep]] < 0:
                                else:
                                    #if istep == 1:
                                    #   if data['rate'][x][y][irate[istep]] < 1e-13:
                                    #        print(x,y,data['rate'][x][y][irate[istep]])
                                    if data['rate'][x][y][irate[istep]] < 1e-13:
                                        rate[ix][iy]=1e-13
                                    else:
                                        rate[ix][iy]=data['rate'][x][y][irate[istep]]#/np.sum(data[x][y][:nsteps])
                            except:
                                XY[ix][iy]=np.nan#data[x][y][istep]#/np.sum(data[x][y][:3])
                                rate[ix][iy]=1e-20#np.nan#data[x][y][istep]#/np.sum(data[x][y][:nsteps])
                                pass

        if col == 0:
         if nsteps == 1: plotax=ax
         elif cols == 1: plotax= ax[istep]
         else: plotax= ax[istep][0]


         b = plotax.imshow(rate.T,
                cmap=cm.jet,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-7,
                    vmax=1e07,#)
                    aspect=0.05)#, vmin=-abs(alldata[:,2]).max())

        else:
         a = ax[istep][1].imshow(XY.T,
                interpolation='bicubic',
                cmap=cm.RdYlGn,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-10,
                    vmax=1,
                    aspect='auto')#, vmin=-abs(alldata[:,2]).max())

        if istep == nsteps-1:
            if nsteps==1: ax.set_xlabel('U$_{SHE}$ [V]',fontsize=20)
            else:
                if cols == 1:
                    ax[istep].set_xlabel('U$_{SHE}$ [V]')
                else:
                    for thisax in ax[istep]:
                        thisax.set_xlabel('U$_{SHE}$ [V]')
        else:
            if cols == 1:
                    ax[istep].set_xticks([])
            else:
                for thisax in ax[istep]:
                    thisax.set_xticks([])

        if cols > 1:
            ax[istep][0].set_ylabel('pH')
            ax[istep][1].set_yticks([])
        else:
            if nsteps == 1:
                ax.set_ylabel('pH',fontsize=20)
            else:
                ax[istep].set_ylabel('pH')

    if cols == 1:
        if nsteps == 1: barax=ax
        else: barax=ax[0]
    else: barax = ax[0][0]
#    axins1 = inset_axes(barax,
#                    width="100%",  # width = 50% of parent_bbox width
#                    height="5%",  # height : 5%
#                    loc='lower left',
#                     bbox_to_anchor=(0, 1.+nsteps/10., 1, 1),
#                  bbox_transform=barax.transAxes)
#    fig.colorbar(b, cax=axins1,orientation='horizontal',fraction=0.07,anchor=(1.0,1.0))#cbar_ax)

#    barax.annotate('Turnover frequency [s$^{-1}$]',(-1.7,13.5),color='k',bbox=dict(facecolor='w', edgecolor=None),fontsize=20)
#    fig.subplots_adjust(wspace=0.2,hspace=0.01)
    plt.tick_params(labelsize=20)
    plt.tight_layout()
    fig.savefig('TOF.pdf')


    plt.close()
    exit()
    colors = plt.cm.YlGnBu(np.linspace(0,1,len(phs)))
    for iph,pH in enumerate(sorted(phs)):
        if pH%1: continue
        pltdat=[]
        for pot in data['taf'][pH]:
            pltdat.append([pot,data['taf'][pH][pot]])
        pltdat=np.array(pltdat)
        plt.plot(pltdat[:,0],pltdat[:,1]*1000,'-',color=colors[iph])
    plt.ylabel('Tafel slope [mV/dec]')
    plt.xlabel('U$_{SHE}$ [V]')
    plt.savefig('Tafel_slopes.pdf')

    exit()
