import sys,os
sys.path.append('../../../tools_for_analysis')
from FED_tools import plot_FED_with_barrier,read_calculated_data
from intermediates_dict import *

facets= ['111','100','211','110']

def main():
    alldata=read_calculated_data(None,facets,1,pklfile='../../../calculated_energetics.pkl')

    for pH in [3,7,13]:
        plot_FED_with_barrier(ads_and_electron,
                ['110','111','211','100'],
                ['CO+CO','OCCO','OCCOH'],
                potentials=[3.0],
                pH=[13],
                ylim=[-0.5,2.0],
                view=False,
                outdir='.',
                outformat='pdf',
                annotate_intermediates=True,
                proton_donor={13:'base',7:'base',3:'base',0:'acid'},
                figsize=(12,6),
                title='',
                fontsize=25
                )



main()
