from catmap import ReactionModel
from catmap import analyze
from string import Template
import os,sys
import pickle

facet=os.getcwd().split('/')[-1]

model = ReactionModel(setup_file = facet+'.mkm')
model.output_variables+=['production_rate', 'free_energy']#, 'selectivity', 'interacting_energy', 'interaction_matrix']
model.run()

vm = analyze.VectorMap(model)
ma = analyze.MechanismAnalysis(model)
ma.energy_type = 'free_energy'
label_size = 10

# The plots below are created as a check for the finally produced plot
vm.plot_variable = 'production_rate'
vm.log_scale = True
vm.colorbar = True
vm.min = 1e-10
vm.max = 1e+10
fig = vm.plot(save=False)
fig.savefig('production_rate'+facet+'.pdf')

vm = analyze.VectorMap(model)
vm.log_scale = False
vm.unique_only = False
vm.plot_variable = 'coverage'
vm.min = 0
vm.max = 1
fig = vm.plot(save=False)
fig.savefig('coverage'+facet+'.pdf')

vm = analyze.VectorMap(model)
vm.log_scale = True
vm.unique_only = False
vm.plot_variable = 'coverage'
vm.min = 1e-20
vm.max = 1
fig = vm.plot(save=False)
fig.savefig('coverageLog'+facet+'.pdf')
