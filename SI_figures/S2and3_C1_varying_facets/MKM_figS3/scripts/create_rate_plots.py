#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.patheffects as path_effects
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl

home=os.getcwd()
facet=home.split('/')[-2]
if len(sys.argv) > 1:
    potscale=sys.argv[1]
else:
    potscale='SHE'

if __name__ == "__main__":
    data_in = pkl.load(open(facet+'_py3.pkl','rb'),encoding='latin1')
    data={'rate':{},'cov':{},'taf':{}}
    pots,phs=[],[]
    nsteps=1
    irate=[1,0]
    iads=[3,4]

    for dat in data_in['rate_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['rate']:
            data['rate'][pot] = {}
        data['rate'][pot][ph] = dat[1]
        if pot not in pots:
            pots.append(np.around(pot,3))
        if ph not in phs:
            phs.append(ph)

    pots=sorted(np.unique(pots))
    print(pots)
    for pH in data['rate'][pot]:
        if pH not in data['taf']:
            data['taf'][pH]={}
        for ipot,pot in enumerate(pots):
            if ipot==0: continue
            data['taf'][pH][(pot+pots[ipot-1])/2]=\
            -(pot-pots[ipot-1])/(np.log10(data['rate'][pot][pH][1])-np.log10(data['rate'][pots[ipot-1]][pH][1]))

    X=np.array(pots)
    Y=np.array(sorted(phs))

    fig,ax=plt.subplots(2,1,sharex=True,figsize=(7,9))

    rate=np.ones((len(X),len(Y)))*0.5
    for ix,x in enumerate(X):
            for iy,y in enumerate(Y):
                            try:
                                    if data['rate'][x][y][irate[0]] < 1e-13:
                                        rate[ix][iy]=1e-13
                                    else:
                                        rate[ix][iy]=data['rate'][x][y][irate[0]]#/np.sum(data[x][y][:nsteps])
                            except:
                                rate[ix][iy]=1e-13#np.nan#data[x][y][istep]#/np.sum(data[x][y][:nsteps])
                                pass

    b = ax[0].imshow(rate.T,
                cmap=cm.jet,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-13,
                    vmax=1e05,
                    aspect=0.05)#, vmin=-abs(alldata[:,2]).max())

    ax[0].set_xticks([])
    ax[0].set_ylabel('pH',fontsize=30)

    mech=os.getcwd()[-3:]
    if facet in ['111','110'] and mech == 'COH':
        text=ax[0].annotate(mech+' mechanism',(-1.8,3.2),color='w',fontweight='bold',ha='left',fontsize=35)
        text.set_path_effects([path_effects.Stroke(linewidth=3, foreground='k'),
                       path_effects.Normal()])
    else:
        text=ax[0].annotate(mech+' mechanism',(-1.8,3.2),color='k',fontweight='bold',ha='left',fontsize=35)
        text.set_path_effects([path_effects.Stroke(linewidth=3, foreground='w'),
                       path_effects.Normal()])
    #fig.tight_layout()
    #fig.savefig('TOF.pdf')


    #plt.close()
    #colors = plt.cm.YlGnBu(np.linspace(0,1,len(phs)))
    colors = plt.cm.coolwarm(np.linspace(0,1,len(phs)))
    for iph,pH in enumerate(sorted(phs)):
        if pH%1: continue
        pltdat=[]
        for pot in data['taf'][pH]:
            pltdat.append([pot,data['taf'][pH][pot]])
        pltdat=np.array(pltdat)
        if potscale == 'RHE':
            pltdat[:,0]+=0.059*pH
        ax[1].plot(pltdat[:,0],pltdat[:,1]*1000,'-',color=colors[iph])

    if facet == '211' and mech == 'COH':
        ax[1].set_ylim([0,150])
    ax[1].set_ylabel('Tafel slope [mV/dec]',fontsize=28)
    ax[1].set_xlabel('U$_{%s}$ [V]'%potscale,fontsize=28)
    #ax[1].set_yticks(fontsize=20)

    ax[1].set_ylim(bottom=0)
    ax[1].tick_params(axis='x',labelsize=25)
    ax[0].tick_params(axis='y',labelsize=25)
    ax[1].tick_params(axis='y',labelsize=25)
    if potscale != 'RHE':
        ax[1].set_xticks([-1.8,-1.4,-1.0])
    else:
        ax[1].set_xticks([-1.7,-1.2,-0.7,-0.2])
    fig.tight_layout()
    fig.savefig(f'TOF_and_Tafel_slopes_{potscale}.pdf')


    exit()
