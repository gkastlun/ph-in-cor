import sys
interaction_strength=float(sys.argv[1])
# General settings - usually unchanged
scaler = 'ThermodynamicScaler'
descriptor_names= ['voltage', 'pH']
descriptor_ranges= [[-1.8,-1.0],[3,14]]
temperature = 300.
resolution=[33,23]

gas_thermo_mode = 'frozen_gas'
adsorbate_thermo_mode = 'frozen_adsorbate'
electrochemical_thermo_mode = 'surface_charge_density'

# solver settings
decimal_precision = 100
tolerance = 1e-25
max_rootfinding_iterations = 200
max_bisections = 3

beta = 0.5

# Cu - CO2 reduction input file settings
input_file = '../../../../../formation_energies_for_catmap.txt'
surface_names = ['Cu']
potential_reference_scale = 'SHE'

species_definitions = {}
# pressures
species_definitions['ele_g'] = {'pressure':1.0, 'composition':{}}
species_definitions['CO_g'] = {'pressure':1.0}
species_definitions['H2_g'] = {'pressure':0.0}
species_definitions['H2O_g'] = {'pressure':0.035}
species_definitions['CH4_g'] = {'pressure':0.0}
species_definitions['O2_g'] = {'pressure':0.0}
species_definitions['CH3CH2OH_g'] = {'pressure':0.0}
species_definitions['C2H4_g'] = {'pressure':0.0}
species_definitions['OH_g'] = {'pressure':0.0}

# interaction model

adsorbate_interaction_model = 'first_order' #use "single site" interaction model
#adsorbate_interaction_model = None #use "single site" interaction model

interaction_response_function = 'smooth_piecewise_linear' #use "smooth piecewise linear" interactions

interaction_fitting_mode = None
cross_interaction_mode = 'geometric_mean' #use geometric mean for cross parameters

if 1 == len(surface_names):
	numBeforePt = 0
	numAfterPt = 0
else:
	numBeforePt = len(surface_names)-surface_names.index('Pt')
	numAfterPt = len(surface_names)-numBeforePt-1
	transition_state_cross_interaction_mode = 'transition_state_scaling' #use TS scaling for TS interaction

# site interaction scaling:

eHCO=0.7274*interaction_strength
eCO=2.4670*interaction_strength
eH=0.0

sigma_input = ['CH', 1]
Upzc = 0.00


