from numpy import array

try:
    import cPickle as pickle

except: #workaround for python3.X
    import _pickle as pickle


from mpmath import mpf 

parser = None

scaler = 'ThermodynamicScaler'

thermodynamics = 'ThermoCorrections'

solver = 'SteadyStateSolver'

mapper = 'MinResidMapper'

binary_data = pickle.load(open("111.pkl"))

locals().update(binary_data)

A_uc = None

Upzc = 0.0

adsorbate_interaction_model = 'first_order'

adsorbate_names = ('CHOH_111', 'CHO_111', 'CH_111', 'COH_111', 'CO_111', 'C_111', 'H_111')

adsorbate_thermo_mode = 'frozen_adsorbate'

analytical_jacobian = True

atomic_reservoir_list = [{'H': 'CH4_g', 'C': 'CO_g', 'O': 'H2O_g'}, {'H': 'CH4_g', 'C': 'CO_g', 'O': 'OH_g'}, {'H': 'H2O_g', 'C': 'CH4_g', 'O': 'OH_g'}, {'H': 'H2O_g', 'C': 'CO_g', 'O': 'OH_g'}, {'H': 'H2_g', 'C': 'CH4_g', 'O': 'CO_g'}, {'H': 'H2_g', 'C': 'CH4_g', 'O': 'H2O_g'}, {'H': 'H2_g', 'C': 'CH4_g', 'O': 'OH_g'}, {'H': 'H2_g', 'C': 'CO_g', 'O': 'H2O_g'}, {'H': 'H2_g', 'C': 'CO_g', 'O': 'OH_g'}]

atoms_dict = {}

beta = 0.5

coverage_headers = ['coverage', 'coadsorbate_coverage']

cross_interaction_mode = 'geometric_mean'

data_file = '111.pkl'

decimal_precision = 100

default_self_interaction_parameter = 0

descriptor_decimal_precision = 2

descriptor_names = ('voltage', 'pH')

descriptor_ranges = [[-1.8, -1.0], [3, 14]]

eCO = 0.0

eH = 0.0

eHCO = 0.0

echem_transition_state_names = ['echemTS-6-1.5_111']

electrochemical_thermo_mode = 'surface_charge_density'

elementary_rxns = ([['111', 'ele_g', 'H2O_g'], ['H2O-ele_111'], ['H_111', 'OH_g']], [['CO_111', 'ele_g', 'H2O_g'], ['OC-H2O-ele_111'], ['CHO_111', 'OH_g']], [['CO_111', 'ele_g', 'H2O_g'], ['CO-H2O-ele_111'], ['COH_111', 'OH_g']], [['CHO_111', 'ele_g', 'H2O_g'], ['CHO-H2O-ele_111'], ['CHOH_111', 'OH_g']], [['COH_111', 'ele_g'], ['COH-ele_111'], ['C_111', 'OH_g']], [['CHOH_111', 'ele_g'], ['CHOH-ele_111'], ['CH_111', 'OH_g']], [['C_111', 'ele_g', 'H2O_g'], ['echemTS-6-1.5_111'], ['CH_111', 'OH_g']], [['CH_111', 'H2O_g', 'H2O_g', 'H2O_g', 'ele_g', 'ele_g', 'ele_g'], ['CH4_g', 'OH_g', 'OH_g', 'OH_g', '111']], [['H2O_g', 'ele_g', 'H_111'], ['H2_g', '111', 'OH_g']], [['H_111', 'H_111'], ['H2_g', '111', '111']], [['CO_g', '111'], ['CO_111']])

estimate_frequencies = True

extrapolate_coverages = False

extrapolated_potential = 0.0

fixed_entropy_dict = {'H2_g': 0.00135, 'other': 0.002}

force_recalculation = True

force_recompilation = False

frequency_dict = {'H2_g': [], 'CO_g': [], 'H2O_g': [], 'C_111': [], 'echemTS-6-1.5_111': [], 'COH_111': [], '111': [], 'H_111': [], 'CHO_111': [], 'CO-H2O-ele_111': [], 'ele_g': [], 'H2O-ele_111': [], 'COH-ele_111': [], '*_g': [], 'CHOH-ele_111': [], 'CHOH_111': [], 'CH4_g': [0.0, 0.0, 0.0, 0.0, 0.0102906886, 0.01781652954, 0.15972884486, 0.16153901418000002, 0.16171259206000002, 0.18655902574000002, 0.18792285194000002, 0.3746802524, 0.38536769044, 0.38695468820000006, 0.38758700762000003], 'CHO-H2O-ele_111': [], '*_111': [], 'g': [], 'CH_111': [], 'CO_111': [], 'OH_g': [], 'OC-H2O-ele_111': []}

frequency_surface_names = []

frequency_unit_conversion = 0.0001239842

gas_names = ('CH4_g', 'CO_g', 'H2O_g', 'H2_g', 'OH_g', 'ele_g')

gas_pressures = [0.0, 1.0, 0.035, 0.0, 1.0, 1.0]

gas_thermo_mode = 'frozen_gas'

hbond_dict = {'OCHCH2': 0.0, 'C': 0.0, 'CO2': 0.0, 'CO': -0.1, 'OCH2O': 0.0, 'OH': -0.5, 'CHOH': -0.25, 'H': 0.0, 'OCHO': 0.0, 'O': 0.0, 'CHO': -0.1, 'OCH3': 0.0, 'CH2': 0.0, 'CH': 0.0, 'COHOH': -0.25, 'CH3': 0.0, 'CH2OH': -0.25, 'CH2O': 0.0, 'COH': -0.25, 'COOH': -0.25, 'OCHCHO': 0.0}

hindered_ads_params = {}

ideal_gas_params = {'H2_g': [2, 'linear', 0], 'CH3OH_g': [1, 'nonlinear', 0], 'C5H6O2_g': [1, 'nonlinear', 0], 'CO_g': [1, 'linear', 0], 'H2O_g': [2, 'nonlinear', 0], 'HCOOH_g': [1, 'nonlinear', 0], 'CH4O2_g': [2, 'nonlinear', 0], 'CH3CH2CHCH2_g': [1, 'nonlinear', 0], 'CH3CHCH2_g': [1, 'nonlinear', 0], 'O2_g': [2, 'linear', 1.0], 'N2_g': [2, 'linear', 0], 'CH3CH2OH_g': [1, 'nonlinear', 0], 'NH3_g': [3, 'nonlinear', 0], 'CH4_g': [12, 'nonlinear', 0], 'CH3CHO_g': [1, 'nonlinear', 0], 'C3H6_g': [1, 'nonlinear', 0], 'HCl_g': [1, 'linear', 0], 'CH2CH2_g': [4, 'nonlinear', 0], 'C2H2_g': [2, 'linear', 0], 'CH3CHCHCH3_g': [2, 'nonlinear', 0], 'CO2_g': [2, 'linear', 0], 'pe_g': [2, 'linear', 0], 'CH3CH3CCH2_g': [2, 'nonlinear', 0], 'C2H4_g': [4, 'nonlinear', 0], 'C5H4O2_g': [1, 'nonlinear', 0], 'C5H6O_g': [1, 'nonlinear', 0], 'CH2O_g': [2, 'nonlinear', 0], 'CH3COOH_g': [1, 'nonlinear', 0], 'C2H6_g': [6, 'nonlinear', 0], 'Cl2_g': [2, 'linear', 0]}

input_file = '../../catin_allfacets_4.400_freEn.txt'

input_overrides_fit = True

interaction_cross_term_names = ('CHOH_111&H_111', 'CHO_111&H_111', 'CH_111&H_111', 'COH_111&H_111', 'CO_111&H_111', 'C_111&H_111', 'H_111&H_111')

interaction_fitting_mode = None

interaction_parameters = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

interaction_response_parameters = {'slope': 1, 'cutoff': 0.25, 'smoothing': 0.05}

interaction_strength = 0.0

interaction_surface_names = None

interaction_transition_state_scaling_matrix = [[0.5, 0.5, 0, 0, 0, 0, 0], [0.5, 0, 0.5, 0, 0, 0, 0], [0, 0, 0, 0.5, 0.5, 0, 0], [0, 0, 0, 0.5, 0, 0.5, 0], [0, 0, 0, 0, 0, 0, 0.5], [0, 0.5, 0, 0, 0.5, 0, 0], [0, 0, 0.5, 0, 0, 0.5, 0]]

internally_constrain_coverages = True

max_bisections = 3

max_initial_guesses = 3

max_rootfinding_iterations = 200

model_name = '111'

numAfterPt = 0

numBeforePt = 0

numerical_delta_theta = None

numerical_representation = 'mpmath'

optimize_analytical_expressions = False

output_labels = {'rate': ([['111', 'ele_g', 'H2O_g'], ['H2O-ele_111'], ['H_111', 'OH_g']], [['CO_111', 'ele_g', 'H2O_g'], ['OC-H2O-ele_111'], ['CHO_111', 'OH_g']], [['CO_111', 'ele_g', 'H2O_g'], ['CO-H2O-ele_111'], ['COH_111', 'OH_g']], [['CHO_111', 'ele_g', 'H2O_g'], ['CHO-H2O-ele_111'], ['CHOH_111', 'OH_g']], [['COH_111', 'ele_g'], ['COH-ele_111'], ['C_111', 'OH_g']], [['CHOH_111', 'ele_g'], ['CHOH-ele_111'], ['CH_111', 'OH_g']], [['C_111', 'ele_g', 'H2O_g'], ['echemTS-6-1.5_111'], ['CH_111', 'OH_g']], [['CH_111', 'H2O_g', 'H2O_g', 'H2O_g', 'ele_g', 'ele_g', 'ele_g'], ['CH4_g', 'OH_g', 'OH_g', 'OH_g', '111']], [['H2O_g', 'ele_g', 'H_111'], ['H2_g', '111', 'OH_g']], [['H_111', 'H_111'], ['H2_g', '111', '111']], [['CO_g', '111'], ['CO_111']]), 'production_rate': ('CH4_g', 'CO_g', 'H2O_g', 'H2_g', 'OH_g', 'ele_g'), 'coverage': ('CHOH_111', 'CHO_111', 'CH_111', 'COH_111', 'CO_111', 'C_111', 'H_111'), 'free_energy': ('CH4_g', 'CO_g', 'H2O_g', 'H2_g', 'OH_g', 'ele_g', 'CHOH_111', 'CHO_111', 'CH_111', 'COH_111', 'CO_111', 'C_111', 'H_111', 'CHO-H2O-ele_111', 'CHOH-ele_111', 'CO-H2O-ele_111', 'COH-ele_111', 'H2O-ele_111', 'OC-H2O-ele_111', 'echemTS-6-1.5_111')}

output_variables = ['coverage', 'rate', 'production_rate', 'free_energy']

pH = 3.0

parameter_mode = 'formation_energy'

parameter_names = ('CHOH_111', 'CHO_111', 'CH_111', 'COH_111', 'CO_111', 'C_111', 'H_111', 'CHO-H2O-ele_111', 'CHOH-ele_111', 'CO-H2O-ele_111', 'COH-ele_111', 'H2O-ele_111', 'OC-H2O-ele_111', 'echemTS-6-1.5_111')

parse_headers = ['formation_energy', 'frequencies']

perturbation_size = 1e-14

potential_reference_scale = 'SHE'

prefactor_list = ['1e+13', '1e+13', '1e+13', '1e+13', '1e+13', '1e+13', '1e+13', '1e+13', '1e+13', '1e+13', '1e+13']

pressure = 1

pressure_mode = 'static'

required_headers = ['species_name', 'surface_name', 'site_name', 'formation_energy', 'frequencies', 'reference']

residual_threshold = 0.9

resolution = [17, 12]

rxn_expressions = ['*_111 + ele_g + H2O_g <-> H2O-ele_111 <-> H_111 + OH_g; beta=0.42359926075241655', 'CO_111 + ele_g + H2O_g <-> OC-H2O-ele_111 <-> CHO_111 + OH_g; beta=0.3304142072488211', 'CO_111 + ele_g + H2O_g <-> CO-H2O-ele_111 <-> COH_111 + OH_g; beta=0.7310279014764086', 'CHO_111 + ele_g + H2O_g <-> CHO-H2O-ele_111 <-> CHOH_111 + OH_g; beta=0.5362096965724192', 'COH_111 + ele_g <-> COH-ele_111 <-> C_111 + OH_g; beta=0.6446638651303487 ', 'CHOH_111 + ele_g <-> CHOH-ele_111 <-> CH_111 + OH_g; beta=0.4009788031956354 ', 'C_111 + ele_g + H2O_g <-> ^1.5eV_111 <-> CH_111 + OH_g; beta=0.5', 'CH_111 + 3H2O_g + 3ele_g -> CH4_g + 3OH_g + *_111', 'H2O_g + ele_g + H*_111 -> H2_g + *_111 + OH_g; beta=0.5', 'H*_111 + H*_111 -> H2_g + 2*_111', 'CO_g + *_111 <-> CO_111']

rxn_options_dict = {'beta': {0: '0.42359926075241655', 1: '0.3304142072488211', 2: '0.7310279014764086', 3: '0.5362096965724192', 4: '0.6446638651303487', 5: '0.4009788031956354', 6: '0.5', 8: '0.5'}, 'prefactor': {}}

search_directions = [[0, 0], [0, 1], [1, 0], [0, -1], [-1, 0], [-1, 1], [1, 1], [1, -1], [-1, -1]]

setup_file = '111.mkm'

shomate_params = {'HNO3_g:298-1200': [19.63229, 153.9599, -115.8378, 32.87955, -0.249114, -146.8818, 247.7049, -134.306], 'CH4_g:298-1300': [-0.703029, 108.4773, -42.52157, 5.862788, 0.678565, -76.84376, 158.7163, -74.8731], 'C5H4O2_g:100-1500': [-16.36694, 0.46958, -0.000331925, 8.50912e-08, 198590.9145, -1661.34999, -15.68938, 12461.34999], 'CH2CH2_g:298-1200': [-6.38788, 184.4019, -112.9718, 28.49593, 0.31554, 48.17332, 163.1568, 52.46694], 'HCN_g:298-1200': [32.69373, 22.59205, -4.369142, -0.407697, -0.282399, 123.4811, 233.2597, 135.1432], 'N2_g:100-500': [28.98641, 1.853978, -9.647459, 16.63537, 0.000117, -8.671914, 226.4168, 0.0], 'O2_g:2000-6000': [20.91111, 10.72071, -2.020498, 0.146449, 9.245722, 5.337651, 237.6185, 0.0], 'HCl_g:298-1200': [32.12392, -13.45805, 19.86852, -6.853936, -0.049672, -101.6206, 228.6866, -92.31201], 'CO2_g:1200-6000': [58.16639, 2.720075, -0.492289, 0.038844, -6.447293, -425.9186, 263.6125, -393.5224], 'H2_g:1000-2500': [18.563083, 12.257357, -2.859786, 0.268238, 1.97799, -1.147438, 156.288133, 0], 'Cl2_g:298-1000': [33.0506, 12.2294, -12.0651, 4.38533, -0.159494, -10.8348, 259.029, 0.0], 'HNO2_g:298-1200': [24.89974, 91.37563, -64.84614, 17.92007, -0.134737, -88.13596, 254.2671, -76.73498], 'NO_g:298-1200': [23.83491, 12.58878, -1.139011, -1.497459, 0.214194, 83.35783, 237.1219, 90.29114], 'NH3_g:298-1400': [19.99563, 49.77119, -15.37599, 1.921168, 0.189174, -53.30667, 203.8591, -45.89806], 'N2_g:500-2000': [19.50583, 19.88705, -8.598535, 1.369784, 0.527601, -4.935202, 212.39, 0.0], 'O2_g:100-700': [31.32234, -20.23531, 57.8664, -36.50624, -0.007374, -8.903471, 246.7945, 0.0], 'CO_g:1300-1600': [35.1507, 1.300095, -0.205921, 0.01355, -3.28278, -127.8375, 231.712, -110.5271], 'CH2O_g:298-1500': [5.193767, 93.23249, -44.85457, 7.882279, 0.551175, -119.3591, 202.4663, -115.8972], 'CH4_g:1300-1600': [85.81217, 11.26467, -2.114146, 0.13819, -26.42221, -153.5327, 224.4143, -74.8731], 'C5H6O2_g:100-1500': [-18.57626, 0.50733, -0.000335824, 8.28263e-08, 231619.2141, -6357.53523, 2.75692, 7057.53523], 'H2O_g:1700-6000': [41.96426, 8.622053, -1.49978, 0.098119, -11.15764, -272.1797, 219.7809, -241.8264], 'H2_g:2500-6000': [43.41356, -4.293079, 1.272428, -0.096876, -20.533862, -38.515158, 162.081354, 0], 'H2_g:298-1000': [33.066178, -11.363417, 11.432816, -2.772874, -0.158558, -9.980797, 172.707974, 0], 'NO3_g:298-1200': [11.22316, 166.3889, -148.4458, 47.40598, -0.176791, 61.00858, 221.7679, 71.128], 'C5H6O_g:50-1500': [1.26847, 0.35388, -0.000173962, 2.87427e-08, 48401.71207, -5691.94893, -85.38991, 7191.94893], 'CO_g:298-1300': [25.56759, 6.09613, 4.054656, -2.671201, 0.131021, -118.0089, 227.3665, -110.5271], 'O2_g:700-2000': [30.03235, 8.772972, -3.988133, 0.788313, -0.741599, -11.32468, 236.1663, 0.0], 'H2O_g:500-1700': [30.092, 6.832514, 6.793435, -2.53448, 0.082139, -250.881, 223.3967, -241.8264], 'CH3CH2OH_g:298-1500': [-4.7367880805894265, 271.96181550301463, -169.34946529216657, 43.73860153608533, 0.24643430702178773, -9.828280829187861, 203.3325622617075, 0.0], 'CH3CHO_g:298-1500': [4.8037386340099095, 185.92002366990326, -99.1084610902012, 20.61473919069016, 0.2770802548558646, -8.566600040790945, 220.00244674443837, 0.0], 'N2O_g:298-1400': [27.67988, 51.14898, -30.64454, 6.847911, -0.157906, 71.24934, 238.6164, 82.04824], 'HCOOH_g:298-1500': [3.802752304225226, 153.66217894746168, -84.64046773816926, 16.297377707561505, 0.2772064997263338, -6.16527, 212.9698972559699, 0], 'NO2_g:298-1200': [16.10857, 75.89525, -54.3874, 14.30777, 0.239423, 26.17464, 240.5386, 33.09502], 'CO2_g:298-1200': [24.99735, 55.18696, -33.69137, 7.948387, -0.136638, -403.6075, 228.2431, -393.5224], 'H2O_g:100-500': [36.303952, -24.11232, 63.64111, -38.9524, -0.01385, -10.23966, 237.39431, 0.0], 'CH3OH_g:298-1500': [-1.0845814245433651, 153.2463565199489, -79.53050359618254, 16.47130239316011, 0.5220334601533215, -4.897417024069, 199.1893745506112, 0.0]}

sigma_input = ['CH', 1]

site_names = ('111', 'g')

sp = 'H3CCH2O_111'

species_definitions = {'H2_g': {'n_sites': 0, 'pressure': 0.0, 'formation_energy': 0.0, 'name': 'H2', 'frequencies': [], 'formation_energy_source': 'GK calc', 'type': 'gas', 'composition': {'H': 2}, 'site': 'g'}, 'CO_g': {'n_sites': 0, 'pressure': 1.0, 'formation_energy': 0.0, 'name': 'CO', 'frequencies': [], 'formation_energy_source': 'GK calc', 'type': 'gas', 'composition': {'C': 1, 'O': 1}, 'site': 'g'}, 'H2O_g': {'n_sites': 0, 'pressure': 0.035, 'formation_energy': 0.0, 'name': 'H2O', 'frequencies': [], 'formation_energy_source': 'GK_calc', 'type': 'gas', 'composition': {'H': 2, 'O': 1}, 'site': 'g'}, '*_g': {'n_sites': 0, 'formation_energy': 0, 'name': 'g', 'frequencies': [], 'site': 'g', 'total': 0, 'type': 'site', 'composition': {}, 'site_names': ['gas']}, 'CHOH-ele_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [2.693537], 'name': 'CHOH-ele', 'frequencies': [], 'cross_interaction_parameters': {'H_111': [0.0]}, 'formation_energy_source': ['beta=(0.4009788031956354, -0.10674792253950358) CHOH_to_CH 386j'], 'type': 'transition_state', 'composition': {'H': 2, 'C': 1, 'O': 1}, 'site': '111'}, 'CHOH_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [1.628026], 'name': 'CHOH', 'frequencies': [], 'formation_energy_source': ['beta=(0.008084389953898109, 0.47326870510086266)'], 'cross_interaction_parameters': {'H_111': [0.0]}, 'site': '111', 'sigma_params': [0.008084389953898109, 0.47326870510086266], 'type': 'adsorbate', 'composition': {'H': 2, 'C': 1, 'O': 1}}, 'C_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [2.164376], 'name': 'C', 'frequencies': [], 'formation_energy_source': ['beta=(0.13549035397181203, 1.0431304981916867)'], 'cross_interaction_parameters': {'H_111': [0.0]}, 'site': '111', 'sigma_params': [0.13549035397181203, 1.0431304981916867], 'type': 'adsorbate', 'composition': {'C': 1}}, 'echemTS-6-1.5_111': {'n_sites': 1, 'formation_energy': [0.0], 'name': 'echemTS-6-1.5', 'frequencies': [], 'formation_energy_source': ['Generated by CatMAP'], 'site': '111', 'type': 'transition_state', 'composition': {'H': 1}}, 'COH_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [1.606226], 'name': 'COH', 'frequencies': [], 'formation_energy_source': ['beta=(-0.10038032431916426, 1.2188469852825932)'], 'cross_interaction_parameters': {'H_111': [0.0]}, 'site': '111', 'sigma_params': [-0.10038032431916426, 1.2188469852825932], 'type': 'adsorbate', 'composition': {'H': 1, 'C': 1, 'O': 1}}, '111': {'n_sites': 1, 'formation_energy': 0, 'name': '111', 'frequencies': [], 'site': '111', 'interaction_response_parameters': {'cutoff': 0.25, 'smoothing': 0.05}, 'total': 1.0, 'type': 'site', 'composition': {}, 'site_names': ['111']}, 'COH-ele_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [3.620849], 'name': 'COH-ele', 'frequencies': [], 'cross_interaction_parameters': {'H_111': [0.0]}, 'formation_energy_source': ['beta=(0.6446638651303487, 0.005368755279288262) COH_to_C 267j'], 'type': 'transition_state', 'composition': {'H': 1, 'C': 1, 'O': 1}, 'site': '111'}, '*_111': {'n_sites': 1, 'formation_energy': 0, 'name': '111', 'frequencies': [], 'site': '111', 'interaction_response_parameters': {'cutoff': 0.25, 'smoothing': 0.05}, 'total': 1.0, 'type': 'site', 'composition': {}, 'site_names': ['111']}, 'H_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [0.197099], 'name': 'H', 'frequencies': [], 'formation_energy_source': ['beta=(0.00105005948699, 0.028759896948697555)'], 'cross_interaction_parameters': {'H_111': [0.0]}, 'site': '111', 'sigma_params': [0.00105005948699, 0.028759896948697555], 'type': 'adsorbate', 'composition': {'H': 1}}, 'CHO_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [1.053528], 'name': 'CHO', 'frequencies': [], 'formation_energy_source': ['beta=(0.06958176987052457, 0.07912760993555497)'], 'cross_interaction_parameters': {'H_111': [0.0]}, 'site': '111', 'sigma_params': [0.06958176987052457, 0.07912760993555497], 'type': 'adsorbate', 'composition': {'H': 1, 'C': 1, 'O': 1}}, 'g': {'n_sites': 0, 'formation_energy': 0, 'name': 'g', 'frequencies': [], 'site': 'g', 'total': 0, 'type': 'site', 'composition': {}, 'site_names': ['gas']}, 'CO-H2O-ele_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [2.468062], 'name': 'CO-H2O-ele', 'frequencies': [], 'cross_interaction_parameters': {'H_111': [0.0]}, 'formation_energy_source': ['beta=(0.7310279014764086, -1.2722465640763132) CO_to_COH 0'], 'type': 'transition_state', 'composition': {'H': 2, 'C': 1, 'O': 2}, 'site': '111'}, 'CH_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [1.123301], 'name': 'CH', 'frequencies': [], 'formation_energy_source': ['beta=(0.016817874043850933, 0.26136465629302125)'], 'cross_interaction_parameters': {'H_111': [0.0]}, 'site': '111', 'sigma_params': [0.016817874043850933, 0.26136465629302125], 'type': 'adsorbate', 'composition': {'H': 1, 'C': 1}}, 'CO_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [0.335671], 'name': 'CO', 'frequencies': [], 'formation_energy_source': ['beta=(0.1523670746611968, -0.8585293405548797)'], 'cross_interaction_parameters': {'H_111': [0.0]}, 'site': '111', 'sigma_params': [0.1523670746611968, -0.8585293405548797], 'type': 'adsorbate', 'composition': {'C': 1, 'O': 1}}, 'ele_g': {'n_sites': 0, 'pressure': 1.0, 'formation_energy': 0.0, 'name': 'ele', 'frequencies': [], 'formation_energy_source': 'Generated by CatMAP', 'type': 'gas', 'composition': {}, 'site': 'g'}, 'OH_g': {'n_sites': 0, 'pressure': 1.0, 'formation_energy': 0.0, 'name': 'OH', 'frequencies': [], 'formation_energy_source': 'Generated by CatMAP', 'type': 'gas', 'composition': {'H': 1, 'O': 1}, 'site': 'g'}, 'CH4_g': {'n_sites': 0, 'pressure': 0.0, 'formation_energy': -2.486809, 'name': 'CH4', 'frequencies': [0.0, 0.0, 0.0, 0.0, 0.0102906886, 0.01781652954, 0.15972884486, 0.16153901418000002, 0.16171259206000002, 0.18655902574000002, 0.18792285194000002, 0.3746802524, 0.38536769044, 0.38695468820000006, 0.38758700762000003], 'formation_energy_source': 'GK calc', 'type': 'gas', 'composition': {'H': 4, 'C': 1}, 'site': 'g'}, 'CHO-H2O-ele_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [2.474595], 'name': 'CHO-H2O-ele', 'frequencies': [], 'cross_interaction_parameters': {'H_111': [0.0]}, 'formation_energy_source': ['beta=(0.5362096965724192, -0.650410907236333) CHO_to_CHOH 131j'], 'type': 'transition_state', 'composition': {'H': 3, 'C': 1, 'O': 2}, 'site': '111'}, 'H2O-ele_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [1.428792], 'name': 'H2O-ele', 'frequencies': [], 'cross_interaction_parameters': {'H_111': [0.0]}, 'formation_energy_source': ['beta=(0.42359926075241655, -0.11802607677867906) clean_to_H 809j'], 'type': 'transition_state', 'composition': {'H': 2, 'O': 1}, 'site': '111'}, 'OC-H2O-ele_111': {'n_sites': 1, 'self_interaction_parameter': [0.0], 'formation_energy': [1.677078], 'name': 'OC-H2O-ele', 'frequencies': [], 'cross_interaction_parameters': {'H_111': [0.0]}, 'formation_energy_source': ['beta=(0.3304142072488211, -0.04572408611096083) CO_to_CHO 809j'], 'type': 'transition_state', 'composition': {'H': 2, 'C': 1, 'O': 2}, 'site': '111'}}

standard_coadsorbate_coverage = 'min'

standard_coverage = 'min'

surface_names = ('Cu',)

temperature = 300.0

thermodynamic_corrections = ['gas', 'adsorbate', 'electrochemical']

thermodynamic_variables = ['temperature', 'gas_pressures', 'voltage', 'beta', 'pH', 'Upzc', 'gas_thermo_mode', 'adsorbate_thermo_mode']

tolerance = 1e-25

transition_state_cross_interaction_mode = 'intermediate_state'

transition_state_names = ('CHO-H2O-ele_111', 'CHOH-ele_111', 'CO-H2O-ele_111', 'COH-ele_111', 'H2O-ele_111', 'OC-H2O-ele_111', 'echemTS-6-1.5_111')

verbose = 1

voltage = -1.8

voltage_diff_drop = 0.0

stdout = """header_evaluation: fail - could not save sys = <module 'sys' (built-in)>


initial_evaluation: success - initial guess at point [-1.00,14.00]
initial_evaluation: success - initial guess at point [-1.00,13.00]
initial_evaluation: success - initial guess at point [-1.00,12.00]
initial_evaluation: success - initial guess at point [-1.00,11.00]
initial_evaluation: success - initial guess at point [-1.00,10.00]
initial_evaluation: success - initial guess at point [-1.00, 9.00]
initial_evaluation: success - initial guess at point [-1.00, 8.00]
initial_evaluation: success - initial guess at point [-1.00, 7.00]
initial_evaluation: success - initial guess at point [-1.00, 6.00]
initial_evaluation: success - initial guess at point [-1.00, 5.00]
initial_evaluation: success - initial guess at point [-1.00, 4.00]
initial_evaluation: success - initial guess at point [-1.00, 3.00]
initial_evaluation: success - initial guess at point [-1.05,14.00]
initial_evaluation: success - initial guess at point [-1.05,13.00]
initial_evaluation: success - initial guess at point [-1.05,12.00]
initial_evaluation: success - initial guess at point [-1.05,11.00]
initial_evaluation: success - initial guess at point [-1.05,10.00]
initial_evaluation: success - initial guess at point [-1.05, 9.00]
initial_evaluation: success - initial guess at point [-1.05, 8.00]
initial_evaluation: success - initial guess at point [-1.05, 7.00]
initial_evaluation: success - initial guess at point [-1.05, 6.00]
initial_evaluation: success - initial guess at point [-1.05, 5.00]
initial_evaluation: success - initial guess at point [-1.05, 4.00]
initial_evaluation: success - initial guess at point [-1.05, 3.00]
initial_evaluation: success - initial guess at point [-1.10,14.00]
initial_evaluation: success - initial guess at point [-1.10,13.00]
initial_evaluation: success - initial guess at point [-1.10,12.00]
initial_evaluation: success - initial guess at point [-1.10,11.00]
initial_evaluation: success - initial guess at point [-1.10,10.00]
initial_evaluation: success - initial guess at point [-1.10, 9.00]
initial_evaluation: success - initial guess at point [-1.10, 8.00]
initial_evaluation: success - initial guess at point [-1.10, 7.00]
initial_evaluation: success - initial guess at point [-1.10, 6.00]
initial_evaluation: success - initial guess at point [-1.10, 5.00]
initial_evaluation: success - initial guess at point [-1.10, 4.00]
initial_evaluation: success - initial guess at point [-1.10, 3.00]
initial_evaluation: success - initial guess at point [-1.15,14.00]
initial_evaluation: success - initial guess at point [-1.15,13.00]
initial_evaluation: success - initial guess at point [-1.15,12.00]
initial_evaluation: success - initial guess at point [-1.15,11.00]
initial_evaluation: success - initial guess at point [-1.15,10.00]
initial_evaluation: success - initial guess at point [-1.15, 9.00]
initial_evaluation: success - initial guess at point [-1.15, 8.00]
initial_evaluation: success - initial guess at point [-1.15, 7.00]
initial_evaluation: success - initial guess at point [-1.15, 6.00]
initial_evaluation: success - initial guess at point [-1.15, 5.00]
initial_evaluation: success - initial guess at point [-1.15, 4.00]
initial_evaluation: success - initial guess at point [-1.15, 3.00]
initial_evaluation: success - initial guess at point [-1.20,14.00]
initial_evaluation: success - initial guess at point [-1.20,13.00]
initial_evaluation: success - initial guess at point [-1.20,12.00]
initial_evaluation: success - initial guess at point [-1.20,11.00]
initial_evaluation: success - initial guess at point [-1.20,10.00]
initial_evaluation: success - initial guess at point [-1.20, 9.00]
initial_evaluation: success - initial guess at point [-1.20, 8.00]
initial_evaluation: success - initial guess at point [-1.20, 7.00]
initial_evaluation: success - initial guess at point [-1.20, 6.00]
initial_evaluation: success - initial guess at point [-1.20, 5.00]
initial_evaluation: success - initial guess at point [-1.20, 4.00]
initial_evaluation: success - initial guess at point [-1.20, 3.00]
initial_evaluation: success - initial guess at point [-1.25,14.00]
initial_evaluation: success - initial guess at point [-1.25,13.00]
initial_evaluation: success - initial guess at point [-1.25,12.00]
initial_evaluation: success - initial guess at point [-1.25,11.00]
initial_evaluation: success - initial guess at point [-1.25,10.00]
initial_evaluation: success - initial guess at point [-1.25, 9.00]
initial_evaluation: success - initial guess at point [-1.25, 8.00]
initial_evaluation: success - initial guess at point [-1.25, 7.00]
initial_evaluation: success - initial guess at point [-1.25, 6.00]
initial_evaluation: success - initial guess at point [-1.25, 5.00]
initial_evaluation: success - initial guess at point [-1.25, 4.00]
initial_evaluation: success - initial guess at point [-1.25, 3.00]
initial_evaluation: success - initial guess at point [-1.30,14.00]
initial_evaluation: success - initial guess at point [-1.30,13.00]
initial_evaluation: success - initial guess at point [-1.30,12.00]
initial_evaluation: success - initial guess at point [-1.30,11.00]
initial_evaluation: success - initial guess at point [-1.30,10.00]
initial_evaluation: success - initial guess at point [-1.30, 9.00]
initial_evaluation: success - initial guess at point [-1.30, 8.00]
initial_evaluation: success - initial guess at point [-1.30, 7.00]
initial_evaluation: success - initial guess at point [-1.30, 6.00]
initial_evaluation: success - initial guess at point [-1.30, 5.00]
initial_evaluation: success - initial guess at point [-1.30, 4.00]
initial_evaluation: success - initial guess at point [-1.30, 3.00]
initial_evaluation: success - initial guess at point [-1.35,14.00]
initial_evaluation: success - initial guess at point [-1.35,13.00]
initial_evaluation: success - initial guess at point [-1.35,12.00]
initial_evaluation: success - initial guess at point [-1.35,11.00]
initial_evaluation: success - initial guess at point [-1.35,10.00]
initial_evaluation: success - initial guess at point [-1.35, 9.00]
initial_evaluation: success - initial guess at point [-1.35, 8.00]
initial_evaluation: success - initial guess at point [-1.35, 7.00]
initial_evaluation: success - initial guess at point [-1.35, 6.00]
initial_evaluation: success - initial guess at point [-1.35, 5.00]
initial_evaluation: success - initial guess at point [-1.35, 4.00]
initial_evaluation: success - initial guess at point [-1.35, 3.00]
initial_evaluation: success - initial guess at point [-1.40,14.00]
initial_evaluation: success - initial guess at point [-1.40,13.00]
initial_evaluation: success - initial guess at point [-1.40,12.00]
initial_evaluation: success - initial guess at point [-1.40,11.00]
initial_evaluation: success - initial guess at point [-1.40,10.00]
initial_evaluation: success - initial guess at point [-1.40, 9.00]
initial_evaluation: success - initial guess at point [-1.40, 8.00]
initial_evaluation: success - initial guess at point [-1.40, 7.00]
initial_evaluation: success - initial guess at point [-1.40, 6.00]
initial_evaluation: success - initial guess at point [-1.40, 5.00]
initial_evaluation: success - initial guess at point [-1.40, 4.00]
initial_evaluation: success - initial guess at point [-1.40, 3.00]
initial_evaluation: success - initial guess at point [-1.45,14.00]
initial_evaluation: success - initial guess at point [-1.45,13.00]
initial_evaluation: success - initial guess at point [-1.45,12.00]
initial_evaluation: success - initial guess at point [-1.45,11.00]
initial_evaluation: success - initial guess at point [-1.45,10.00]
initial_evaluation: success - initial guess at point [-1.45, 9.00]
initial_evaluation: success - initial guess at point [-1.45, 8.00]
initial_evaluation: success - initial guess at point [-1.45, 7.00]
initial_evaluation: success - initial guess at point [-1.45, 6.00]
initial_evaluation: success - initial guess at point [-1.45, 5.00]
initial_evaluation: success - initial guess at point [-1.45, 4.00]
initial_evaluation: success - initial guess at point [-1.45, 3.00]
initial_evaluation: success - initial guess at point [-1.50,14.00]
initial_evaluation: success - initial guess at point [-1.50,13.00]
initial_evaluation: success - initial guess at point [-1.50,12.00]
initial_evaluation: success - initial guess at point [-1.50,11.00]
initial_evaluation: success - initial guess at point [-1.50,10.00]
initial_evaluation: success - initial guess at point [-1.50, 9.00]
initial_evaluation: success - initial guess at point [-1.50, 8.00]
initial_evaluation: success - initial guess at point [-1.50, 7.00]
initial_evaluation: success - initial guess at point [-1.50, 6.00]
initial_evaluation: success - initial guess at point [-1.50, 5.00]
initial_evaluation: success - initial guess at point [-1.50, 4.00]
initial_evaluation: success - initial guess at point [-1.50, 3.00]
initial_evaluation: success - initial guess at point [-1.55,14.00]
initial_evaluation: success - initial guess at point [-1.55,13.00]
initial_evaluation: success - initial guess at point [-1.55,12.00]
initial_evaluation: success - initial guess at point [-1.55,11.00]
initial_evaluation: success - initial guess at point [-1.55,10.00]
initial_evaluation: success - initial guess at point [-1.55, 9.00]
initial_evaluation: success - initial guess at point [-1.55, 8.00]
initial_evaluation: success - initial guess at point [-1.55, 7.00]
initial_evaluation: success - initial guess at point [-1.55, 6.00]
initial_evaluation: success - initial guess at point [-1.55, 5.00]
initial_evaluation: success - initial guess at point [-1.55, 4.00]
initial_evaluation: success - initial guess at point [-1.55, 3.00]
initial_evaluation: success - initial guess at point [-1.60,14.00]
initial_evaluation: success - initial guess at point [-1.60,13.00]
initial_evaluation: success - initial guess at point [-1.60,12.00]
initial_evaluation: success - initial guess at point [-1.60,11.00]
initial_evaluation: success - initial guess at point [-1.60,10.00]
initial_evaluation: success - initial guess at point [-1.60, 9.00]
initial_evaluation: success - initial guess at point [-1.60, 8.00]
initial_evaluation: success - initial guess at point [-1.60, 7.00]
initial_evaluation: success - initial guess at point [-1.60, 6.00]
initial_evaluation: success - initial guess at point [-1.60, 5.00]
initial_evaluation: success - initial guess at point [-1.60, 4.00]
initial_evaluation: success - initial guess at point [-1.60, 3.00]
initial_evaluation: success - initial guess at point [-1.65,14.00]
initial_evaluation: success - initial guess at point [-1.65,13.00]
initial_evaluation: success - initial guess at point [-1.65,12.00]
initial_evaluation: success - initial guess at point [-1.65,11.00]
initial_evaluation: success - initial guess at point [-1.65,10.00]
initial_evaluation: success - initial guess at point [-1.65, 9.00]
initial_evaluation: success - initial guess at point [-1.65, 8.00]
initial_evaluation: success - initial guess at point [-1.65, 7.00]
initial_evaluation: success - initial guess at point [-1.65, 6.00]
initial_evaluation: success - initial guess at point [-1.65, 5.00]
initial_evaluation: success - initial guess at point [-1.65, 4.00]
initial_evaluation: success - initial guess at point [-1.65, 3.00]
initial_evaluation: success - initial guess at point [-1.70,14.00]
initial_evaluation: success - initial guess at point [-1.70,13.00]
initial_evaluation: success - initial guess at point [-1.70,12.00]
initial_evaluation: success - initial guess at point [-1.70,11.00]
initial_evaluation: success - initial guess at point [-1.70,10.00]
initial_evaluation: success - initial guess at point [-1.70, 9.00]
initial_evaluation: success - initial guess at point [-1.70, 8.00]
initial_evaluation: success - initial guess at point [-1.70, 7.00]
initial_evaluation: success - initial guess at point [-1.70, 6.00]
initial_evaluation: success - initial guess at point [-1.70, 5.00]
initial_evaluation: success - initial guess at point [-1.70, 4.00]
initial_evaluation: success - initial guess at point [-1.70, 3.00]
initial_evaluation: success - initial guess at point [-1.75,14.00]
initial_evaluation: success - initial guess at point [-1.75,13.00]
initial_evaluation: success - initial guess at point [-1.75,12.00]
initial_evaluation: success - initial guess at point [-1.75,11.00]
initial_evaluation: success - initial guess at point [-1.75,10.00]
initial_evaluation: success - initial guess at point [-1.75, 9.00]
initial_evaluation: success - initial guess at point [-1.75, 8.00]
initial_evaluation: success - initial guess at point [-1.75, 7.00]
initial_evaluation: success - initial guess at point [-1.75, 6.00]
initial_evaluation: success - initial guess at point [-1.75, 5.00]
initial_evaluation: success - initial guess at point [-1.75, 4.00]
initial_evaluation: success - initial guess at point [-1.75, 3.00]
initial_evaluation: success - initial guess at point [-1.80,14.00]
initial_evaluation: success - initial guess at point [-1.80,13.00]
initial_evaluation: success - initial guess at point [-1.80,12.00]
initial_evaluation: success - initial guess at point [-1.80,11.00]
initial_evaluation: success - initial guess at point [-1.80,10.00]
initial_evaluation: success - initial guess at point [-1.80, 9.00]
initial_evaluation: success - initial guess at point [-1.80, 8.00]
initial_evaluation: success - initial guess at point [-1.80, 7.00]
initial_evaluation: success - initial guess at point [-1.80, 6.00]
initial_evaluation: success - initial guess at point [-1.80, 5.00]
initial_evaluation: success - initial guess at point [-1.80, 4.00]
initial_evaluation: success - initial guess at point [-1.80, 3.00]
mapper_iteration_0: status - 0 points do not have valid solution.
header_evaluation: fail - could not save sys = <module 'sys' (built-in)>

"""