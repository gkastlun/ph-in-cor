#Input master in ../../scripts contains most of the model parameters
from input_master import *

## Reaction network
rxn_expressions=[
'*_110 + ele_g + H2O_g <-> ^1.5eV_110 <-> H_110 + OH_g; beta=0.5',   #1
'CO_110 + ele_g + H2O_g <-> OC-H2O-ele_110 <-> CHO_110 + OH_g; beta=0.38770438902170645',   #2
#'CO_110 + ele_g + H2O_g <-> CO-H2O-ele_110 <-> COH_110 + OH_g; beta=0.3385713010811752',   #3
'CHO_110 + ele_g + H2O_g <-> CHO-H2O-ele_110 <-> CHOH_110 + OH_g; beta=0.5679839958430297',   #6
#'COH_110 + ele_g <-> COH-ele_110 <-> C_110 + OH_g; beta=0.16517563660648968 ',   #7
'CHOH_110 + ele_g <-> CHOH-ele_110 <-> CH_110 + OH_g; beta=0.38845881079062394 ',   #10
#'C_110 + ele_g + H2O_g <-> CH_110 + OH_g; beta=0.5',   #11
'CH_110 + 3H2O_g + 3ele_g -> CH4_g + 3OH_g + *_110',
'H2O_g + ele_g + H*_110 -> H2_g + *_110 + OH_g; beta=0.5',     # Heyrovsky 2
'H*_110 + H*_110 -> H2_g + 2*_110',     # Tafel 3
'CO_g + *_110 <-> CO_110',
]
# Standard rate parameter settings - usually not changed
prefactor_list = [1e13]*len(rxn_expressions)  #Heine: prefactor for CO is 1e8

species_definitions['110'] = {'site_names': ['110'], 'total':1.0}
species_definitions['110']['interaction_response_parameters'] = {'cutoff':0.25,'smoothing':0.05}
species_definitions['dl'] = {'site_names': ['dl'], 'total':1.0}
species_definitions['CO_110'] =             {
		'self_interaction_parameter':[None]*numBeforePt+[eCO]+[None]*numAfterPt,
		'cross_interaction_parameters':{
		'H_110': [None]*numBeforePt+[eHCO]+[None]*numAfterPt,
} }

species_definitions['OH_110'] = {
                   'self_interaction_parameter':[None]*numBeforePt+[1.0330]+[None]*numAfterPt,
                   }

species_definitions['H_110'] = {
                   'cross_interaction_parameters':{
		    'CO_110': [None]*numBeforePt+[eHCO]+[None]*numAfterPt}}

species_definitions['OCCO_110'] = {
                   'self_interaction_parameter':[None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
                   'cross_interaction_parameters':{
                   'CO_110': [None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
                   'H_110': [None]*numBeforePt+[1*eHCO]+[None]*numAfterPt},
                   'n_sites':2}


for sp in ['CO_110','CO-H2O-ele_110','OC-H2O-ele_110','H_110','CHO_110','CHO-H2O-ele_110','COH_110','COH-ele_110','CHOH_110','CHOH-ele_110','C_110','CH_110','CH2_110','CH3_110','H3CCO_110','O_110','OH_110',]:
    species_definitions[sp] = species_definitions['CO_110'].copy()

for sp in ['COCO_110','CO-CO_110','OCCO_110','OCCO-H2O-ele_110','OCCOH_110','OCCHO_110','HOCCOH_110','CCO_110','CCOH_110','HCCO_110','HCCO-H2O-ele_110','OHCC-H2O-ele_110','CCHO_110','CC_110','H2CCO_110','H2CCO-H2O-ele_110','OH2CC-H2O-ele_110','COH2C-H2O-ele_110','HCCOH_110','HCCHO_110','CCH_110','OHCCH2_110','H2CCHO_110','H2CCOH_110','HCCH_110','OCHCH3_110','H2CCH2O_110','H2CCH2O-H2O-ele_110','H2CCH_110','H3CCH2O_110',]:
    species_definitions[sp] = species_definitions['OCCO_110'].copy()

sigma_input = ['CH', 1]
Upzc = 0.00
species_definitions['CO_110']['sigma_params']=[-0.07514393918370614,-0.2846638995601361]
species_definitions['H_110']['sigma_params']=[0.042782826572447495,-0.14012953677746529]
species_definitions['COCO_110']['sigma_params']=[0.24921565362694095,-1.8821977121774898]
species_definitions['OCCO_110']['sigma_params']=[0.5454666477937457,-2.633282358603701]
species_definitions['CHO_110']['sigma_params']=[0.28103338269596256,-1.2833394674167542]
species_definitions['COH_110']['sigma_params']=[-0.21034441738054435,1.6566816559619288]
species_definitions['OCCOH_110']['sigma_params']=[0.25893229722966676,-1.630310172566427]
species_definitions['CHOH_110']['sigma_params']=[-0.16359533530376136,0.5837043460387414]
species_definitions['C_110']['sigma_params']=[0.011252723891682768,1.131562501558023]
species_definitions['HOCCOH_110']['sigma_params']=[0.05771683896422919,-0.44373417325760084]
species_definitions['CCO_110']['sigma_params']=[0.2166362563146809,-1.9414543432943376]
species_definitions['CH_110']['sigma_params']=[-0.04241631772762203,0.4801115907036925]
species_definitions['CCOH_110']['sigma_params']=[-0.05445931557289181,0.2177953759612336]
species_definitions['HCCO_110']['sigma_params']=[-0.06531177951437711,-1.6391015910468]
species_definitions['CC_110']['sigma_params']=[0.02510709980774166,-0.899943197341863]
species_definitions['H2CCO_110']['sigma_params']=[0.27665970375997206,-2.540996753525458]
species_definitions['HCCOH_110']['sigma_params']=[-0.1879518550780282,-0.5450302933539751]
species_definitions['HCCHO_110']['sigma_params']=[-0.016742518141351328,-1.9734592656657295]
species_definitions['CCH_110']['sigma_params']=[-0.1439501344347866,-0.3872822022066116]
species_definitions['OHCCH2_110']['sigma_params']=[0.07074451756816948,-3.0960569544123406]
species_definitions['H3CCO_110']['sigma_params']=[-0.06701996529852268,-1.8482149355507547]
species_definitions['H2CCOH_110']['sigma_params']=[0.053868460946485404,-2.0500176387308695]
species_definitions['HCCH_110']['sigma_params']=[-0.19910444665294172,-0.31144191869628934]
species_definitions['H2CCH2O_110']['sigma_params']=[-0.19772536886356637,-1.6529046886036167]
species_definitions['O_110']['sigma_params']=[0.2757400073523963,-0.7919233271100203]
species_definitions['H3CCH2O_110']['sigma_params']=[0.013704479633439766,-3.5811610327718117]
species_definitions['OH_110']['sigma_params']=[0.019295168137768673,-0.5533246870655576]

data_file = '110.pkl'
