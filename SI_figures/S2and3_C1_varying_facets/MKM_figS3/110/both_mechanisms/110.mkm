from input_master import *
# General settings - usually unchanged
scaler = 'ThermodynamicScaler'
descriptor_names= ['voltage', 'pH']
#descriptor_ranges= [[-1.5,0],[3,8]]
descriptor_ranges= [[-1.8,-1.0],[3,14]]
temperature = 300.
#resolution=[15,15]
resolution=[17,12]

gas_thermo_mode = 'frozen_gas'
adsorbate_thermo_mode = 'frozen_adsorbate'
electrochemical_thermo_mode = 'surface_charge_density'

# solver settings
decimal_precision = 100
tolerance = 1e-25
max_rootfinding_iterations = 200
max_bisections = 3

## Reaction network
rxn_expressions=[
'*_110 + ele_g + H2O_g <-> ^1.5eV_110 <-> H_110 + OH_g; beta=0.5',   #1
'CO_110 + ele_g + H2O_g <-> OC-H2O-ele_110 <-> CHO_110 + OH_g; beta=0.38770438902170645',   #2
'CO_110 + ele_g + H2O_g <-> CO-H2O-ele_110 <-> COH_110 + OH_g; beta=0.3385713010811752',   #3
'CHO_110 + ele_g + H2O_g <-> CHO-H2O-ele_110 <-> CHOH_110 + OH_g; beta=0.5679839958430297',   #6
'COH_110 + ele_g <-> COH-ele_110 <-> C_110 + OH_g; beta=0.16517563660648968 ',   #7
'CHOH_110 + ele_g <-> CHOH-ele_110 <-> CH_110 + OH_g; beta=0.38845881079062394 ',   #10
'C_110 + ele_g + H2O_g <-> ^1.5eV_110 <-> CH_110 + OH_g; beta=0.5',   #11
'CH_110 + 3H2O_g + 3ele_g -> CH4_g + 3OH_g + *_110',

'H2O_g + ele_g + H*_110 -> H2_g + *_110 + OH_g; beta=0.5',     # Heyrovsky 2
'H*_110 + H*_110 -> H2_g + 2*_110',     # Tafel 3
'CO_g + *_110 <-> CO_110',
#'C_110 + CO_g <-> OCC_110',   #21
]
#rxn_mechanisms = {
#       'C2_OCCOH':[5, 5, 22,27,  28, 29, 31],
#}

# Standard rate parameter settings - usually not changed
prefactor_list = [1e13]*len(rxn_expressions)  #Heine: prefactor for CO is 1e8
beta = 0.5

# Cu - CO2 reduction input file settings
input_file = '../../../../../formation_energies_for_catmap.txt'
surface_names = ['Cu']

# Electrochemical settings - usually not changed
potential_reference_scale = 'SHE'
# extrapolated potential only acts on electrochemical barriers
#extrapolated_potential = -0.416985308915 # Xinyan extrapolated her barriers to this potential! Odd!




species_definitions = {}
# pressures
#species_definitions['H_g'] = {'pressure':1.0}
species_definitions['ele_g'] = {'pressure':1.0, 'composition':{}}
species_definitions['CO_g'] = {'pressure':1.0}
species_definitions['H2_g'] = {'pressure':0.0}
species_definitions['H2O_g'] = {'pressure':0.035}
species_definitions['CH4_g'] = {'pressure':0.0}
#species_definitions['CH2O_g'] = {'pressure':0.0}
species_definitions['O2_g'] = {'pressure':0.0}
species_definitions['CH3CH2OH_g'] = {'pressure':0.0}
species_definitions['C2H4_g'] = {'pressure':0.0}
species_definitions['OH_g'] = {'pressure':0.0}

#sites
#frequency_surface_names = ['211','111']
estimate_frequencies = 1



# interaction model

adsorbate_interaction_model = 'first_order' #use "single site" interaction model
#adsorbate_interaction_model = None #use "single site" interaction model

interaction_response_function = 'smooth_piecewise_linear' #use "smooth piecewise linear" interactions

interaction_fitting_mode = None
cross_interaction_mode = 'geometric_mean' #use geometric mean for cross parameters

if 1 == len(surface_names):
	numBeforePt = 0
	numAfterPt = 0
else:
	numBeforePt = len(surface_names)-surface_names.index('Pt')
	numAfterPt = len(surface_names)-numBeforePt-1
	transition_state_cross_interaction_mode = 'transition_state_scaling' #use TS scaling for TS interaction

# site interaction scaling:

eHCO=0.7274*interaction_strength
eCO=2.4670*interaction_strength
eH=0.0

species_definitions['110'] = {'site_names': ['110'], 'total':1.0}
species_definitions['110']['interaction_response_parameters'] = {'cutoff':0.25,'smoothing':0.05}
species_definitions['dl'] = {'site_names': ['dl'], 'total':1.0}
species_definitions['CO_110'] =             {
		'self_interaction_parameter':[None]*numBeforePt+[eCO]+[None]*numAfterPt,
		'cross_interaction_parameters':{
		'H_110': [None]*numBeforePt+[eHCO]+[None]*numAfterPt,
########	'H2O-ele_t': [None]*numBeforePt+[eHCO*1.08]+[None]*numAfterPt,
########	'H-H2O-ele_t': [None]*numBeforePt+[eHCO*0.7]+[None]*numAfterPt,
########	'H-H_t': [None]*numBeforePt+[eHCO*1.59]+[None]*numAfterPt,
########	'H2O-CO-ele_t': [None]*numBeforePt+[eCO*0.79]+[None]*numAfterPt,
########	'CO-H2O-ele_t': [None]*numBeforePt+[eCO*3.1889]+[None]*numAfterPt,
########	'COH-H2O-ele_t': [None]*numBeforePt+[eCO*0.56]+[None]*numAfterPt,
########	'CH-OH-ele_t': [None]*numBeforePt+[eCO*1.89]+[None]*numAfterPt,
########	'H-CO_t': [None]*numBeforePt+[eCO*1]+[None]*numAfterPt,
########	'OC-CO_t': [None]*numBeforePt+[1.*eCO]+[None]*numAfterPt,
########	'OCCO_t': [None]*numBeforePt+[1.*eCO]+[None]*numAfterPt,
########	'OCCO-OH2-ele_t': [None]*numBeforePt+[1.0238*eCO]+[None]*numAfterPt,
########	'OC-CHO_t': [None]*numBeforePt+[1.194*eCO]+[None]*numAfterPt,
########	'OCCHO_t': [None]*numBeforePt+[1.4839*eCO]+[None]*numAfterPt,
########	'OCCO-H2O-ele_t': [None]*numBeforePt+[1.*eCO]+[None]*numAfterPt,
########	'OCCOH_t': [None]*numBeforePt+[0.44*eCO]+[None]*numAfterPt,
########	'OCC-OH-ele_t': [None]*numBeforePt+[1.*eCO]+[None]*numAfterPt,
########	'OCC_t': [None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
########	'OCC-H_t': [None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
########	'OCC-H2O-ele_t': [None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
########	'OCCH_t': [None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
########	'CO-_t': [None]*numBeforePt+[eCO]+[None]*numAfterPt,
########	'OC-CHOH_t': [None]*numBeforePt+[1.*eCO]+[None]*numAfterPt,
########	'OCCHOH_t': [None]*numBeforePt+[1.2745*eCO]+[None]*numAfterPt,
########	'CHO-OH2-ele_t': [None]*numBeforePt+[0.77*eCO]+[None]*numAfterPt,
########	'CH2O_t': [None]*numBeforePt+[0.3*eCO]+[None]*numAfterPt,
########	'CH2O-H2O-ele_t': [None]*numBeforePt+[0.3*eCO]+[None]*numAfterPt,
########	'CH2OH_t': [None]*numBeforePt+[0.68*eCO]+[None]*numAfterPt,
########	'CH2-OH-ele_t': [None]*numBeforePt+[0.68*eCO]+[None]*numAfterPt,
} }
species_definitions['OH_110'] = {
                   'self_interaction_parameter':[None]*numBeforePt+[1.0330]+[None]*numAfterPt,
                   }

species_definitions['H_110'] = {
                   'cross_interaction_parameters':{
		    'CO_110': [None]*numBeforePt+[eHCO]+[None]*numAfterPt,
#                   'H2O-ele_t': [None]*numBeforePt+[eH*1.08]+[None]*numAfterPt,
#   #		'H-H2O-ele_t': [None]*numBeforePt+[eH*0.7]+[None]*numAfterPt,
#                   'H-H_t': [None]*numBeforePt+[eH*1.59]+[None]*numAfterPt,
#                   'H2O-CO-ele_t': [None]*numBeforePt+[eHCO*0.79]+[None]*numAfterPt,
#                   'CO-H2O-ele_t': [None]*numBeforePt+[eHCO*3.1889]+[None]*numAfterPt,
#                   'COH-H2O-ele_t': [None]*numBeforePt+[eHCO*0.56]+[None]*numAfterPt,
#                   'H-CO_t': [None]*numBeforePt+[eHCO*1]+[None]*numAfterPt,
                            }
                   }

#   species_definitions['COH_110'] = {
#                   'self_interaction_parameter':[None]*numBeforePt+[1.75*eCO]+[None]*numAfterPt,
#                   }

species_definitions['OCCO_110'] = {
                   'self_interaction_parameter':[None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
                   'cross_interaction_parameters':{
                   'CO_110': [None]*numBeforePt+[1*eCO]+[None]*numAfterPt,
                   'H_110': [None]*numBeforePt+[1*eHCO]+[None]*numAfterPt,
#                   'H2O-ele_t': [None]*numBeforePt+[1*eHCO*1.08]+[None]*numAfterPt,
#   #		'H-H2O-ele_t': [None]*numBeforePt+[1*eHCO*0.39]+[None]*numAfterPt,
#                   'H-H_t': [None]*numBeforePt+[1*eHCO*1.59]+[None]*numAfterPt,
#                   'H2O-CO-ele_t': [None]*numBeforePt+[1*eCO*0.79]+[None]*numAfterPt,
#                   'CO-H2O-ele_t': [None]*numBeforePt+[1*eCO*3.1889]+[None]*numAfterPt,
#                   'COH-H2O-ele_t': [None]*numBeforePt+[1*eCO*0.56]+[None]*numAfterPt,
#                   'H-CO_t': [None]*numBeforePt+[1*eCO*1]+[None]*numAfterPt,
                           },
                   'n_sites':2
                   }


for sp in ['CO_110','CO-H2O-ele_110','OC-H2O-ele_110','H_110','CHO_110','CHO-H2O-ele_110','COH_110','COH-ele_110','CHOH_110','CHOH-ele_110','C_110','CH_110','CH2_110','CH3_110','H3CCO_110','O_110','OH_110',]:
    species_definitions[sp] = species_definitions['CO_110'].copy()

for sp in ['COCO_110','CO-CO_110','OCCO_110','OCCO-H2O-ele_110','OCCOH_110','OCCHO_110','HOCCOH_110','CCO_110','CCOH_110','HCCO_110','HCCO-H2O-ele_110','OHCC-H2O-ele_110','CCHO_110','CC_110','H2CCO_110','H2CCO-H2O-ele_110','OH2CC-H2O-ele_110','COH2C-H2O-ele_110','HCCOH_110','HCCHO_110','CCH_110','OHCCH2_110','H2CCHO_110','H2CCOH_110','HCCH_110','OCHCH3_110','H2CCH2O_110','H2CCH2O-H2O-ele_110','H2CCH_110','H3CCH2O_110',]:
    species_definitions[sp] = species_definitions['OCCO_110'].copy()

sigma_input = ['CH', 1]
Upzc = 0.00
species_definitions['CO_110']['sigma_params']=[-0.07514393918370614,-0.2846638995601361]
species_definitions['H_110']['sigma_params']=[0.042782826572447495,-0.14012953677746529]
species_definitions['COCO_110']['sigma_params']=[0.24921565362694095,-1.8821977121774898]
species_definitions['OCCO_110']['sigma_params']=[0.5454666477937457,-2.633282358603701]
species_definitions['CHO_110']['sigma_params']=[0.28103338269596256,-1.2833394674167542]
species_definitions['COH_110']['sigma_params']=[-0.21034441738054435,1.6566816559619288]
species_definitions['OCCOH_110']['sigma_params']=[0.25893229722966676,-1.630310172566427]
species_definitions['CHOH_110']['sigma_params']=[-0.16359533530376136,0.5837043460387414]
species_definitions['C_110']['sigma_params']=[0.011252723891682768,1.131562501558023]
species_definitions['HOCCOH_110']['sigma_params']=[0.05771683896422919,-0.44373417325760084]
species_definitions['CCO_110']['sigma_params']=[0.2166362563146809,-1.9414543432943376]
species_definitions['CH_110']['sigma_params']=[-0.04241631772762203,0.4801115907036925]
species_definitions['CCOH_110']['sigma_params']=[-0.05445931557289181,0.2177953759612336]
species_definitions['HCCO_110']['sigma_params']=[-0.06531177951437711,-1.6391015910468]
species_definitions['CC_110']['sigma_params']=[0.02510709980774166,-0.899943197341863]
species_definitions['H2CCO_110']['sigma_params']=[0.27665970375997206,-2.540996753525458]
species_definitions['HCCOH_110']['sigma_params']=[-0.1879518550780282,-0.5450302933539751]
species_definitions['HCCHO_110']['sigma_params']=[-0.016742518141351328,-1.9734592656657295]
species_definitions['CCH_110']['sigma_params']=[-0.1439501344347866,-0.3872822022066116]
species_definitions['OHCCH2_110']['sigma_params']=[0.07074451756816948,-3.0960569544123406]
species_definitions['H3CCO_110']['sigma_params']=[-0.06701996529852268,-1.8482149355507547]
species_definitions['H2CCOH_110']['sigma_params']=[0.053868460946485404,-2.0500176387308695]
species_definitions['HCCH_110']['sigma_params']=[-0.19910444665294172,-0.31144191869628934]
species_definitions['H2CCH2O_110']['sigma_params']=[-0.19772536886356637,-1.6529046886036167]
species_definitions['O_110']['sigma_params']=[0.2757400073523963,-0.7919233271100203]
species_definitions['H3CCH2O_110']['sigma_params']=[0.013704479633439766,-3.5811610327718117]
species_definitions['OH_110']['sigma_params']=[0.019295168137768673,-0.5533246870655576]

data_file = '110.pkl'
