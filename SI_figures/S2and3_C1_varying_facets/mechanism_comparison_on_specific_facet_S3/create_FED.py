import numpy as np
import math
import sys,os
sys.path.append('../../../tools_for_analysis')
import pickle as pckl
from matplotlib import pyplot
from mkm_creator import *
from FED_tools import *
from intermediates_dict import *

substrates= ['Cu']
if len(sys.argv) > 1:
    facets= [sys.argv[1]]#['100']
else:
    facets= ['111','100','211','110']

start_from_pkl=1

inputfile = None


pot_for_FED=3.2
figsize=(9,7)

def main():
    alldata=read_calculated_data(inputfile,facets,start_from_pkl,pklfile='../../../calculated_energetics.pkl')

    for pH in [13]:
      for facet in ['111','100','110','211']:
        plot_FED_with_barrier(ads_and_electron,
                [facet],
                [['CO','CHO','CHOH','CH'],
                 ['CO','COH','C','CH']],
                potentials=[2.9],
                pH=[pH],
                ylim=[-1.6,2.0],
                view=True,
                outdir='.',
                annotate_intermediates=True,
                proton_donor={13:'base',7:'base',3:'base',0:'acid'},
                figsize=(12,7),
                labelsize=32,
                fontsize=38,
                title='',
                outformat='pdf'
                )


main()
