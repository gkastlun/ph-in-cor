import numpy as np
import math
import sys,os
sys.path.append('../../../tools_for_analysis')
import pickle as pckl
from matplotlib import pyplot
from mkm_creator import *
from FED_tools import *
from intermediates_dict import *

show_reaction_steps=False
start_from_pkl=1
facets=['110','111','211','100']
inputfile = None#'catmap_input_files/catin_100_4.400.txt'

def main():
    alldata=read_calculated_data(inputfile,facets,start_from_pkl,pklfile='../../../calculated_energetics.pkl')

    for pot_for_FED in [3.2,2.9]:
     for pH in [13]:
        plot_FED_with_barrier(ads_and_electron,
                facets,
                ['CO','CHO','CHOH','CH'],
                potentials=[pot_for_FED],
                pH=[pH],
                ylim=[-1.7,2.5],
                view=False,
                outdir='.',
                annotate_intermediates=False,
                proton_donor={13:'base',7:'base',3:'base',0:'acid'},
                figsize=(12,6),
                title='',
                fontsize=25,
                outformat='pdf'
                )

        plot_FED_with_barrier(ads_and_electron,facets,[['CO','COH','C']],
                potentials=[pot_for_FED],pH=[pH],
                ylim=[-1.7,2.5],
                #view=True,
                outdir='.',
                annotate_intermediates=False,
                proton_donor='base',
                normalize_to_IS=False,
                figsize=(9,6),
                title='',
                fontsize=25,
                outformat='pdf'
                )


main()
