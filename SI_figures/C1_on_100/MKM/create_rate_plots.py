#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.patheffects as path_effects
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl

#from catmaphelpers.catmap_plotter import _make_plots, _plot_model, _plot_CV, post_process_CV, _compute_tafel_prate, plot_tafel_screening, _get_unit_cell_area, _plot_pH, _plot_map, _plot_pH_bar_coverage
#from catmaphelpers.catmap_output_handler import _get_data, _sum_catmap_data, _get_rate_control, read_catmap_wdir
#from catmaphelpers.extra_tools import make_FEDs

#sys.path.append("../.")
#from post_catmap import read_catmap_wdir
home=os.getcwd()
facet=home.split('/')[-2]

if __name__ == "__main__":
    data_in = pkl.load(open(facet+'_py3.pkl','rb'),encoding='latin1')
    data={'rate':{},'cov':{},'taf':{}}
    pots,phs=[],[]
    nsteps=1
    irate=[1,0]
    iads=[3,4]

    for dat in data_in['protductionrate_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['rate']:
            data['rate'][pot] = {}
        data['rate'][pot][ph] = dat[1]
        if pot not in pots:
            pots.append(np.around(pot,2))
        if ph not in phs:
            phs.append(ph)

#    for dat in data_in['coverage_map']:
#        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
#        if pot not in data['cov']:
#            data['cov'][pot] = {}
#        data['cov'][pot][ph] = dat[1]

    pots=sorted(np.unique(pots))
    for pH in data['rate'][pot]:
        if pH not in data['taf']:
            data['taf'][pH]={}
        for ipot,pot in enumerate(pots):
            if ipot==0: continue
            data['taf'][pH][(pot+pots[ipot-1])/2]=\
            -(pot-pots[ipot-1])/(np.log10(data['rate'][pot][pH][1])-np.log10(data['rate'][pots[ipot-1]][pH][1]))

    #X=np.array(sorted(np.unique(pots)))
    X=np.array(pots)
    Y=np.array(sorted(phs))

    fig,ax=plt.subplots(2,1,sharex=True,figsize=(7,9))

    rate=np.ones((len(X),len(Y)))*0.5
    for ix,x in enumerate(X):
            for iy,y in enumerate(Y):
                            try:
                                    if data['rate'][x][y][irate[0]] < 1e-13:
                                        rate[ix][iy]=1e-13
                                    else:
                                        rate[ix][iy]=data['rate'][x][y][irate[0]]#/np.sum(data[x][y][:nsteps])
                            except:
                                rate[ix][iy]=1e-20#np.nan#data[x][y][istep]#/np.sum(data[x][y][:nsteps])
                                pass

    b = ax[0].imshow(rate.T,
#                interpolation='bicubic',
                cmap=cm.jet,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-13,
                    vmax=1e05,
                    aspect=0.05)#, vmin=-abs(alldata[:,2]).max())

    ax[0].set_xticks([])
    ax[0].set_ylabel('pH',fontsize=30)

    mech=os.getcwd()[-3:]
    if facet in ['111','110'] and mech == 'COH':
        text=ax[0].annotate(mech+' mechanism',(-1.8,3.2),color='w',fontweight='bold',ha='left',fontsize=35)
        text.set_path_effects([path_effects.Stroke(linewidth=3, foreground='k'),
                       path_effects.Normal()])
    else:
        text=ax[0].annotate(mech+' mechanism',(-1.8,3.2),color='k',fontweight='bold',ha='left',fontsize=35)
        text.set_path_effects([path_effects.Stroke(linewidth=3, foreground='w'),
                       path_effects.Normal()])
    #fig.tight_layout()
    #fig.savefig('TOF.pdf')


    #plt.close()
    colors = plt.cm.YlGnBu(np.linspace(0,1,len(phs)))
    for iph,pH in enumerate(sorted(phs)):
        if pH%1: continue
        pltdat=[]
        for pot in data['taf'][pH]:
            pltdat.append([pot,data['taf'][pH][pot]])
        pltdat=np.array(pltdat)
        ax[1].plot(pltdat[:,0],pltdat[:,1]*1000,'-',color=colors[iph])
    #plt.annotate(r'$\frac{60}{\Delta \beta}=200$',(min(pltdat[:,0])-0.05,max(pltdat[:,1])*1000/1.1),fontsize=18)
    #plt.annotate(r'Volmer-Heyrovsky',(max(pltdat[:,0]),max(pltdat[:,1])*1000/1.02),ha='right',fontsize=18,fontweight='bold')
    #plt.annotate(r'$\beta_V - \beta_H$=0.3',(max(pltdat[:,0])-0.05,max(pltdat[:,1])*1000/1.12),ha='right',fontweight='bold',fontsize=18)
    #plt.arrow(-0.7,125,-0.4,0,length_includes_head=True,head_width=4,head_length=0.1,visible=True)
    #plt.annotate(r'$\Delta$pH$\cdot$60mV',(-0.7,130),ha='left',bbox=dict(facecolor='w', edgecolor='w'),xytext=(-0.7,125),fontsize=18)
    if facet == '211' and mech == 'COH':
        ax[1].set_ylim([0,150])
    ax[1].set_ylabel('Tafel slope [mV/dec]',fontsize=25)
    ax[1].set_xlabel('U$_{SHE}$ [V]',fontsize=25)
    #ax[1].set_yticks(fontsize=20)
    ax[1].tick_params(axis='x',labelsize=20)
    ax[0].tick_params(axis='y',labelsize=20)
    ax[1].tick_params(axis='y',labelsize=20)
    ax[1].set_xticks([-1.8,-1.4,-1.0])
    fig.tight_layout()
    fig.savefig('TOF_and_Tafel_slopes.pdf')

    exit()
