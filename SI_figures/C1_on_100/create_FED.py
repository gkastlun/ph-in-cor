import numpy as np
import math
import sys,os
sys.path.append('/Users/geokast/SelectCO2/endstates/')
import pickle as pckl
from matplotlib import pyplot
from tools_for_analysis.mkm_creator import *
from tools_for_analysis.FED_tools import *
from intermediates_dict import *



#inputfile = 'C2model_h2o.pkl'
show = True
add_field=False
products=['C2H4','CH3CH2OH','CH4','H2']

substrates= ['Cu']
if len(sys.argv) > 1:
    facets= [sys.argv[1]]#['100']
else:
    facets= ['111','100','211','110']


pH = 13
kT=0.025
show_reaction_steps=False
PZC=-0.5
PZC=0.0
dqdphi=20 #muF/cm^2
dqdphi=1 #Given as beta
start_from_pkl=1

try:
    SHE_potential = float(sys.argv[2])-4.4
    inputfile = 'catmap_input_files/catin_%s_%s.txt'%(sys.argv[1],sys.argv[2])
except:
    SHE_potential = -0.767#float(sys.argv[1])
    inputfile = 'catmap_input_files/catin_100_4.400.txt'

RHE_potential = SHE_potential+0.059*pH

ylabel='$\Delta$G [eV]'
if show_reaction_steps:
    xlabel='Reaction step'
else:
    xlabel='H$^+$ and e$^-$ transferred'

pot_for_FED=3.2
figsize=(9,7)

def main():
    alldata=read_calculated_data(inputfile,facets,start_from_pkl,pklfile='parsed_data.pckl')

    plot_FED_with_barrier(ads_and_electron,
                [100],
                [['CO','COH','C']],
                potentials=[2.4,2.9,3.4],
                pH=[13],
                ylim=[-1.6,2.0],
                view=True,
                outdir='.',
                annotate_intermediates=True,
                proton_donor={13:'base',7:'base',3:'base',0:'acid'},
                figsize=(12,7),
                labelsize=32,
                title=''
                )

    plot_FED_with_barrier(ads_and_electron,
                [100],
                [['CO','COH','C']],
                potentials=[2.9],
                pH=[7,10,13],
                ylim=[-1.6,2.0],
                view=True,
                outdir='.',
                annotate_intermediates=True,
                proton_donor='base',
                figsize=(12,7),
                labelsize=32,
                title=''
                )

main()
