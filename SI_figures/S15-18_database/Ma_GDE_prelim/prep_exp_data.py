#!/usr/bin/env python

import sys, os
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
sys.path.append('../.')
from plot_paper_data import _write_pickle_file

def _read_header(txtfile):
    with open(txtfile, 'r') as infile:
        print(txtfile)
        hline = infile.readlines()[0][1:]
    return(hline)

def _read_meta_data(mfile):
    with open(mfile, 'r') as infile:
        lines = infile.readlines()
    dat = {}
    for l in lines:
        e = [s.strip() for s in l.split('=')]
        dat.update({e[0]:e[1]})
    return(dat)

if __name__ == "__main__":
    # read main data
    # no std std deviation
    full_data = {}
    nfiles,mfiles=[],[]

    for files in os.listdir():
        if files.split('_')[0] == 'raw':
            if files.split('_')[1][:4] == 'data':
                nfiles.append(files)
        elif files.split('_')[0] == 'meta':
            if files.split('_')[1][:4] == 'data':
                mfiles.append(files)

    if len(nfiles) != len(mfiles):
        print('The number of experimental and meta data files in %s do not\
               match!  Check!'%(os.getcwd().split('/')[-1]))


    for nfile in nfiles:
        #nfile = 'raw_data_%i.txt'%i
        keys = _read_header(nfile).split()
        val = np.loadtxt(nfile)
        # std
        sfile = 'raw_std_%i.txt'%int(nfile.split('.')[0].split('_')[2])
        std = np.loadtxt(sfile)

        if len(val.shape) == 1:
            val=np.array([val])
            std=np.array([std])

        val[:,2:] /= 100. # correct for percentage
        std[:,1:] /= 100

        # prep data
        data = {keys[i]:np.zeros((val.shape[0], 2)) for i in range(len(keys))}
        for k in data:
            data[k][:,0] = val[:,keys.index(k)]
            data[k][:,1] = std[:,keys.index(k)-1]
        

        mfile = 'meta'+nfile.lstrip('raw')
        mdat = _read_meta_data(mfile)
        mdat['roughness'] = 6.0
        data.update(mdat)

        # sort into one file
        full_data.update({mdat['catalyst']+'+pH'+str(mdat['pH']):data})


    # save data
    name = os.getcwd().split('/')[-1]
    _write_pickle_file("%s.pkl"%name, full_data)


