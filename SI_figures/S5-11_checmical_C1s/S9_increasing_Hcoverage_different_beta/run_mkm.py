from catmap import ReactionModel
from catmap import analyze
from string import Template
import os,sys
import pickle

model = ReactionModel(setup_file = '211.mkm')
model.output_variables+=['production_rate']
model.run()

vm = analyze.VectorMap(model)
vm.plot_variable = 'rate'
vm.log_scale = True
vm.min = 1e-20
vm.max = 1e+3
fig = vm.plot(save=False)
fig.savefig('rate.pdf')

vm.plot_variable = 'production_rate'
vm.log_scale = True
vm.colorbar = True
vm.min = 1e-20
vm.max = 1e+5
fig = vm.plot(save=False)
fig.savefig('production_rate.pdf')

vm = analyze.VectorMap(model)
vm.log_scale = False
vm.unique_only = False
vm.plot_variable = 'coverage'
vm.min = 0
vm.max = 1
fig = vm.plot(save=False)
fig.savefig('coverage.pdf')

vm = analyze.VectorMap(model)
vm.log_scale = True
vm.unique_only = False
vm.plot_variable = 'coverage'
vm.min = 1e-20
vm.max = 1
fig = vm.plot(save=False)
fig.savefig('coverageLog.pdf')

os.system('python ../../../tools_for_analysis/transfering_pickles.py 211.pkl')

