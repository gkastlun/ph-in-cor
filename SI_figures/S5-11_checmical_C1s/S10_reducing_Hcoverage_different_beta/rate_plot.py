#!/usr/bin/env python
import sys, os
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
import matplotlib.patheffects as path_effects
sys.path.append('../scripts/')
from rate_plot_tools import *
import matplotlib

matplotlib.rcParams['font.size']=14
data,X,Y,pots,phs = parse_catmap_pkl(outfile='211_py3.pkl')
nsteps=2
irate=[0,1]
iads=[3,4]

if nsteps == 1:cols=1
else: cols=2
cols=2

fig,ax=plt.subplots(nsteps,cols,figsize=(12,5))

for col in range(cols):
  for istep in range(nsteps):
    rate,XY = create_plot_matrix(data,X,Y,iads,istep,col,irate)
    if col == 0:
        ax,b = create_heat_plots(ax,rate,col,XY,X,Y,nsteps,cols,istep,interpolation='bicubic')
    else:
        ax,a = create_heat_plots(ax,rate,col,XY,X,Y,nsteps,cols,istep,interpolation='bicubic')

add_colorbars(fig,ax,cols,nsteps,a,b)
add_main_panel_annotations(ax)
add_RHE_annotation(ax,CH4_rate_color='w')

ax[0,0].annotate('Tafel\nslope',(-1.59,10),color='w',ha='left',fontsize=12)
ax[0,0].annotate(r'$\approx \frac{60}{\Delta\beta}$',(-1.29,10.3),color='w',ha='left',fontsize=14)
ax[1,1].annotate('K$_\mathrm{V}$ governing',(-0.5,8),color='k',ha='left',fontsize=13,rotation=-75)
ax[1,1].annotate(r'$\Delta\beta$ governing',(-1.2,8),color='k',ha='left',fontsize=13,rotation=-90)

#If tafel slope plot should be added
plt.subplots_adjust(left=0.1,right=0.5,bottom=0.15)
tafax=plt.axes([0.61, 0.13, 0.38, 0.82])
tafax.set_ylim(-300,300)
pltdat,colors=add_Tafel_slope_plot(tafax,data,phs)

tafax.annotate(r'$\frac{60}{\Delta \beta}\approx-200$',(min(pltdat[:,0])-0.05,-250),fontsize=18)
tafax.annotate(r'Volmer-Heyrovsky',(min(pltdat[:,0])-0.05,250),ha='left',fontsize=18,fontweight='bold')
tafax.annotate(r'$\beta_V - \beta_H$=-0.3',(min(pltdat[:,0])-0.05,200),ha='left',fontweight='bold',fontsize=18)
tafax.arrow(-0.6,-50,-0.4,0,length_includes_head=True,head_width=4,head_length=0.1,visible=True)
tafax.annotate(r'$\Delta$pH$\cdot$60mV',(-0.5,-50),ha='left',bbox=dict(facecolor='w', edgecolor='w'),fontsize=18)
text=plt.annotate(r'pH 7',(-0.65,-200),color=colors[0],rotation=-90,fontsize=20,fontweight='bold')
text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                   path_effects.Normal()])
text=plt.annotate(r'pH 13',(-1.2,-100),color=colors[-1],rotation=-90,fontsize=20,fontweight='bold')
plt.ylabel('Tafel slope [mV/dec]',fontsize=18)
plt.xlabel('U$_{SHE}$ [V]',fontsize=18)
plt.savefig('TOF_and_Tafel_slopes.pdf')
