#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl
import matplotlib.patheffects as path_effects

data_in = pkl.load(open('211_py3.pkl','rb'),encoding='latin1')
data={'rate':{},'cov':{}}
pots,phs=[],[]
nsteps=2
irate=[0,1]
iads=[3,4]

for dat in data_in['rate_map']:
    pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
    if pot not in data['rate']:
        data['rate'][pot] = {}
    data['rate'][pot][ph] = dat[1]
    if pot not in pots:
        pots.append(np.around(pot,2))
    if ph not in phs:
        phs.append(ph)

for dat in data_in['coverage_map']:
    pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
    if pot not in data['cov']:
        data['cov'][pot] = {}
    data['cov'][pot][ph] = dat[1]

X=np.array(sorted(np.unique(pots)))
Y=np.array(sorted(phs))
if nsteps == 1:cols=1
else: cols=2
cols=2

fig,ax=plt.subplots(nsteps,cols)


for col in range(cols):
  for istep in range(nsteps):
    XY=np.ones((len(X),len(Y)))*0.5
    rate=np.ones((len(X),len(Y)))*0.5
    for ix,x in enumerate(X):
        for iy,y in enumerate(Y):
                        try:
                            if col == 1:
                                XY[ix][iy]=data['cov'][x][y][iads[istep]]
                                if data['cov'][x][y][iads[istep]] < 0:
                                    print(data['cov'][x][y][iads[istep]])
                            else:
                                if data['rate'][x][y][irate[istep]] < 1e-13:
                                    rate[ix][iy]=1e-13
                                else:
                                    rate[ix][iy]=data['rate'][x][y][irate[istep]]#/np.sum(data[x][y][:nsteps])
                        except:
                            XY[ix][iy]=np.nan#data[x][y][istep]#/np.sum(data[x][y][:3])
                            rate[ix][iy]=1e-20#np.nan#data[x][y][istep]#/np.sum(data[x][y][:nsteps])
                            print(x,y)
                            pass

    if col == 0:
     if nsteps == 1: plotax=ax
     elif cols == 1: plotax= ax[istep]
     else: plotax= ax[istep][0]


     b = plotax.imshow(rate.T,
            interpolation='bicubic',
            cmap=cm.jet,
               origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                vmin=1e-15,
                vmax=1e05,#)
                aspect='auto')#, vmin=-abs(alldata[:,2]).max())

    else:
     a = ax[istep][1].imshow(XY.T,
            interpolation='bicubic',
            cmap=cm.RdYlGn,
               origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                vmin=1e-10,
                vmax=1,
                aspect='auto')#, vmin=-abs(alldata[:,2]).max())

    if istep == nsteps-1:
        #fig.colorbar(a,ax=ax[0][1],fraction=0.146, pad=.04,label='Selectivity')
        if nsteps==1: ax.set_xlabel('U$_{SHE}$ [V]')
        else:
            if cols == 1:
                ax[istep].set_xlabel('U$_{SHE}$ [V]')
            else:
                for thisax in ax[istep]:
                    thisax.set_xlabel('U$_{SHE}$ [V]')
    else:
        if cols == 1:
                ax[istep].set_xticks([])
        else:
            for thisax in ax[istep]:
                thisax.set_xticks([])

    if cols > 1:
        ax[istep][0].set_ylabel('pH')
        ax[istep][1].set_yticks([])
    else:
        if nsteps == 1:
            ax.set_ylabel('pH')
        else:
            ax[istep].set_ylabel('pH')

if cols == 1:
    if nsteps == 1: barax=ax
    else: barax=ax[0]
else: barax = ax[0][0]
axins1 = inset_axes(barax,
                width="100%",  # width = 50% of parent_bbox width
                height="5%",  # height : 5%
                loc='lower left',
                 bbox_to_anchor=(0, 1.+nsteps/10., 1, 1),
              bbox_transform=barax.transAxes)
fig.colorbar(b, cax=axins1,orientation='horizontal',fraction=0.07,anchor=(1.0,1.0))#cbar_ax)

if cols > 1:
 axins2 = inset_axes(ax[0][1],
                width="100%",  # width = 50% of parent_bbox width
                height="5%",  # height : 5%
                loc='lower left',
                bbox_to_anchor=(0, 1.2, 1, 1),
                bbox_transform=ax[0][1].transAxes)
 fig.colorbar(a, cax=axins2,orientation='horizontal',fraction=0.07,anchor=(1.0,1.0))#cbar_ax)
 ax[0][1].annotate('Coverage',(-1.4,14),color='k',bbox=dict(facecolor='w', edgecolor=None))
 text = ax[0][1].annotate(r'*CO',(-1.59,7.2),color='w',fontweight='bold',fontsize=20,ha='left')
 text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
 text=ax[1][1].annotate(r'*H',(-1.59,7.2),color='w',fontweight='bold',fontsize=20,ha='left')
 text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])


 ax[1][1].arrow(-0.45,7,-0.059*1.8,1.8,head_width=False,length_includes_head=True,linestyle='dotted')
 startpH=12.5
 ax[1][1].arrow(-0.45-0.059*(startpH-7),startpH,-0.059*(14-startpH),14-startpH,
         head_width=False,length_includes_head=True,linestyle='dotted')
 ax[1][1].annotate('$U_{RHE} = -\Delta$G$_H$',(-0.7-0.059*2,9),rotation=-72)

barax.annotate('Turnover frequency [s$^{-1}$]',(-1.55,14),color='k',bbox=dict(facecolor='w', edgecolor=None))

for row in range(nsteps):
 ax[0][0].arrow(-0.45,7,-0.059*1.8,1.8,head_width=False,length_includes_head=True,linestyle='dotted',color='w')
 ax[1][0].arrow(-0.45,7,-0.059*1.8,1.8,head_width=False,length_includes_head=True,linestyle='dotted')
 startpH=12.5
 ax[0][0].arrow(-0.45-0.059*(startpH-7),startpH,-0.059*(14-startpH),14-startpH,color='w',
         head_width=False,length_includes_head=True,linestyle='dotted')
 ax[1][0].arrow(-0.45-0.059*(startpH-7),startpH,-0.059*(14-startpH),14-startpH,
         head_width=False,length_includes_head=True,linestyle='dotted')
 ax[0][0].annotate('$U_{RHE} = -\Delta$G$_H$',(-0.7-0.059*2,9),rotation=-72,color='w')
 ax[1][0].annotate('$U_{RHE} = -\Delta$G$_H$',(-0.7-0.059*2,9),rotation=-72)

text=ax[0,0].annotate('CH4',(-1.59,7.2),color='w',fontweight='bold',ha='left',fontsize=20)
text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
text=ax[0,0].annotate('Tafel\nslope',(-1.59,10),color='w',ha='left',fontsize=12)
ax[0,0].annotate(r'$\rightarrow \infty$',(-1.29,10.3),color='w',ha='left',fontsize=14)
text=ax[1,0].annotate('H2',(-1.59,7.2),color='w',fontweight='bold',ha='left',fontsize=20)#,bbox=dict(facecolor='w', edgecolor=None))
text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
fig.subplots_adjust(wspace=0.2,hspace=0.01)
#fig.tight_layout()
fig.savefig('TOF.pdf')

exit()
