#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl

from catmaphelpers.catmap_plotter import _make_plots, _plot_model, _plot_CV, post_process_CV, _compute_tafel_prate, plot_tafel_screening, _get_unit_cell_area, _plot_pH, _plot_map, _plot_pH_bar_coverage
from catmaphelpers.catmap_output_handler import _get_data, _sum_catmap_data, _get_rate_control, read_catmap_wdir
from catmaphelpers.extra_tools import make_FEDs

#sys.path.append("../.")
#from post_catmap import read_catmap_wdir


if __name__ == "__main__":
    data_in = pkl.load(open('100_py3.pkl','rb'),encoding='latin1')
    print(data_in.keys())
    data={}
    pots,phs=[],[]
    nsteps=2
    iads=[1,2]

    for dat in data_in['coverage_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data:
            data[pot] = {}
        data[pot][ph] = dat[1]
        if pot not in pots:
            pots.append(np.around(pot,2))
        if ph not in phs:
            phs.append(ph)

    X=np.array(sorted(np.unique(pots)))
    Y=np.array(sorted(phs))

    if nsteps == 1:cols=1
    else: cols=2
    cols=1

    fig,ax=plt.subplots(nsteps,cols)


    for col in range(cols):
      for istep in range(nsteps):
        XY=np.ones((len(X),len(Y)))*0.5
        rate=np.ones((len(X),len(Y)))*0.5
        for ix,x in enumerate(X):
            for iy,y in enumerate(Y):
                            try:
                                if col == 1:
                                    XY[ix][iy]=data[x][y][iads[istep]]/np.sum(data[x][y][:nsteps])
                                else:
                                    rate[ix][iy]=data[x][y][iads[istep]]#/np.sum(data[x][y][:nsteps])
                            except:
                                XY[ix][iy]=np.nan#data[x][y][istep]#/np.sum(data[x][y][:3])
                                rate[ix][iy]=1e-20#np.nan#data[x][y][istep]#/np.sum(data[x][y][:nsteps])
                                print(x,y)
                                pass

        if col == 0:
         if nsteps == 1: plotax=ax
         elif cols == 1: plotax= ax[istep]
         else: plotax= ax[istep][0]


         b = plotax.imshow(rate.T,
                interpolation='bicubic',
                cmap=cm.jet,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-10,
                    vmax=1e0,#)
                    aspect=0.2)#, vmin=-abs(alldata[:,2]).max())

        else:
         a = ax[istep][1].imshow(XY.T,
                interpolation='bicubic',
                cmap=cm.RdYlGn,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],#norm=LogNorm(),#,
                    vmin=0,
                    vmax=1,
                    aspect='auto')#, vmin=-abs(alldata[:,2]).max())

        if istep == nsteps-1:
#            fig.colorbar(a,ax=ax[0][1],fraction=0.146, pad=.04,label='Selectivity')
            if nsteps==1: ax.set_xlabel('U$_{SHE}$ [V]')
            else:
                if cols == 1:
                    ax[istep].set_xlabel('U$_{SHE}$ [V]')
                else:
                    for thisax in ax[istep]:
                        thisax.set_xlabel('U$_{SHE}$ [V]')
        else:
            if cols == 1:
                    ax[istep].set_xticks([])
            else:
                for thisax in ax[istep]:
                    thisax.set_xticks([])

        if cols > 1:
            ax[istep][0].set_ylabel('pH')
            ax[istep][1].set_yticks([])
        else:
            if nsteps == 1:
                ax.set_ylabel('pH')
            else:
                ax[istep].set_ylabel('pH')

        #eli:

#            fig.colorbar(a,ax=ax[:,0],fraction=0.046, pad=0.04,label='Turnover frequency [s$^{-1}$]')
     #fig.colorbar(a,ax=ax[:,0])#,label='Turnover frequency [s$^{-1}$]')
#     fig.subplots_adjust(right=0.8)
#     cbar_ax = fig.add_axes([0.45, 0.15, 0.05, 0.7])
    if cols == 1:
        if nsteps == 1: barax=ax
        else: barax=ax[0]
    else: barax = ax[0][0]
    axins1 = inset_axes(barax,
                    width="100%",  # width = 50% of parent_bbox width
                    height="5%",  # height : 5%
                    loc='lower left',
                     bbox_to_anchor=(0, 1.+nsteps/10., 1, 1),
                  bbox_transform=barax.transAxes)
    fig.colorbar(b, cax=axins1,orientation='horizontal',fraction=0.07,anchor=(1.0,1.0))#cbar_ax)

    #fig.colorbar(b, cax=axins1,fraction=0.07,anchor=(1.0,1.0))#cbar_ax)
    if cols > 1:
     axins2 = inset_axes(ax[0][1],
                    width="100%",  # width = 50% of parent_bbox width
                    height="5%",  # height : 5%
                    loc='lower left',
                    bbox_to_anchor=(0, 1.2, 1, 1),
                    bbox_transform=ax[0][1].transAxes)
     fig.colorbar(a, cax=axins2,orientation='horizontal',fraction=0.07,anchor=(1.0,1.0))#cbar_ax)
     ax[0][1].annotate('Selectivity',(-1.45,14),color='k',bbox=dict(facecolor='w', edgecolor=None))

    barax.annotate('Coverage',(-1.6,14),color='k',bbox=dict(facecolor='w', edgecolor=None))
    fig.subplots_adjust(wspace=0.2,hspace=0.01)
#    fig.tight_layout()
    fig.savefig('Coverage.pdf')



#        plt.close()



    exit()
