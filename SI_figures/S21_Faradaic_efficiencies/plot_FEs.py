import numpy as np
from matplotlib import pyplot as plt
import matplotlib
#matplotlib.rc('text', usetex=True)
#matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
import matplotlib.pyplot as plt
plt.rcParams["font.family"] = "Times New Roman"
plt.rc('axes', labelsize=22)    # fontsize of the x and y labels
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
markersize=10
from matplotlib.ticker import FormatStrFormatter

fig,allax=plt.subplots(3,1,figsize=(15,10))
for ipH,pH in enumerate(['3','7','13']):
    dat=np.loadtxt(f'data_lei_COR_pH{pH}.txt')
    print(dat)
    ax=allax[ipH]
    ax2=ax.twinx()

    ax.plot(np.nan,np.nan, 's',color='darkgreen',label='C$_{2+}$')
    ax.plot(np.nan,np.nan, 's',color='red',label='CH$_4$')
    ax.plot(np.nan,np.nan, 's',color='b',label='HER')
    barwidth=0.02
    edgecolor='k'
    for potline in dat:
        totcurr=np.sum(potline[2:])/100
        ax2.plot(potline[1],totcurr,'ok')
        ax.bar(potline[1],potline[2]/totcurr,width=barwidth,color='darkgreen',edgecolor=edgecolor)
        ax.bar(potline[1],potline[3]/totcurr,bottom=potline[2]/totcurr,width=barwidth,color='r',edgecolor=edgecolor)
        ax.bar(potline[1],potline[4]/totcurr,bottom=(potline[2]+potline[3])/totcurr,width=barwidth,color='b',edgecolor=edgecolor)
    #    plt.bar(potline[1],potline[3])
    #    plt.bar(potline[1],potline[4])
    ax.set_title(f'pH {pH}',fontsize=20)
    ax.set_ylabel(r'Faradaic efficiency (%)')
    ax.set_xlabel(r'U vs SHE / V')
    ax2.set_ylabel('Total current density / (mA/cm$^2$)')
    ax.legend()
#plt.tight_layout()
#plt.subplots_adjust(left=0.1,
#                    bottom=0.15,
#                    right=0.9,
#                    top=0.9,
#                    wspace=1.0,
#                    hspace=0.4)
allax[0].set_position([0.1,0.55,0.3,0.35])
allax[1].set_position([0.55,0.55,0.3,0.35])
allax[2].set_position([0.2,0.1,0.6,0.35])

#plt.show()
plt.savefig('FEs.pdf')


