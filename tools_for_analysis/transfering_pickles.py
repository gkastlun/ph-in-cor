#!/usr/bin/env python

import sys, os
import numpy as np
#from cathelpers.misc import _write_pickle_file
import mpmath
import pickle

def handle_list(li):
    for i in range(0,len(li)):
        if type(li[i]) == mpmath.ctx_mp_python.mpf:
            li[i] = float(li[i])
        elif type(li[i]) == list:
            li[i] = handle_list(li[i])
    return(li)

def handle_dict(dd):
    for d in dd:
        if type(dd[d]) == list:
            dd[d] = handle_list(dd[d])
        elif type(dd[d]) == dict:
            dd[d] = handle_dict(dd[d])
    return(dd)

def convert_pkl(fpkl):
    pklmod = '.'.join(fpkl.split('.')[:-1])+'_py3.pkl'
    if not os.path.isfile(pklmod) or \
        os.path.getmtime(fpkl) > os.path.getmtime(pklmod):
        print('converting %s to %s'%(fpkl,pklmod))
        pickle_file = open(fpkl,'rb')
        data = pickle.load(pickle_file)
        pickle_file.close()
        data = handle_dict(data)
        _write_pickle_file(pklmod,data)

def search_pkl_files(ftree):
    # pack pkl in ftree
    fpkls = [ftree+"/"+f for f in os.listdir(ftree) if f[-4:] == '.pkl' and f[-8:] != '_py3.pkl']
    for f in fpkls:
        convert_pkl(f)
    # find subfolders to search for packing
    fsub = [ftree+"/"+f for f in os.listdir(ftree) if os.path.isdir(ftree+'/'+f)]
    for f in fsub:
        search_pkl_files(f)

def _write_pickle_file(filename,data,py2=False):
    ptcl = 0
    if py2:
        filename = filename.split('.')[0]+'_py2.'+filename.split('.')[1]
        ptcl = 2
    output = open(filename, 'wb')
    pickle.dump(data, output, protocol=ptcl)
    output.close()

if __name__ == "__main__":

    convert_pkl(sys.argv[1])
