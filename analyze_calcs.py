from ase.io import read,write
from scipy.optimize import curve_fit
import matplotlib
import pickle
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
import os,sys
import numpy as np
from tools_for_analysis.general_tools import get_reference_energies,lin_fun,quad_fun
from tools_for_analysis.sjm_analyse_tools import *
from ase import units

#matplotlib.rc('text', usetex=True)
#matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
plt.rcParams["font.family"] = "Times New Roman"
plt.rc('axes', labelsize=28)    # fontsize of the x and y labels
plt.rcParams['xtick.labelsize'] = 18
plt.rcParams['ytick.labelsize'] = 18
plt.rcParams['figure.figsize'] = (7,5)
markersize=10

from matplotlib.ticker import FormatStrFormatter
if len(sys.argv) > 1:
    facets = [sys.argv[1]]
else:
    facets=['100','111','211','110']

if len(sys.argv) > 2:
    output_potential = float(sys.argv[2])
else:
    output_potential = 4.40

potentials=np.arange(2.2,4.45,0.05)
pkloutfile='calculated_energetics.pkl'
#output_potential=4.40
home=os.getcwd()
barrier_path=home+'/../'

outfile='formation_energies_for_catmap.txt'
catbasein='tools_for_analysis/catinput_base_freeen.txt'
alldata={}

if pkloutfile not in os.listdir():
        alldata = get_data(potentials,facets,
                basepath='structures/endstates',
                include_barriers=True,
                plot=False,
                outfile=pkloutfile,
                alldata=alldata)
else:
    import pickle
    alldata = pickle.load(open(pkloutfile,'rb'))


write_catinput(alldata,facets,output_potential,outfile)

