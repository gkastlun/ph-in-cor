import sys,os
sys.path.append('../../../tools_for_analysis')
from FED_tools import plot_FED_with_barrier,read_calculated_data
from intermediates_dict import *

if len(sys.argv) > 1:
    facets= [sys.argv[1]]#['100']
else:
    facets= ['111','100','211','110']

start_from_pkl=1

def main():
    alldata=read_calculated_data(None,facets,start_from_pkl,
            pklfile='../../../calculated_energetics.pkl')

    plot_FED_with_barrier(ads_and_electron,'100',
                [['CO+CO','COH+CO','OCCOH'],['CO+CO','CHO+CO','OCCHO'],['2CO','OCCO','OCCOH']],
                potentials=[3.0],pH=[13],ylim=[-1.2,1.3],
                view=False,annotate_intermediates=False,
                proton_donor='base',check_barriers_with_line=False,
                title='',figsize=(12,6),
                outdir='.',outformat='png')

    plot_FED_with_barrier(ads_and_electron,'100',
                [['2CO','OCCO','OCCOH']],
                potentials=[3.5,3.0,2.5],pH=[13],ylim=[-1.2,1.3],
                view=False,annotate_intermediates=False,
                proton_donor='base',check_barriers_with_line=False,
                title='',figsize=(12,6),
                outdir='.',outformat='png')


main()
