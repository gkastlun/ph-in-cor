import numpy as np
from catmap import ReactionModel
from catmap import analyze
from string import Template
import os,sys
import pickle
from multiprocessing import Process

include_rate_control = 0
plot_all_rates=0
plot_production_rate=0
plot_coverage=0
#pH=[sys.argv[1]]
home=os.getcwd()

#if home.split('/')[-1] != 'MKM':
if home.split('/')[-1] != 'test':
    print('The model should be run in figures/fig3_C2pathways/MKM' )
    exit()

def main():
    for rundir in os.listdir(home):
        if rundir[:9] != 'increase_': continue
        p = Process(target=run_model, args=(rundir,))
        p.start()
    p.join()


def run_model(rundir):
        runpath=os.path.join(home,rundir)
        os.chdir(runpath)
        print('Running in %s with pH %s and interaction %s'%(os.getcwd(),sys.argv[1],sys.argv[2]))
        model = ReactionModel(setup_file = home+'/../scripts/COR.mkm')

        model.output_variables += ['production_rate','rate','coverage']
        if include_rate_control:
            model.output_variables += ['rate_control']
        model.run()
        if not float(sys.argv[1])%1:
            os.system('python ~/scripts/transfering_pickles.py %s.pkl'%sys.argv[1])

#vm = analyze.VectorMap(model)


if 0:
     if plot_all_rates:
         vm.plot_variable = 'rate'
         vm.log_scale = True
         vm.min = 1e-20
         vm.max = 1e+3
         fig = vm.plot(save=False)
         fig.savefig('rate'+facet+'.pdf')

     if plot_production_rate:
         vm.plot_variable = 'production_rate'
         vm.log_scale = True
         vm.colorbar = True
         vm.min = 1e-5
         vm.max = 1e+5
         fig = vm.plot(save=False)
         fig.savefig('production_rate'+facet+'.pdf')

     if plot_coverage:
         vm = analyze.VectorMap(model)
         vm.log_scale = False
         vm.unique_only = False
         vm.plot_variable = 'coverage'
         vm.min = 0
         vm.max = 1
         fig = vm.plot(save=False)
         fig.savefig('coverage'+facet+'.pdf')

         vm = analyze.VectorMap(model)
         vm.log_scale = True
         vm.unique_only = False
         vm.plot_variable = 'coverage'
         vm.min = 1e-20
         vm.max = 1
         fig = vm.plot(save=False)
         fig.savefig('coverageLog'+facet+'.pdf')

     if include_rate_control:
         mm = analyze.MatrixMap(model)
         mm.plot_variable = 'rate_control'
         mm.log_scale = False
         mm.min = -2
         mm.max = 2
         mm.plot(save='rate_control.pdf')

if __name__ == '__main__':
    main()
