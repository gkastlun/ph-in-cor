#!/usr/bin/env python
import pickle
import sys
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from catmap.model import ReactionModel
from matplotlib.pyplot import cm
from matplotlib import rc
from matplotlib.ticker import NullFormatter
from pylab import *
from matplotlib.font_manager import FontProperties

class Object(object):
    pass

mathtext_prop = {'fontset' : 'custom',
                 'it' : 'serif:italic',
                 'sf' : 'Helvetica:bold',
                 'cal' : 'serif:italic:bold'}
rc('font', family='serif', serif='Helvetica')
rc('mathtext', **mathtext_prop)
rc('xtick', labelsize=15)
rc('ytick', labelsize=15)

def get_data(pickle_file):
    a = pickle.load(open(pickle_file,'rb'))
    data = Object()
    #COVERAGES
    data.coverage_names = model.output_labels['coverage']
    coverage_map = np.array(a['coverage_map'])
    data.voltage = []
    scaler_array = coverage_map[:,0]
    for s in scaler_array:
        data.voltage.append(s[0])
    coverage_mpf = coverage_map[:,1]
    data.coverage = np.zeros((len(coverage_mpf),len(data.coverage_names)))
    for i in range(0,len(coverage_mpf)):
        for j in range(0,len(coverage_mpf[i])):
            float_rate = float(coverage_mpf[i][j])
            data.coverage[i][j]=float_rate
    #PRODUCTION RATES
    data.prod_names = model.output_labels['production_rate']
    production_rate_map = np.array(a['production_rate_map'])
    production_rate_mpf = production_rate_map[:,1]
    data.production_rate = np.zeros((len(production_rate_mpf),len(data.prod_names)))
    data.voltage = np.zeros((len(production_rate_mpf),1))
    for i in range(0,len(production_rate_mpf)):
        data.voltage[i][0] = production_rate_map[:,0][i][0]
        for j in range(0,len(data.prod_names)):
            float_rate = float(production_rate_mpf[i][j])
            data.production_rate[i][j]=float_rate
    #RATES
    data.rate_names = model.output_labels['rate']
    rate_map = np.array(a['rate_map'])
    rate_mpf = rate_map[:,1]
    data.rate = np.zeros((len(rate_mpf),len(data.rate_names)))
    for i in range(0,len(rate_mpf)):
        for j in range(0,len(rate_mpf[i])):
            float_rate = float(rate_mpf[i][j])
            data.rate[i][j]=float_rate
    return data

# plots
color_list = ['#BF3F3F','#F47A33','#FFE228','#7FBF3F','#3FBFBF','#3F7FBF','#3F3FBF','#7F3FBF','#BF3F7F','#BF3F3F','#333333','#000000']
pH = ['3','7','13']
voltages = [(-1.8,-0.9)]
# all pH
home=os.getcwd()
for dirs in ['increase_0.0','increase_-0.2','increase_0.2']:
 for i in range(0,len(pH)):
    os.chdir(os.path.join(home,dirs))
    j = pH[i]
    color_var = color_list[i]
    print('pH = '+j)

    # all voltages
    for i, item in enumerate(voltages):
        lower_, upper_ = item
        model = ReactionModel(setup_file = home+'/scripts/COR.log')
        pickle_file = str(j)+'.pkl'
        phX = get_data(pickle_file)

        # sum over relevant products
#        products = ['C2H4_g','OH_g']
        products = ['C2H4_g']
        for k, product in enumerate(products):
            idx=phX.prod_names.index(product)
            if k == 0:
                data=np.column_stack((phX.voltage, phX.production_rate[:,idx]))
            else:
                data_var=np.column_stack((phX.voltage, phX.production_rate[:,idx]))
                data[:,1] += data_var[:,1]
            print('pH = '+str(j)+'  ITERATION = '+str(k)+'  PRODUCT = '+product)
            print('TOTAL TOF =' + str(np.sum(data[:,1])))

        # merge data
        pol_file = 'TOF_pH_'+j+'.list'
        x=data[np.argsort(data[:, 0])][:,0]
        y=data[np.argsort(data[:, 0])][:,1]
        if i == 0:
            with open(pol_file,'w') as f:
                np.savetxt(f, np.array([x,y]).T, delimiter=' ')
        else:
            with open(pol_file,'a+') as f:
                np.savetxt(f, np.array([x,y]).T, delimiter=' ')

    # plot
    pol_file = 'TOF_pH_'+j+'.list'
    data = np.loadtxt(pol_file)
    data = data[data[:,0].argsort()]
    x = data[:,0]
    y = data[:,1]
    plt.plot(x,y, 'o-', color = color_var, linewidth=1.2, label='pH '+j,markeredgecolor='k')

 fig = plt.figure(1, figsize=(7.5, 5.5))
 ax = fig.add_subplot(1,1,1)
 leg = plt.legend(loc='upper right', ncol=1, prop={'size':12}, fancybox=True, shadow=False)
 leg.get_frame().set_facecolor('none')
 leg.get_frame().set_linewidth(0.0)
 plt.gcf().subplots_adjust(bottom=0.18, left=0.18)
 plt.yscale('log')
 plt.xlabel(r'U (V vs. SHE)', fontsize=16)
 plt.ylabel(r'i (mA/cm$^{2}$)', fontsize=16)
 fig_name = 'TOF.pdf'
 fig.savefig(fig_name)
 plt.close()
