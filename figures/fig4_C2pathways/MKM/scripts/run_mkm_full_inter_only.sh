home=`pwd`
if [ -z $1 ];then
    echo pH needs to be given to the script
    exit
fi

# With $2 a single increase can be started

for pH in $1
do
        if [ -f mkm_$pH.log ]; then
            rm mkm_$pH.log
        fi

        ramp=1
        for i in $ramp
        do
            echo Running pH $pH and interaction strength $i
            if [ -z $2 ];then
                python $home/scripts/run_mkm.py $pH $i > mkm_$pH.log
            else
                python $home/scripts/run_mkm.py $pH $i $2 > mkm_$pH.log
            fi
        done
done
