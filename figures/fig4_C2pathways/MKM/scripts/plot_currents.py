import sys,os
from catmaphelpers.catmap_plotter import _make_plots, _plot_model, _plot_CV, post_process_CV, _compute_tafel_prate, plot_tafel_screening, _get_unit_cell_area, _plot_pH, _plot_map, _plot_pH_bar_coverage
import matplotlib
from matplotlib import patheffects
from matplotlib import pyplot as plt
import numpy as np
from general_tools import lin_fun
from scipy.optimize import curve_fit
from ase.units import C,mol

#matplotlib.rc('text', usetex=True)
#matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
plt.rcParams["font.family"] = "Times New Roman"
plt.rc('axes', labelsize=30)    # fontsize of the x and y labels
plt.rcParams['xtick.labelsize'] = 25
plt.rcParams['ytick.labelsize'] = 25
#plt.rcParams['markers.markersize'] = 7

potscales=['RHE','SHE']
currents=['partcurdens','turnover','molprod']
#currents=['molprod']
products=['Ethylene','Methane','Hydrogen']
#products=['C2+','C1']
surface_ratio=0.18
colors={3:'darkblue',7:'lightgreen',13:'brown'}
limited_range=[-1.4,-1.15]
markersize=9

def main():
    data={}
    fig,ax = plt.subplots(ncols=1,figsize=(12,7))
    potscale=1

#    C2,C1,HER,fitdata = get_experimental_data(['data_lei_COR_pH3.txt','data_lei_COR_pH7.txt','data_lei_COR_pH13.txt'])
    AperSite = _get_unit_cell_area(a_input=3.6149)
    ec = 1.602176e-19; A2cm = 1e-8
    tof2cur = (1e3*ec/(A2cm*A2cm))/AperSite['100']
    tof2cur = {'C2+':8*tof2cur, 'Ac':4*tof2cur, 'tot':1.0,'EtOH':8*tof2cur,'C1':6*tof2cur,'HER':2*tof2cur,'C2H4':8*tof2cur}
    #Loop over pH
    for iph,ph in enumerate([3,7,13]):
     data[ph]={}
 #    expdat=C2[ph]['cv']
 #    if ph == 3:
 #        ax.plot(np.nan,np.nan,'o',markerfacecolor='w',markeredgecolor='k',label='Theory')
 #        ax.plot(np.nan,np.nan,'d',markerfacecolor='w',markeredgecolor='k',label='Experiment')
     ax.plot(np.nan,np.nan,'s',color=colors[ph],label='pH %i'%ph)
     for increase in ['-0.2','0.0','0.2']:
      data[ph][increase]=np.loadtxt('increase_%s/TOF_pH_%s.list'%(increase,ph))
      data[ph][increase][:,1]*=tof2cur['C2H4']

     ax.plot(data[ph]['0.0'][::2,0],np.log10(data[ph]['0.0'][::2,1]*surface_ratio),'o-',
                        markeredgecolor='k',color=colors[ph],markersize=markersize)
     ax.plot(data[ph]['0.2'][:,0],np.log10(data[ph]['0.2'][:,1]*surface_ratio),'--',
                        markeredgecolor='k',color=colors[ph],markersize=markersize)
     ax.plot(data[ph]['-0.2'][:,0],np.log10(data[ph]['-0.2'][:,1]*surface_ratio),'--',
                        markeredgecolor='k',color=colors[ph],markersize=markersize)
     ax.fill_between(data[ph]['-0.2'][:,0],np.log10(data[ph]['-0.2'][:,1]*surface_ratio),np.log10(data[ph]['0.2'][:,1]*surface_ratio),facecolor=colors[ph],alpha=0.1)

#     if ph == 13:
#         coeff,dummy = curve_fit(lin_fun,expdat[:4,1],np.log10(expdat[:4,2]))
#         ax[0].plot(expdat[:4,1],10**(coeff[0]*expdat[:4,1]+coeff[1]),color=colors[ph])
#         ax[0].plot(expdat[:4,1],expdat[:4,2],'d',color=colors[ph],markeredgecolor='k',markersize=markersize)
#         ax[0].plot(expdat[4:,1],expdat[4:,2],'d',color=colors[ph],markersize=markersize,markerfacecolor='w')
#     else:
#         coeff,dummy = curve_fit(lin_fun,expdat[:,1],np.log10(expdat[:,2]))
#         ax[0].plot(expdat[:,1],10**(coeff[0]*expdat[:,1]+coeff[1]),color=colors[ph])
#         ax[0].plot(expdat[:,1],expdat[:,2],'d',color=colors[ph],markeredgecolor='k',markersize=markersize)
     #print(ph,1000/coeff)
#     ax[1].annotate('%i mV/dec'%(1000/coeff[0]),(-1.3,10**-iph),fontsize=24,rotation=-12,color=colors[ph]).draggable()

    derivs=calculate_derivs(data)
    #ax[0].annotate('66 mV/dec',(-1.3,1e-3),fontsize=24,rotation=-35).draggable()
    #ax[0].annotate('171 mV/dec',(-1.6,3e3),fontsize=24,rotation=-12).draggable()
    ax.set_xlim([-1.79,-1.01])
    ax.set_xticks([-1.7,-1.5,-1.3,-1.1])
    ax.set_ylim([-4,5])
    ax.set_ylabel('log( j$_{C_{2+}}$ / mA/cm$^2$)')
    ax.set_xlabel('U$_{SHE}$ / V')


    text=ax.annotate('66 mV/dec',(-1.3,0.),fontsize=22,fontweight='bold',rotation=-33,color='k')
    text=ax.annotate('171 mV/dec',(-1.75,4.0),fontsize=22,fontweight='bold',rotation=-13,color='k')
    text=ax.annotate('RLS: OC-CO',(-1.76,3.3),fontsize=22,fontweight='bold',rotation=-13,color='k')
    text=ax.annotate('RLS: OCCO-H',(-1.37,-0.5),fontsize=22,fontweight='bold',rotation=-33,color='k')
    text=ax.annotate('pH 3',(-1.1,-0.5),fontsize=22,fontweight='bold',rotation=-13,color='darkblue')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                   patheffects.Normal()])
    text=ax.annotate('pH 7',(-1.12,-1.45),fontsize=22,fontweight='bold',rotation=-25,color='lightgreen')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                   patheffects.Normal()])
    text=ax.annotate('pH 13',(-1.12,-3.5),fontsize=22,fontweight='bold',rotation=-13,color='brown')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                   patheffects.Normal()])



    #ax.legend()
#    ax.set_yscale('log')
      #legend.get_frame().set_alpha(None)
      #axs[0].annotate(products[iprod],(-1.15,0.5),fontsize=30,fontweight='bold')
    #fig.figsize=(12,6)
    #fig2.suptitle('Theory',x=0.54,fontsize=30)
    fig.tight_layout()
    plt.tight_layout()
    #fig.savefig('Current_vs_pot_theo.pdf')
    fig.savefig('Current_vs_pot_theo.png',dpi=1000)
#    plt.show()
    plt.close()

    #fig,ax = plt.subplots(figsize=(12,5))
    #for ph in [3,7,13]:
    # ax.plot(expdat[:,1],expdat[:,2],'d',color=colors[ph],markeredgecolor='k',markersize=markersize)

    print('-'*13)

def calculate_derivs(data):
    for ph in data:
        dat=data[ph]['0.0']
        for idat in range(len(dat)-1):
            d=(dat[idat+1][0]-dat[idat][0])/\
                    (np.log10(dat[idat+1][1])-np.log10(dat[idat][1]))
            #print(ph,dat[idat+1][0],d)
    return None

def get_experimental_data(infiles):
    alldata={}
    for infile in infiles:
        pH = infile.split('.')[0].split('_')[-1]
        pH = int(pH[2:])
        alldata[pH] = np.loadtxt(infile, comments='#')
    C1={}
    C2={}
    HER={}
    for iprod,prod in enumerate([C2,C1,HER]):
      for pH in alldata.keys():
        prod[pH]={}
        prod[pH]['cv']=[]
        for dat in alldata[pH]:
            prod[pH]['cv'].append([dat[0],dat[1],dat[iprod+2]])
        prod[pH]['cv']=np.array(prod[pH]['cv'])
      trim_array(prod)
      #print(prod[pH]['cv'][0])
      for pH in alldata.keys():
        prod[pH]['fits']={}
        for potscale in range(len(potscales)):
            for icurr in range(len(currents)):
                if pH == 13:
                    fitdata=prod[pH]['cv'][:4]
                else:
                    fitdata=prod[pH]['cv']
                #prod[pH]['fits']['%s_%s'%(currents[icurr],potscales[potscale])]=linear_fit(prod[pH]['cv'],potscale,icurr,pH,logscale='y')
#                prod[pH]['fits']['%s_%s'%(currents[icurr],potscales[potscale])]=linear_fit(fitdata,potscale,icurr,pH,logscale='y')
        #print(prod[pH]['fits'])
    return C2,C1,HER,prod

def trim_array(data):
    for pH in data.keys():
        data_non_zero=len(np.trim_zeros(data[pH]['cv'][:,2].copy()))
        data[pH]['cv']=data[pH]['cv'][-data_non_zero:]




def fit_Tafel(alldata):
    pass
main()
