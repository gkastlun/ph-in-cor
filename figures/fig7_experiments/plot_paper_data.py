#!/usr/bin/env python

import sys, os
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle

import matplotlib.pyplot as plt
#from rtools.helpers.matplotlibhelpers import tumcolors as tumcs
#import rtools.helpers.matplotlibhelpers as matplotlibhelpers
from scipy.constants import golden_ratio, inch
from matplotlib import rcParams
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

CRED = '\033[91m'
CEND = '\033[0m'

def _set_plotting_env(width=3.37,height=3.37/ golden_ratio *1.5/2,\
                    lrbt=[0.135,0.80,0.25,0.95],fsize=9.0,font='helvetica'):
    # set plot geometry
    rcParams['figure.figsize'] = (width, height) # x,y
    rcParams['font.size'] = fsize
    rcParams['figure.subplot.left'] = lrbt[0]  # the left side of the subplots of the figure
    rcParams['figure.subplot.right'] = lrbt[1] #0.965 # the right side of the subplots of the figure
    rcParams['figure.subplot.bottom'] = lrbt[2] # the bottom of the subplots of the figure
    rcParams['figure.subplot.top'] = lrbt[3] # the bottom of the subplots of the figure

    rcParams['xtick.top'] = True
    rcParams['xtick.direction'] = 'in'
    rcParams['ytick.right'] = True
    rcParams['ytick.direction'] = 'in'

    rcParams['legend.fancybox'] = False
    #rcParams['legend.framealpha'] = 1.0
    rcParams['legend.edgecolor'] = 'k'
    if font != 'None':
        matplotlibhelpers.set_latex(rcParams,font=font) #poster

def _compute_partial_jecsa(data, adsorbate, maxpH=100.,minpH=0):
    data = deepcopy(data)
    out = {}
    for k in data:
        if data[k]['roughness'] != 'None' and adsorbate in data[k]:
            # transform total current into j-ecsa
            try:
                pj = _partial_jecsa(data[k], adsorbate)
            except:
                continue
            else:
                ind = np.where(pj != 0.0)[0]
                pj = np.absolute(pj[ind,:]) # correct for current definition

                urhe = data[k]['V_RHE'][ind,:]
                ushe = deepcopy(urhe)
                ushe[:,0] -= 0.059 * float(data[k]['pH'])
            # multiply with FE for chosen product

        #    pj = np.array([data[k][adsorbate][i,:]*j_ecsa[i] for i in range(len(j_ecsa))])
        #    ind = np.where(pj != 0.0)[0]
        #    pj = np.absolute(pj[ind,:]) # correct for current definition
        #    urhe = data[k]['V_RHE'][ind,:]
        #    ushe = deepcopy(urhe)
        #    ushe[:,0] -= 0.059 * float(data[k]['pH'])
            if minpH <= float(data[k]['pH']) <= maxpH:
                out.update({k:{'U_RHE':urhe, 'U_SHE':ushe, 'j_partial':pj, 'cell':data[k]['cell'], \
                    'mode':data[k]['mode'], 'pH':data[k]['pH'], 'roughness':data[k]['roughness'],
                    'folder':data[k]['folder']}})
        else:
            print("%s does not contain roughness or %s"%(k, adsorbate))
    return(out)

def _partial_jecsa(dat, adsorbate):
    j_ecsa = dat['I(mA/cm2)'] / float(dat['roughness'])
    # multiply with FE for chosen product
    pj = np.array([dat[adsorbate][i,:]*j_ecsa[i] for i in range(len(j_ecsa))])
    return(pj)

# global keywords
c2a = ['Ethylene', 'Acetate', 'Ethanol', 'Acetaldehyde', 'n-propanol',\
        'Allyl-Alcohol','Acetone','Ethane','Propionaldehyde','Allyl-alcohol',\
        'Hydroxyacetone','Ethylene-Glycol','Glycolaldehyde','Ethylene-glycol']
c1a = ['Methane', 'Methanol', 'Formate']


def _add_c1_c2(data):
    for k in data:
        # check for new adsorbates
        for e in data[k]:
            if e not in c2a+c1a+['Hydrogen','V_RHE','mode','pH','catalyst','roughness','cell','doi','I(mA/cm2)','CO']:
                print(e)
        # sum FE of C1
        c1 = np.zeros((data[k]['V_RHE'].shape))
        for e in c1a:
            if e in data[k]:
                c1[:,0] += data[k][e][:,0]
                c1[:,1] += data[k][e][:,1]**2.0
        c1[:,1] = np.sqrt(c1[:,1])
        data[k].update({"C1":c1})
        # sum FE of C2s
        c2 = np.zeros((data[k]['V_RHE'].shape))
        for e in c2a:
            if e in data[k]:
                c2[:,0] += data[k][e][:,0]
                c2[:,1] += data[k][e][:,1]**2.0
        c2[:,1] = np.sqrt(c2[:,1])
        data[k].update({"C2+":c2})
    return(data)

def _load_pickle_file(filename,py23=False):
    pickle_file = open(filename,'rb')
    if py23:
        data = pickle.load(pickle_file, encoding='latin1')
    else:
        data = pickle.load(pickle_file)
    pickle_file.close()
    return(data)

def _write_pickle_file(filename,data,py2=False):
    ptcl = 0
    if py2:
        filename = filename.split('.')[0]+'_py2.'+filename.split('.')[1]
        ptcl = 2
    output = open(filename, 'wb')
    pickle.dump(data, output, protocol=ptcl)
    output.close()

def plot_partial_current_densities(filename, data, pot, clr='data'):
    _set_plotting_env(width=3.37*1.3,height=3.37,\
                   lrbt=[0.15,0.75,0.13,0.98],fsize=7.0,font='None')

   #vcolors = [tumcs['tumorange'],tumcs['tumgreen'],tumcs['tumblue'],\
   #    tumcs['tumlightblue'],tumcs['acc_yellow'],tumcs['diag_violet']]
    if clr == 'data':
        ks = list(data.keys())
    elif clr == 'pH':
        ks = (np.unique([float(data[k]['pH']) for k in data])).tolist()
    colors = plt.cm.jet(np.linspace(0,1,len(ks)))
    kclr = {ks[i]:colors[i] for i in range(len(ks))}
    lss = {'COR':'-', 'CO2R':'--'}
    mks = {'H-cell':'o', 'GDE':'x'}


    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    for k in data:
        x1 = data[k][pot][:,0]; xerr1 = data[k][pot][:,1]
        #x2 = data[k]['U_RHE'][:,0]; xerr2 = data[k]['U_RHE'][:,1]
        if np.all(xerr1 == 0.0):
            xerr1 = None
       #if np.all(xerr2 == 0.0):
       #    xerr2 = None
        kc = k
        if clr == 'pH':
            kc = float(data[k]['pH'])

        y = data[k]['j_partial'][:,0]; yerr = data[k]['j_partial'][:,1]
        if np.all(yerr == 0.0):
            yerr = None
        #if True:
        if data[k]['cell'] == 'GDE' and data[k]['mode'] == 'COR':
            ax1.errorbar(x1, y, xerr=xerr1, yerr=yerr, color=kclr[kc], ls=lss[data[k]['mode']], marker=mks[data[k]['cell']], markersize=3)
            ax2.errorbar(x1, y, xerr=xerr1, yerr=yerr, color=kclr[kc], ls=lss[data[k]['mode']], marker=mks[data[k]['cell']], markersize=3)

    # legend
    for l in lss:
        ax1.plot(np.nan, np.nan, color='k', ls=lss[l], label=r'%s'%l)
    for m in mks:
        ax1.plot(np.nan, np.nan, color='k', ls='None', marker=mks[m], label=r'%s'%m)
    for k in ks:
        if data[k]['cell'] == 'GDE' and data[k]['mode'] == 'COR':
            ax1.plot(np.nan, np.nan, color=kclr[k], label=r'%s'%k)
    ax1.legend(loc=1,prop={'size':4},bbox_to_anchor=(1.45, 1.))
    ax2.set_zorder(-1)
    #ax2.set_ylim(1e-5,10)
    ax2.set_ylim(1e-5,100)

    # axis labels
    ax1.set_xticklabels([])
    ax2.set_yscale('log')
    ax2.set_xlabel('U vs. %s (V)'%pot.split('_')[1])
    ax2.set_ylabel('$j_{\mathrm{ECSA}}$ (mA/cm$^2$)')
    ax2.yaxis.set_label_coords(-0.15, 1.05)
    plt.subplots_adjust(hspace=0.05)
    #plt.show()
    #matplotlibhelpers.write(filename+'_'+pot,folder='output',\
#        write_info=False,write_png=False,write_pdf=True,write_eps=False)
    writefig(filename+'_'+pot,folder='output')


def writefig(filename, folder='output',  write_eps=False):
    """
      wrapper for creating figures
      Parameters
      ----------
      filename : string
        name of the produced figure (without extention)
      folder : string
        subfolder in which to write the figure (default = output)
      write_eps : bool
        whether to create an eps figure (+ the usual pdf)
    """
    # folder for output
    if not os.path.isdir(folder):
        os.makedirs(folder)
    fileloc = os.path.join(folder, filename)
    print("writing {}".format(fileloc+'.pdf'))
    plt.savefig(fileloc+'.pdf')
    if write_eps:
        print("writing {}".format(fileloc+'.eps'))
        plt.savefig(fileloc+'.eps')
    plt.close(plt.gcf())

#def create_input_pkl(folder):
#    scripts = [ script for script in os.listdir(folder)
#            if script[-3:] == '.py']
#    basedir=os.getcwd()
#    if len(scripts) > 1:
#        print(CRED+"Careful! There's more than one python script in %s"%folder+CEND)
#    #os.chdir(basedir+'/'+folder)
#    os.system('python3 %s'%scripts[0])
    #os.chdir(basedir)

def load_pkl_data(folders,data=None):
    if data is None:
        data={}
    # collect all data
    for f in folders:
        if f not in os.listdir(os.getcwd()):
            print(CRED+'Couldnt find the folder %s, check the spelling!'%f+CEND)
            continue

        pklfile = "%s/%s.pkl"%(f,f)
        if pklfile.split('/')[-1] not in os.listdir(f):
            print('\033[92mCouldnt find input pkl in %s, creating it\033[0m'%f)
            prep_exp_data(f)

        dat = _load_pickle_file(pklfile)

        tag = f.split('_')[0][:5]
        for k in dat:
            print(k)
            if k[:5] != 'Cu-oh': # faulty potential
                data.update({tag+'_'+k:dat[k]})
                data[tag+'_'+k]['folder']=f
    return data


def _read_header(txtfile):
    with open(txtfile, 'r') as infile:
        hline = infile.readlines()[0][1:]
    return(hline)

def _read_meta_data(mfile):
    with open(mfile, 'r') as infile:
        lines = infile.readlines()
    dat = {}
    for l in lines:
        e = [s.strip() for s in l.split('=')]
        dat.update({e[0]:e[1]})
    return(dat)

def prep_exp_data(study):
    # read main data
    # no std std deviation
    full_data2 = {}
    nfiles,mfiles=[],[]
    #study = _check_execution_dir(f)
    print(study)
    for files in os.listdir(study):
        if files.split('_')[0] == 'raw':
            if files.split('_')[1][:4] == 'data':
                nfiles.append(study+'/'+files)
        elif files.split('_')[0] == 'meta':
            if files.split('_')[1][:4] == 'data':
                mfiles.append(study+'/'+files)
    if len(nfiles) != len(mfiles):
        print('The number of experimental and meta data files in %s do not\
               match!  Check!'%(os.getcwd().split('/')[-1]))


    for nfile in sorted(nfiles):
        #nfile = 'raw_data_%i.txt'%i
        keys = _read_header(nfile).split()
        val = np.loadtxt(nfile)
        if len(val.shape) == 1:
            val=np.array([val])

        if 'I(mA/cm2)' not in keys:
            print(study)
            if 'FE_tot' in keys:
                # scaled by FE tot and FE of products
               jsum = val[:,2:].sum(axis=1)
               jtot = jsum/(val[:,1]/100.)
               for j in range(len(jtot)):
                   val[j,2:] /= jtot[j]
               data2 = {keys[k]:np.zeros((val.shape[0], 2)) for k in range(len(keys)) if keys[k] != 'FE_tot'}
               for k in data2:
                  data2[k][:,0] = val[:,keys.index(k)]
               v = np.zeros((len(jtot),2)); v[:,0] = jtot
               data2.update({'I(mA/cm2)':v})
            else:
               continue
        else:
        #if  1:
            val[:,2:] /= 100. # correct for percentage

            # prep data
            data2 = {keys[i]:np.zeros((val.shape[0], 2)) for i in range(len(keys))}
            for k in data2:
                data2[k][:,0] = val[:,keys.index(k)]

        mfile = '/'.join([nfile.split('/')[0],'meta'+nfile.split('/')[1].lstrip('raw')])
        #print(nfile,mfile,os.listdir())
        #print(data2)
        if mfile.split('/')[-1] in os.listdir(study):
            mdat = _read_meta_data(mfile)
            data2.update(mdat)
            data2['folder']=study

            # sort into one file
            full_data2.update({mdat['catalyst']+'_pH'+str(mdat['pH']):data2})

    #print(full_data2)
    #oo
    # save data
    #name = os.getcwd().split('/')[-1]
    _write_pickle_file(study+"/%s.pkl"%study, full_data2)


if __name__ == "__main__":
    folders = ["Bertheussen_COR", "Bertheussen_COR-pcCu", "Huang_CO2R",
        "Kanan_CO2R-ODCu", "Kanan_COR-ODCu", "Kuhl_CO2", "Wang_COR", "Wang_COR-Cuflower", "Raciti_COR",
        "Jouny_COR", "Luc_COR", "Ma_CO2R",
        "Gregorio_CO2R", "Sargent_CO2R-CuN-C", "Sargent_COR-C3prod", "Zuettel_CO2R", "Kanan_COR-GDE",
        "Ma_GDE_prelim", "Binjun_GDE_unpublished", "Li_COR-GDE"
        ]

    # collect all data
    data = load_pkl_data(folders)
    maxpH = 100.
    #maxpH = 13.
    #for ads in ['Hydrogen', 'Methane']:
    #for ads in ['Acetate', 'Acetaldehyde', 'Ethanol', 'Ethylene', 'Hydrogen', 'Methane']:
    for ads in ['Acetate', 'Ethanol', 'Ethylene']:
        a_dat = _compute_partial_jecsa(data, adsorbate=ads, maxpH=maxpH)
        plot_partial_current_densities('partial_j_%s_maxpH%.0f'%(ads,maxpH), a_dat, pot='U_SHE')
        #plot_partial_current_densities('partial_j_pHclr_%s'%(ads), a_dat, pot='U_SHE', clr='pH')
        #plot_partial_current_densities('partial_j_%s_maxpH%.0f'%(ads,maxpH), a_dat, pot='U_RHE')

#   # combination of C2s
#   ac_dat = _compute_partial_jecsa(data, adsorbate='Acetate', maxpH=maxpH)
#   etol_dat = _compute_partial_jecsa(data, adsorbate='Ethanol', maxpH=maxpH)
#   etel_dat = _compute_partial_jecsa(data, adsorbate='Ethylene', maxpH=maxpH)
#
#   c2_dat = {} # combination of Acetate, Ethanol, Ethylene
#   ks = list(set(list(ac_dat.keys()) + list(etol_dat.keys()) + list(etel_dat.keys())))
#   for k in ks:
#       if k in ac_dat and ac_dat[k]['cell'] == 'GDE':
#           j = ac_dat[k]['j_partial'] + etol_dat[k]['j_partial'] + etel_dat[k]['j_partial']
#           c2_dat.update({k:{'j_paritial':j}})
#           c2_dat[k].update({e:etel_dat[k][e] for e in ['U_SHE', 'U_RHE', 'cell', 'mode', 'pH']})

#   data = _add_c1_c2(data)
#   for ads in ["C1", "C2+"]:
#       a_dat = _compute_partial_jecsa(data, adsorbate=ads, maxpH=maxpH)
#       plot_partial_current_densities('partial_j_%s_maxpH%.0f'%(ads, maxpH), a_dat, pot='U_SHE')
#       plot_partial_current_densities('partial_j_pHclr_%s'%(ads), a_dat, pot='U_SHE', clr='pH')

