from plot_paper_data import load_pkl_data, _compute_partial_jecsa
import matplotlib.patheffects as path_effects
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from ase import units
import numpy as np
import string

plt.rcParams["figure.figsize"] = (8,6.5)
plt.rcParams["font.family"] = "Times New Roman"
plt.rc('axes', labelsize=28)    # fontsize of the x and y labels
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16
markersize=10

CRED = '\033[91m'
CEND = '\033[0m'

if 1:
    folders = [
            "Bertheussen_COR", "Bertheussen_COR-pcCu", "Huang_CO2R",
        "Kanan_CO2R-ODCu", "Kanan_COR-ODCu", "Kuhl_CO2", "Wang_COR", "Wang_COR-Cuflower", "Raciti_COR",
        "Jouny_COR", "Luc_COR", "Ma_CO2R",
        "Gregorio_CO2R",
        'Hahn_CO2R','Kanan_COR-ODCuII','Luc_COR_II',
#        'Ren_CO2R',
       # "Sargent_CO2R-CuN-C", "Sargent_COR_C3prod",
        #'Li_COR-GDE',
        #'Li_COR',
        "Zuettel_CO2R", "Kanan_COR-GDE",
        "Ma_GDE_prelim"
        'Manthiram_CO2R',
        'Varela_CO2R','Yeo_CO2R',#'deLuna_CO2R',
        'this_study_COR_pcCu',
        ]
    emphasized_folders=['this_study_COR_pcCu']
    products = ['Ethylene+Ethanol+Acetate+n-propanol','Methane']

    #Figure6 of main article

def main():
    fig,ax=plt.subplots(1,1,figsize=(16,6.5))
    studies,folderlabels,all_folders=perform_Tafel_analysis(folders, products,emphasized_folders,plot_individual_fits=False,potscale='SHE',max_standard_deviation=0.95,individual_Tafel_slopes_in_label=False,fit_CO_and_CO2_separately=False,ax=ax)
    plt.close()
    fig,ax=plt.subplots(1,2,figsize=(16,6.5))
    perform_Tafel_analysis(folders, ['Ethylene+Ethanol+Acetate+n-propanol'],emphasized_folders,plot_individual_fits=False,potscale='SHE',max_standard_deviation=0.95,individual_Tafel_slopes_in_label=False,fit_CO_and_CO2_separately=False,
            studies=studies,folderlabels=folderlabels,ax=ax[0])
    perform_Tafel_analysis(folders, ['Methane'],emphasized_folders,plot_individual_fits=False,potscale='SHE',max_standard_deviation=0.95,individual_Tafel_slopes_in_label=False,fit_CO_and_CO2_separately=True,studies=studies,folderlabels=folderlabels,xlim=[-1.69,-1.15],ax=ax[1])


def compare_products_in_study(folders,products,maxpH=100,potrange=None,
        potscale='SHE',reference=None,outdir='output',max_standard_deviation=0.8):
    c2ads=['Ethylene','Ethanol','Acetate','n-propanol']
    comolads=['Ethylene','Ethanol','Acetate','n-propanol','Methane']
    if potrange is None:
        potrange=np.arange(-2,-1,0.1)

    if 'CO_mol' in products:
        COcons=True
    else:
        COcons=False

    fits = plot_totalcurrents(folders,[],products,
        maxpH=100,potrange=potrange,potscale=potscale,max_standard_deviation=max_standard_deviation,
        COconsumption=COcons,outdir=outdir,plot_results=False)

    if reference is None:
        reference=list(fits.keys())[-1]

    refcurrents={}
    for totads in products:
      refcurrents[totads]=0
      for ads in totads.split('+'):
          if ads == 'C2':
             for c2s in c2ads:
                 refcurrents[totads]+=fits[reference][c2s]
          elif ads == 'CO_mol':
             for c2s in comolads:
                 refcurrents[totads]+=fits[reference][c2s]
          else:
              refcurrents[totads]+=fits[reference][ads]

    relcurrents={}
    for istudy, study in enumerate(fits.keys()):
        if study == reference: continue
        relcurrents[study]={}
        for totads in products:
            relcurrents[study][totads]=0
            for ads in totads.split('+'):
                if ads == 'C2':
                    for c2s in c2ads:
                        relcurrents[study][totads]+=fits[study][c2s]
                elif ads == 'CO_mol':
                    for c2s in comolads:
                        relcurrents[study][totads]+=fits[study][c2s]
                else:
                    relcurrents[study][totads]+=fits[study][ads]
            relcurrents[study][totads]/=refcurrents[totads]

    colors = plt.cm.jet(np.linspace(0, 1, len(relcurrents.keys())))
    markers={'Methane':'^',
            'Ethylene': 'o',
            'Ethanol': 'P',
            'n-propanol': '>',
            'Acetate': '<',
            'Hydrogen':'d',
            'C2':'h',
            'CO_mol': '*'
            }
    plt.axhline(y=1,linestyle='--',color='k')
    for istudy, study in enumerate(relcurrents.keys()):
        plt.plot(np.nan,np.nan,'s',color=colors[istudy],label=study)
        for totads in relcurrents[study].keys():
            plt.plot(fits[study]['V_SHE'],relcurrents[study][totads],markers[totads],color=colors[istudy],markeredgecolor='k')
    for totads in relcurrents[study].keys():
        plt.plot(np.nan,np.nan,markers[totads],color='k',label=totads)

    plt.legend(bbox_to_anchor=(1,1))
    plt.ylabel('Relative current vs %s'%reference)
    plt.xlabel('U_\mathrm{%s} / V'%potscale)
    plt.tight_layout()
    outname = 'Reative_current_%s_%s_maxpH%s_U_\mathrm{%s}'% ('-'.join(folders),'-'.join(products),maxpH, potscale)
    plt.savefig(outdir+'/'+outname+'.pdf')
    #plt.show()
    plt.close()


def plot_totalcurrents(folders,emphasized_folders,products,
        maxpH=100,potrange=None,potscale='SHE',max_standard_deviation=0.98,
        COconsumption=True,outdir='output',plot_results=True):

    colors = ['gray','r','b','g','y']#plt.cm.jet(np.linspace(0, 1, len(a_dat.keys())))
    if potrange is None:
        potrange=np.arange(-2,-1,0.1)
    data = read_data(folders,emphasized_folders)

    allproducts=[]
    nelecs={'Methane':6,
            'Ethylene': 8,
            'Ethanol': 8,
            'n-propanol': 8,
            'Acetate': 4,
#            'Hydrogen':2
            }
    totads=[]
    fitted_data={}
    for ads in nelecs.keys():

#        if '+' in ads: continue
        a_dat = _compute_partial_jecsa(data, adsorbate=ads, maxpH=maxpH)
        for study in a_dat.keys():
            if study not in fitted_data.keys():
                fitted_data[study]={'V_%s'%potscale: potrange}

            if not all(par in a_dat[study].keys()
                   for par in ['U_'+potscale, 'j_partial']):
                print(CRED + 'Couldnt find U_%s and j_partial in %s'
                      % (potscale, study) + CEND)
                continue

            # Perform Tafel fit
            coeff = preselect_datapoints(a_dat[study],
                                     max_standard_deviation,
                                    potscale,ads,add_pH_to_rate=add_pH_to_rate)[2]
            if ads == 'Methane':
                print(study,a_dat[study]['U_SHE'][:,0])
                print(study,a_dat[study]['j_partial'][:,0])
            # Fitting current from Tafel analysis so potentials are equal
            # for all products
            if coeff is None: continue
            j_fit=lin_fun(potrange,*coeff)

            #Changing units to mol/(cm^2*s)
            if COconsumption:
                mol_fit=10**j_fit*units.C/(1e3*units.mol*nelecs[ads])
                if ads in ['Ethylene','Ethanol','Acetate']:
                    mol_fit*=2
                elif ads in ['n-propanol']:
                    mol_fit*=3

                fitted_data[study][ads]=mol_fit
            else:
                fitted_data[study][ads]=10**j_fit

    for istudy,study in enumerate(fitted_data.keys()):
        summol=None
        for ads in fitted_data[study].keys():
            if (ads not in nelecs.keys() or
                ads in ['Hydrogen']): continue

            if summol is None:
                summol = fitted_data[study][ads].copy()
            else:
                summol += fitted_data[study][ads].copy()
        fitted_data[study]['total_CO_moles']=summol


        if plot_results:
            plt.plot(fitted_data[study]['V_%s'%potscale],fitted_data[study]['total_CO_moles'],color=colors[istudy],label='%s: %1.1f mV/dec'                             % (study, 1000/coeff[0]))

    if not plot_results:
        return fitted_data

    plt.yscale('log')
    plt.xlabel(r'V$_{\mathrm{SHE}}$ [V]')
    plt.legend(bbox_to_anchor=(1, 1),prop={'size':4})
    if COconsumption:
        plt.ylabel(r'CO comsumption [$\frac{mol}{s cm^2}$]')
        outname=outdir+'/CO_molconsumption_maxpH%s_U_\mathrm{%s}'% (maxpH, potscale)
    else:
        plt.ylabel(r'Total current [$\frac{mA}{cm^2}$]')
        outname=outdir+'/Total_current_maxpH%s_U_\mathrm{%s}'% (maxpH, potscale)
    name='CO_consumption_'
    plt.tight_layout()

    plt.savefig(outname+'.pdf')
    plt.close()
    return fitted_data



def perform_Tafel_analysis(folders, products, emphasized_folders=[], maxpH=100, outdir='output',
                           min_datapoints=2, max_standard_deviation=0.98, minpH=0,xlim=[-1.69,-0.61],
                           mode=None, potscale='SHE',plot_individual_fits=False,individual_Tafel_slopes_in_label=False,
                           fit_CO_and_CO2_separately=False,add_pH_to_rate=False,studies=None,folderlabels=None,ax=None):
    """
    Main function for making the plots
    """
    data=read_data(folders,emphasized_folders)
    pot_response_fits={}
    nfolders=0
    if folderlabels == None:
        folderlabels={}
    markers={'CO2R':'d','COR':'o'}
    if studies==None:
        studies={}


        for totads in products:
          for ads in totads.split('+'):
            a_dat = _compute_partial_jecsa(data, adsorbate=ads, maxpH=maxpH,minpH=minpH)
            for istudy, study in enumerate(a_dat.keys()):
                if study not in  studies:
                    studies[study]={}
        if len(emphasized_folders):
            nemphstudies=0
            for study in data:
                if 'emphasized' in data[study]:
                    nemphstudies+=1
            colors = plt.cm.jet(np.linspace(0, 1, nemphstudies))
        else:
            colors=plt.cm.jet(np.linspace(0, 1, len(studies.keys())))

        iemps=0
        for istudy, study in enumerate(studies.keys()):
            if len(emphasized_folders):
                if 'emphasized' in data[study]:
                    studies[study]['color']=colors[iemps]
                    iemps+=1
                else:
                    studies[study]['color']='lightgray'
            else:
                    studies[study]['color']=colors[istudy]

    if isinstance(products,str):
        products=[products]

    for totads in products:
      print('Tafel slopes for %s in studies:' % totads)
      tot_all_data, Tafdata, Tafred,all_folders,all_modes = {}, [], [],{},{}
      all_labels=[]
      #plt.plot(np.nan,np.nan,'d',markeredgecolor='k',markerfacecolor='w',label='CO$_2$R')
      #plt.plot(np.nan,np.nan,'o',markeredgecolor='k',markerfacecolor='w',label='COR')
      #plt.plot(np.nan,np.nan,'d',markeredgecolor='w',markerfacecolor='w',label='-'*20)
      COR_Tafred,CO2R_Tafred=[],[]
      for ads in totads.split('+'):
        a_dat = _compute_partial_jecsa(data, adsorbate=ads, maxpH=maxpH,minpH=minpH)
#        colors = plt.cm.jet(np.linspace(0, 1, len(a_dat.keys())))
        for istudy, study in enumerate(a_dat.keys()):
            folder=a_dat[study]['folder']
            if folder not in folderlabels:
                    folderlabels[folder]=string.ascii_uppercase[nfolders]
                    nfolders+=1
            if mode and a_dat[study]['mode'] != mode:
                continue
            if study not in tot_all_data.keys():
                tot_all_data[study] = []

            if not all(par in a_dat[study].keys()
                       for par in ['U_'+potscale, 'j_partial']):
                print(CRED + 'Couldnt find U_%s and j_partial in %s'
                      % (potscale, study) + CEND)
                continue
            folder=a_dat[study]['folder']
            all_folders[study]=folder
            all_modes[study]=a_dat[study]['mode']
            this_reddata, this_all_data, coeff = \
                preselect_datapoints(a_dat[study],
                                     max_standard_deviation,
                                     potscale,ads,add_pH_to_rate=add_pH_to_rate)

            alpha=1 if 'emphasized' in data[study] else 1
            if len(totads.split('+')) == 1:
                thisax=ax
                if coeff is not None:
                    print('(%s) %s: %1.2f mV/dec' % (folderlabels[all_folders[study]], study, 1000/coeff[0]))

                if len(this_reddata) > min_datapoints:
                    Tafred.extend([pot_j for pot_j in this_reddata])
                    if a_dat[study]['mode'] == 'COR' and study[:9] != 'this_pcCu':
                        COR_Tafred.extend([pot_j for pot_j in this_reddata])
                    elif study[:9] != 'this_pcCu':
                        CO2R_Tafred.extend([pot_j for pot_j in this_reddata])
                Tafdata.extend([pot_j for pot_j in this_all_data])

                if plot_individual_fits and coeff is not None:
                    thisax.plot(this_reddata[:,0],lin_fun(this_reddata[:,0],*coeff),
                    #plt.plot(this_reddata[:,0],lin_fun(this_reddata[:,0],*coeff),
                            color=studies[study]['color'],
                            markeredgecolor='k',
                            alpha=alpha)

                label='(%s) '%\
                    folderlabels[folder]+'-'.join(study.split('_')[1:])

                if individual_Tafel_slopes_in_label and coeff is not None:
                    label+='%i mV/dec'% (1000/coeff[0])

                #Plot all points containing mass transport limited
                if len(this_reddata):
                    #plt.plot(this_all_data[:, 0], this_all_data[:, 1], markers[all_modes[study]],
                    thisax.plot(this_all_data[:, 0], this_all_data[:, 1], markers[all_modes[study]],
                             markerfacecolor='w', markeredgecolor=studies[study]['color'],
                             alpha=alpha)

                #Plot only mass transport problem free points
                if len(this_reddata):
                    #plt.plot(this_reddata[:, 0], this_reddata[:, 1], markers[all_modes[study]],
                    thisax.plot(this_reddata[:, 0], this_reddata[:, 1], markers[all_modes[study]],
                            markeredgecolor='k' if 'emphasized' in data[study] else None,
                             color=studies[study]['color'],
                             label=label if 'emphasized' in data[study] else None,
                             alpha=alpha,
                             markersize=10 if 'emphasized' in data[study] else 7)
            else:
                tot_all_data[study].extend([pot_j for pot_j in this_all_data])

      if len(totads.split('+')) > 1:
        thisax=ax
        final_all_data, final_red_data = {}, {}
        if len(emphasized_folders):
            nemphstudies=0
            for study in tot_all_data:
                if 'emphasized' in data[study]:
                    nemphstudies+=1
            colors = plt.cm.jet(np.linspace(0, 1, nemphstudies))
            emphcounter=0
        else:
            colors = plt.cm.jet(np.linspace(0, 1, len(tot_all_data.keys())))
        plottedfolders=[]
        for istudy, study in enumerate(tot_all_data.keys()):
            tot_all_data[study] = np.array(tot_all_data[study])
            final_all_data[study] = []

            for pot in np.unique(tot_all_data[study][:, 0]):
              js_at_pot = (np.where(tot_all_data[study][:, 0] == pot)[0])
              final_all_data[study].append([pot, np.log10(np.sum(
                  10**tot_all_data[study][js_at_pot, 1]))])

            final_all_data[study] = np.array(final_all_data[study])

            final_red_data[study], this_all_data, coeff = \
                preselect_datapoints(final_all_data[study],
                                     max_standard_deviation,
                                     potscale,ads,add_pH_to_rate=add_pH_to_rate)

            if all_modes[study] == 'COR':
                        COR_Tafred.extend([pot_j for pot_j in final_red_data[study]])
            else:
                        CO2R_Tafred.extend([pot_j for pot_j in final_red_data[study]])

            if plot_individual_fits and len(final_red_data[study] > 1):
                #plt.plot(final_red_data[study][:,0],lin_fun(final_red_data[study][:,0],*coeff),
                thisax.plot(final_red_data[study][:,0],lin_fun(final_red_data[study][:,0],*coeff),
                        color=studies[study]['color'])

            #plt.plot(final_all_data[study][:, 0],
            thisax.plot(final_all_data[study][:, 0],
                     final_all_data[study][:, 1],markers[all_modes[study]],
                     markerfacecolor='w',
                     markeredgecolor=studies[study]['color'])

            if len(final_red_data[study]) >= min_datapoints:
                #if '-'.join(study.split('-')[:2]) not in plottedfolders:
                if all_folders[study] not in plottedfolders:
                    plottedfolders.append(all_folders[study])
                print(string.ascii_uppercase[(len(plottedfolders)-1)],plottedfolders[-1],'%s: %1.2f mV/dec' % (study, 1000/coeff[0]))
                #print(study)
                label='(%s) '%\
                    folderlabels[all_folders[study]]+'-'.join(study.split('_')[1:])
                if individual_Tafel_slopes_in_label:
                    label+='%i mV/dec'% ( 1000/coeff[0])
                all_labels.append(label)
                #plt.plot(final_red_data[study][:, 0],
                thisax.plot(final_red_data[study][:, 0],
                         final_red_data[study][:, 1],markers[all_modes[study]],
                         markeredgecolor='k' if 'emphasized' in data[study] else None,
                         color=studies[study]['color'],
                         label=label if 'emphasized' in data[study] else None,#'%s: %1.1f mV/dec' % (study, 1000/coeff[0]))
                         alpha=alpha,
                        markersize=10 if 'emphasized' in data[study] else 7
                         )
                Tafred.extend([pot_j for pot_j in final_red_data[study]])

            Tafdata.extend([pot_j for pot_j in final_all_data[study]])
      if len(products) == 1 and products[0] == 'Methane':
       #text=plt.text(-1.76,1.5,'(b)', fontsize=30, fontweight='bold',color='k')
       text=thisax.text(-1.76,1.5,'(b)', fontsize=30, fontweight='bold',color='k')
       #text=plt.annotate('pH 3\n81mV/dec', (-1.27,-0.8), fontsize=20, fontweight='bold',color='darkblue')
       text=thisax.annotate('pH 3\n81mV/dec', (-1.27,-0.8), fontsize=20, fontweight='bold',color='darkblue')
       text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
       #text=plt.annotate('pH 7\n89mV/dec', (-1.35,-2.8), fontsize=20, fontweight='bold',color='lightgreen')#.draggable()
       text=thisax.annotate('pH 7\n89mV/dec', (-1.35,-2.8), fontsize=20, fontweight='bold',color='lightgreen')#.draggable()
       text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
       #text=plt.annotate('pH 13\n43mV/dec', (-1.65,-1), fontsize=20, fontweight='bold',color='brown')#.draggable()
       text=thisax.annotate('pH 13\n43mV/dec', (-1.65,-1), fontsize=20, fontweight='bold',color='brown')#.draggable()
       text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
      else:
       text=thisax.text(-1.83,1.5,'(a)', fontsize=30, fontweight='bold',color='k')
       text=thisax.annotate('pH 3\n165mV/dec', (-1.4,-2.5), fontsize=20, fontweight='bold',color='darkblue')
       text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
       text=thisax.annotate('pH 7\n199mV/dec', (-1.55,-1.3), fontsize=20, fontweight='bold',color='lightgreen')#.draggable()
       text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
       text=thisax.annotate('pH 13\n119mV/dec', (-1.28,-0.55), fontsize=20, fontweight='bold',color='brown')#.draggable()
       text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
                       path_effects.Normal()])
      pot_response_fits[totads]=finalize_Tafplot(Tafred, Tafdata, totads, potscale,
              plot_individual_fits, mode,maxpH=maxpH,labels=all_labels,COR=COR_Tafred,
              CO2R=CO2R_Tafred,fit_CO_and_CO2_separately=fit_CO_and_CO2_separately,add_pH_to_rate=add_pH_to_rate,xlim=xlim,ax=thisax)
    return studies,folderlabels,all_folders#,pot_response_fits


def finalize_Tafplot(Tafred, Tafdata, ads, potscale, plot_individual_fits,mode=None,fit_CO_and_CO2_separately=False,
                     outdir='output', maxpH=100,labels=None,COR=None,CO2R=None,add_pH_to_rate=False,xlim=[-1.69,-0.6],ax=plt):
    thisax=ax
    if fit_CO_and_CO2_separately and COR:
        COR=np.array(COR)
        CORcoeff, dum = curve_fit(lin_fun, COR[:, 0], COR[:, 1])
        #plt.plot(COR[:, 0], lin_fun(COR[:, 0], *CORcoeff), 'k-')
        thisax.plot(COR[:, 0], lin_fun(COR[:, 0], *CORcoeff), 'k-')
        #plt.annotate('COR: %1.1f mV/dec' % (1000/CORcoeff[0]),
        thisax.annotate('COR: %1.1f mV/dec' % (1000/CORcoeff[0]),
                 (-1.68,-2.8),fontsize=20).draggable()
        thisax.annotate(r' $\alpha$ = %1.2f' % (-CORcoeff[0]*0.059),
        #plt.annotate(r' $\alpha$ = %1.2f' % (-CORcoeff[0]*0.059),
                 (-1.62,-3.2),fontsize=20).draggable()
    if fit_CO_and_CO2_separately and CO2R:
        CO2R=np.array(CO2R)
        CO2Rcoeff, dum = curve_fit(lin_fun, CO2R[:, 0], CO2R[:, 1])
        thisax.plot(CO2R[:, 0], lin_fun(CO2R[:, 0], *CO2Rcoeff), 'k-')
        thisax.annotate('CO2R: %1.1f mV/dec' % (1000/CO2Rcoeff[0]),
                      (-1.5,1.2),fontsize=20).draggable()
        thisax.annotate(r' $\alpha$ = %1.2f' % (-CO2Rcoeff[0]*0.059),
                 (-1.45,0.9),fontsize=20).draggable()

    if not plot_individual_fits and not fit_CO_and_CO2_separately:
        Tafred, Tafdata = np.array(Tafred), np.array(Tafdata)
        coeff, dum = curve_fit(lin_fun, Tafred[:, 0], Tafred[:, 1])
        print(CRED+'Overall %s Tafel slope: ' % ads, 1000/coeff[0], CEND)
        thisax.plot(Tafred[:, 0], lin_fun(Tafred[:, 0], *coeff), 'k-')
        thisax.annotate('Tafel slope = %1.1f mV/dec' % (1000/coeff[0]),
                 (-0.65,1.9),fontsize=20,ha='right',va='top').draggable()
        thisax.annotate(r'$\alpha$ = %1.2f' % (-coeff[0]/1000*59),
                 (-0.65,1.6),fontsize=20,ha='right',va='top').draggable()
        #plt.annotate('C2+',
        #         (-0.8,2),weight='bold',fontsize=24).draggable()

    thisax.set_xlabel('U$_{\mathrm{%s}}$ / V' % potscale)

    if '+' in ads:
        ylabel='log (j$_{\mathrm{C_{2+}}}$ / mA/cm$^2$)'
    else:
        ylabel='log (j$_{\mathrm{CH_4}}$ / mA/cm$^2$)'
    outname=outdir+'/Tafel_analysis_%s_maxpH%s_U_%s'% (ads, maxpH, potscale)

    if add_pH_to_rate and ads == 'Methane' and potscale == 'SHE':
            ylabel+=' + pH'
            outname+='+pH'
    if mode:
        outname+='_%s'%mode
    outname+='.pdf'

    thisax.set_ylabel(ylabel)

#    plt.legend(bbox_to_anchor=(1, 1),prop={'size':6},loc='upper left')
#    plt.legend()
    if not add_pH_to_rate:
        thisax.set_ylim(-5,2)
        thisax.set_xlim(xlim)
    #plt.title(ads)
    if fit_CO_and_CO2_separately:
        plt.tight_layout()
        plt.savefig(outname)
    #plt.show()
#    plt.close()
    #return coeff


def preselect_datapoints(dat, max_standard_deviation, potscale,ads,add_pH_to_rate=False):
    Tafdata = []
    if isinstance(dat, dict):
        for ipot, pot in enumerate(dat['U_'+potscale]):
            Tafdata.append([pot[0], np.log10(dat['j_partial'][ipot][0])])
        Tafdata = np.array(Tafdata)
    else:
        Tafdata = dat


    if len(Tafdata) < 2:
        return [],[],None

    Tafdata = Tafdata[np.argsort(Tafdata[:, 0])]
    Tafred = detect_mass_trans(Tafdata, max_standard_deviation)
    if add_pH_to_rate and ads == 'Methane' and potscale == 'SHE':
              if potscale == 'SHE':
                Tafred[:, 1] += float(dat['pH'])
                Tafdata[:, 1] += float(dat['pH'])
    if len(Tafred) > 1:
        coeff, dum = curve_fit(lin_fun, Tafred[:, 0], Tafred[:, 1])
    else:
        coeff=None

    return Tafred, Tafdata, coeff


def detect_mass_trans(Tafdata, max_standard_deviation):
    counter, r_squared = 0, 0
    while r_squared - max_standard_deviation < 0:
        Tafred = Tafdata.copy()
        if counter:
            Tafred = Tafred[counter:]
        if len(Tafred) < 2:
            Tafred=np.array([])
            break
        coeff, dum = curve_fit(lin_fun, Tafred[:, 0], Tafred[:, 1])
        residuals = Tafred[:, 1] - lin_fun(Tafred[:, 0], *coeff)
        ss_res = np.sum(residuals**2)
        ss_tot = np.sum((Tafred[:, 1]-np.mean(Tafred[:, 1]))**2)
        r_squared = 1 - (ss_res / ss_tot)
        counter += 1
    return Tafred

def read_data(folders,emphasized_folders):
    data = load_pkl_data(folders)
    if len(emphasized_folders):
        data_emph = load_pkl_data(emphasized_folders)
        for emfol in data_emph.keys():
            data_emph[emfol]['emphasized']=True
        data.update(data_emph)
    else:
        for emfol in data.keys():
            data[emfol]['emphasized']=True

    return data

def lin_fun(x, a, b):
    return a*x+b


if __name__ == "__main__":
    main()
