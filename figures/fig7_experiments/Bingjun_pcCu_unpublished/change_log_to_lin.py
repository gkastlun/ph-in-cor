import os,sys

for idat in range(5):
    ftypes=['data','std']
    for ftype in ftypes:
        filename='raw_%s_%i.txt'%(ftype,idat+1)
        inlines=open('backup/'+filename,'r').readlines()
        out = open(filename,'w')
        out.write(inlines[0])
        if ftype=='data':
                constcol=2
        else:
                constcol=1

        for line in inlines[1:]:
            outline=' '.join(line.split()[:constcol])
            for dat in line.split()[constcol:]:
                outline+=' %1.4f'%(10**float(dat))
            outline+='\n'
            out.write(outline)
        out.close()
