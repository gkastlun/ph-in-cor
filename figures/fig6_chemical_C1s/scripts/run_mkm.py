import numpy as np
from catmap import ReactionModel
from catmap import analyze
from string import Template
import os,sys
import pickle

include_rate_control = 0
plot_all_rates=0
plot_production_rate=1
plot_coverage=1
mechanism=sys.argv[1]
home=os.getcwd()

model = ReactionModel(setup_file = home+'/scripts/'+mechanism+'.mkm')

model.output_variables += ['production_rate','rate','coverage']
if include_rate_control:
    model.output_variables += ['rate_control']
model.run()

if any([plot_coverage,plot_production_rate,plot_all_rates]):
    vm = analyze.VectorMap(model)

os.system('python ~/scripts/transfering_pickles.py %s.pkl'%mechanism)

if plot_all_rates:
         vm.plot_variable = 'rate'
         vm.log_scale = True
         vm.min = 1e-30
         vm.max = 1e+3
         fig = vm.plot(save=False)
         fig.savefig('rate_'+mechanism+'.pdf')

if plot_production_rate:
         vm.plot_variable = 'production_rate'
         vm.log_scale = True
         vm.colorbar = True
         vm.min = 1e-5
         vm.max = 1e+5
         fig = vm.plot(save=False)
         fig.savefig('production_rate'+mechanism+'.pdf')

if plot_coverage:
         vm = analyze.VectorMap(model)
         vm.log_scale = False
         vm.unique_only = False
         vm.plot_variable = 'coverage'
         vm.min = 0
         vm.max = 0.2
         fig = vm.plot(save=False)
         fig.savefig('coverage'+mechanism+'.pdf')

         vm = analyze.VectorMap(model)
         vm.log_scale = True
         vm.unique_only = False
         vm.plot_variable = 'coverage'
         vm.min = 1e-20
         vm.max = 1
         fig = vm.plot(save=False)
         fig.savefig('coverageLog'+mechanism+'.pdf')

if include_rate_control:
         mm = analyze.MatrixMap(model)
         mm.plot_variable = 'rate_control'
         mm.log_scale = False
         mm.min = -2
         mm.max = 2
         mm.plot(save='rate_control'+mechanism+'.pdf')

