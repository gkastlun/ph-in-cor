import sys
interaction_strength=float(sys.argv[2])

# General settings - usually unchanged
scaler = 'ThermodynamicScaler'
descriptor_names= ['voltage', 'pH']
descriptor_ranges= [[-1.6,-0.0],[3,14]]
temperature = 300.
resolution=[33,12]

gas_thermo_mode = 'frozen_gas'
adsorbate_thermo_mode = 'frozen_adsorbate'
electrochemical_thermo_mode = 'surface_charge_density'

# solver settings
decimal_precision = 100
tolerance = 1e-25
max_rootfinding_iterations = 200
max_bisections = 3

# Standard rate parameter settings beta will 0.5 if not explicitly given
beta = 0.5

# Cu - CO2 reduction input file settings
input_file = '../../formation_energies_for_catmap.txt' #C2_h2o_energies.txt'
surface_names = ['Cu']
# Electrochemical settings - usually not changed
potential_reference_scale = 'SHE'

species_definitions = {}
# pressures
#species_definitions['H_g'] = {'pressure':1.0}
species_definitions['ele_g'] = {'pressure':1.0, 'composition':{}}
species_definitions['CO_g'] = {'pressure':1.0}
species_definitions['H2_g'] = {'pressure':0.0}
species_definitions['H2O_g'] = {'pressure':0.035}
species_definitions['CH4_g'] = {'pressure':0.0}
species_definitions['OH_g'] = {'pressure':0.0}

#sites
estimate_frequencies = 1

# interaction model
#adsorbate_interaction_model = 'first_order' #use "single site" interaction model
#adsorbate_interaction_model = 'none' #use "single site" interaction model
interaction_response_function = 'smooth_piecewise_linear' #use "smooth piecewise linear" interactions
interaction_fitting_mode = None
cross_interaction_mode = 'geometric_mean' #use geometric mean for cross parameters

# site interaction scaling:
eHCO=0.7274*interaction_strength
eCO=2.4670*interaction_strength
eH=0.0

species_definitions['211'] = {'site_names': ['211'], 'total':1.0}
species_definitions['211']['interaction_response_parameters'] = {'cutoff':0.25,'smoothing':0.05}
species_definitions['dl'] = {'site_names': ['dl'], 'total':1.0}
species_definitions['CO_211'] ={'self_interaction_parameter':[eCO],'cross_interaction_parameters':{'H_211': [eHCO]} }

species_definitions['H_211'] = {'cross_interaction_parameters':{'CO_211': [eHCO]}}

species_definitions['OCCO_211'] = {
                   'self_interaction_parameter':[1*eCO],
                   'cross_interaction_parameters':{'CO_211': [1*eCO],'H_211': [1*eHCO]},
                   'n_sites':2}

for sp in ['H2O-ele_211','CO_211','OC-H2O-ele_211','H_211','CHO_211','CHO-H2O-ele_211','CHOH_211','CH_211']:
    species_definitions[sp] = species_definitions['CO_211'].copy()

for sp in ['CO-H_211']:
    species_definitions[sp] = species_definitions['OCCO_211'].copy()

sigma_input = ['CH', 1]
Upzc = 0.00
#species_definitions['CO_211']['sigma_params']=[-0.000527111148607784,-0.48474645648378445]
#species_definitions['H_211']['sigma_params']=[0.02337267525012133,-0.1436253939978989]
species_definitions['CHO_211']['sigma_params']=[0.27874591667859944,-0.9468234144483205]
species_definitions['CHOH_211']['sigma_params']=[0.002490173436322109,0.049836197165604904]
species_definitions['CH_211']['sigma_params']=[-0.02176069976103352,0.006250115756364946]
# CO-H potential response has been neglected since it only arose from water reorganization
# check the NEB-bands at varying potential, they do not shift.
#species_definitions['CO-H_211']['sigma_params']=[0.23418772599369223, -0.4623519784642922]
