#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl
import matplotlib.patheffects as path_effects
from catmaphelpers.catmap_plotter import _get_unit_cell_area


plt.rcParams["font.family"] = "Times New Roman"
plt.rc('axes', labelsize=18)    # fontsize of the x and y labels
plt.rcParams['xtick.labelsize'] = 14
plt.rcParams['ytick.labelsize'] = 14

#sys.path.append("../.")
#from post_catmap import read_catmap_wdir
potscale='SHE'
surf='211'
surfratio=0.05
AperSite = _get_unit_cell_area(a_input=3.74)
ec = 1.602176e-19; A2cm = 1e-8
tof2cur = (surfratio*1e3*ec/(A2cm**2))/AperSite[str(surf)]
tof2cur = {'C2+':8*tof2cur, 'Ac':4*tof2cur, 'tot':1.0,'EtOH':8*tof2cur,'C1':6*tof2cur,'HER':2*tof2cur,'C2H4':8*tof2cur}

if __name__ == "__main__":
    data_in = {'echem':pkl.load(open('C1s_echem_py3.pkl','rb'),encoding='latin1'),
               'surfh': pkl.load(open('C1s_surface_hydro_py3.pkl','rb'),encoding='latin1')}
    data={'echem':{'rate':{},'cov':{}},
          'surfh':{'rate':{},'cov':{}},
          'HER':{'rate':{},'cov':{}}}
    pots,phs=[],[]
    nsteps=2
    irate=[0,1]
    iads=[3,4]


    for mech in data_in:
        for dat in data_in[mech]['production_rate_map']:
            pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
            if ph not in data[mech]['rate']:
                data[mech]['rate'][ph] = []
            data[mech]['rate'][ph].append([pot,dat[1][0]])

            if mech == 'surfh':
                #print(dat[1][3])
                if ph not in data['HER']['rate']:
                    data['HER']['rate'][ph] = []
                data['HER']['rate'][ph].append([pot,dat[1][3]])

            if pot not in pots:
                pots.append(np.around(pot,2))
            if ph not in phs:
                phs.append(ph)
        for ph in phs:
            data[mech]['rate'][ph] = np.array(data[mech]['rate'][ph])
            data[mech]['rate'][ph][:,1]*=tof2cur['C1']
            if mech == 'surfh':
                data['HER']['rate'][ph] = np.array(data['HER']['rate'][ph])
                data['HER']['rate'][ph][:,1]*=tof2cur['HER']

        for dat in data_in[mech]['coverage_map']:
            pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
            if ph not in data[mech]['cov']:
                data[mech]['cov'][ph] = []
            data[mech]['cov'][ph].append([pot,dat[1][0]])
        for ph in phs:
            data[mech]['cov'][ph] = np.array(data[mech]['cov'][ph])
    X=np.array(sorted(np.unique(pots)))
    Y=np.array(sorted(phs))

    for iph,pH in enumerate(data['surfh']['rate'].keys()):
        data['echem']['rate'][pH]=data['echem']['rate'][pH][np.argsort(data['echem']['rate'][pH][:,0])]
        data['surfh']['rate'][pH]=data['surfh']['rate'][pH][np.argsort(data['surfh']['rate'][pH][:,0])]
        data['HER']['rate'][pH]=data['HER']['rate'][pH][np.argsort(data['HER']['rate'][pH][:,0])]

    colors=['b','g','brown']
    plt.plot(np.nan,np.nan,'-',color='k',label='CH4, surf-hydr')
    plt.plot(np.nan,np.nan,'--',color='k',label='CH4, PCET')
    plt.plot(np.nan,np.nan,':',color='k',label='HER')

    for iph,pH in enumerate([3,7,13]):
        plt.plot(np.nan,np.nan,'s',label='pH %i'%pH,color=colors[iph])

        if potscale != 'RHE':
            plt.plot(data['HER']['rate'][pH][:,0],data['HER']['rate'][pH][:,1],':',color=colors[iph])
            plt.plot(data['echem']['rate'][pH][:,0],data['echem']['rate'][pH][:,1],'--',color=colors[iph])
            plt.plot(data['surfh']['rate'][pH][:,0],data['surfh']['rate'][pH][:,1],'-',color=colors[iph],linewidth=3)
        else:
            plt.plot(data['HER']['rate'][pH][:,0]+0.059*pH,data['HER']['rate'][pH][:,1],':',color=colors[iph])
            plt.plot(data['echem']['rate'][pH][:,0]+0.059*pH,data['echem']['rate'][pH][:,1],'--',color=colors[iph])
            plt.plot(data['surfh']['rate'][pH][:,0]+0.059*pH,data['surfh']['rate'][pH][:,1],'-',color=colors[iph],linewidth=3)

#    text=plt.annotate('Tafel\nslope',(-1.56,1e-9),color='k',ha='left',fontsize=12)
#    text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),
#                       path_effects.Normal()])
    text=plt.annotate(r'Tafel slope $\rightarrow \infty$',(-1.53,10**-9.3),color='k',va='bottom',ha='left',fontsize=14,
            bbox=dict(facecolor='w', edgecolor='k',boxstyle='round'))

    if potscale != 'RHE':
        plt.annotate('', xy=(-0.3, 10**-10.5), xytext=(-0.85, 10**-10.5),
            arrowprops=dict(arrowstyle= '<->',facecolor='black',lw=2),
            )
        plt.annotate('ln(10)k$_\mathrm{B}$T$\Delta$pH', xy=(-0.75, 10**-10.7),fontsize=14,# xytext=(-0.6, 10**-10.5),
            #arrowprops=dict(arrowstyle= '<|-|>',facecolor='black'),
            bbox=dict(facecolor='w', edgecolor='k',boxstyle='round')
            )

    plt.yscale('log')
    plt.ylim([1e-12,10])
    plt.xlim([-1.6,-0.1])
    plt.ylabel('log( j / mAcm$^{-2}$)')
    plt.legend()
    if potscale == 'RHE':
        plt.xlabel('U$_{\mathrm{RHE}}$/V')
        plt.tight_layout()
        plt.savefig('Figure6_RHE.pdf')
    else:
        plt.xlabel('U$_{\mathrm{SHE}}$/V')
        plt.tight_layout()
        plt.savefig('Figure6.pdf')

