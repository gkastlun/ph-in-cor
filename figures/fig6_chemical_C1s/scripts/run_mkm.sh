for mech in echem surface_hydro
do
    for i in `seq 1 0.1 1`
    do
        python scripts/run_mkm.py C1s_$mech $i
    done
    python ../../tools_for_analysis/transfering_pickles.py C1s_$mech.pkl
done
