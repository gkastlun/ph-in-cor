if [ -f mkm.log ];then
    rm mkm.log
fi

for i in `seq 0 0.05 1.01`
do
    echo Running interaction strength $i
    python scripts/run_mkm.py 211 $i >> mkm.log
done

python ../../../tools_for_analysis/transfering_pickles.py 211.pkl
