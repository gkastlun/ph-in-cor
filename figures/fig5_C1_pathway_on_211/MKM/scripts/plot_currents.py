#!/usr/bin/env python
import sys, os
#from mpl_toolkits.axes_grid1 import ImageGrid
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm,patheffects
import numpy as np
from scipy.optimize import curve_fit, leastsq
from copy import deepcopy
import pickle as pkl
from catmaphelpers.catmap_plotter import _get_unit_cell_area

#from catmaphelpers.catmap_plotter import _make_plots, _plot_model, _plot_CV, post_process_CV, _compute_tafel_prate, plot_tafel_screening, _get_unit_cell_area, _plot_pH, _plot_map, _plot_pH_bar_coverage
#from catmaphelpers.catmap_output_handler import _get_data, _sum_catmap_data, _get_rate_control, read_catmap_wdir
#from catmaphelpers.extra_tools import make_FEDs

plt.rcParams["font.family"] = "Times New Roman"
plt.rc('axes', labelsize=18)    # fontsize of the x and y labels
plt.rcParams['xtick.labelsize'] = 14
plt.rcParams['ytick.labelsize'] = 14
home=os.getcwd()
surface_ratio=0.05
facet='211'#home.split('/')[-3]
pHs_for_xy_plot=[3.0,7.0,13.0]

if __name__ == "__main__":
    data_in = pkl.load(open(facet+'_py3.pkl','rb'),encoding='latin1')
    data={'rate':{},'cov':{},'taf':{}}
    pots,phs=[],[]
    nsteps=1
    irate=[0,0]
    iads=[3,4]

    for dat in data_in['production_rate_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['rate']:
            data['rate'][pot] = {}
        data['rate'][pot][ph] = [i*surface_ratio for i in dat[1]]
        if pot not in pots:
            pots.append(np.around(pot,2))
        if ph not in phs:
            phs.append(ph)

    for dat in data_in['coverage_map']:
        pot,ph=np.around(dat[0][0],3),np.around(dat[0][1],3)
        if pot not in data['cov']:
            data['cov'][pot] = {}
        data['cov'][pot][ph] = dat[1]

    pots=sorted(np.unique(pots))
    for pH in data['rate'][pot]:
        if pH not in data['taf']:
            data['taf'][pH]={}
        for ipot,pot in enumerate(pots):
            if ipot==0: continue
            #print(pH,pot)
            #print(data['rate'][pot].keys())
            #print(data['rate'][pots[ipot-1]][pH][0])
            data['taf'][pH][(pot+pots[ipot-1])/2]=\
            -(pot-pots[ipot-1])/(np.log10(data['rate'][pot][pH][0])-np.log10(data['rate'][pots[ipot-1]][pH][0]))

    #X=np.array(sorted(np.unique(pots)))
    X=np.array(pots)
    Y=np.array(sorted(phs))
    if nsteps == 1:cols=1
    else: cols=2
    cols=1

    fig,ax=plt.subplots(nsteps,cols)


    for col in range(cols):
      for istep in range(nsteps):
        XY=np.ones((len(X),len(Y)))*0.5
        rate=np.ones((len(X),len(Y)))*0.5
        for ix,x in enumerate(X):
            for iy,y in enumerate(Y):
                            try:
                                if col == 1:
                                    XY[ix][iy]=data['cov'][x][y][iads[istep]]
                                #    if data['cov'][x][y][iads[istep]] < 0:
                                else:
                                    #if istep == 1:
                                    #   if data['rate'][x][y][irate[istep]] < 1e-13:
                                    #        print(x,y,data['rate'][x][y][irate[istep]])
                                    if data['rate'][x][y][irate[istep]] < 1e-13:
                                        rate[ix][iy]=1e-13
                                    else:
                                        rate[ix][iy]=data['rate'][x][y][irate[istep]]#/np.sum(data[x][y][:nsteps])
                            except:
                                XY[ix][iy]=np.nan#data[x][y][istep]#/np.sum(data[x][y][:3])
                                rate[ix][iy]=1e-20#np.nan#data[x][y][istep]#/np.sum(data[x][y][:nsteps])
                                pass

        AperSite = _get_unit_cell_area(a_input=3.6149)
        ec = 1.602176e-19; A2cm = 1e-8
        tof2cur = 6*(1e3*ec/(A2cm*A2cm))/AperSite[facet]
        rate*=tof2cur
        if col == 0:
         if nsteps == 1: plotax=ax
         elif cols == 1: plotax= ax[istep]
         else: plotax= ax[istep][0]


         b = plotax.imshow(rate.T,
                interpolation='bicubic',
                cmap=cm.jet,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-5,
                    vmax=1e02,#)
                    aspect=0.03)#, vmin=-abs(alldata[:,2]).max())

        else:
         a = ax[istep][1].imshow(XY.T,
                interpolation='bicubic',
                cmap=cm.RdYlGn,
                   origin='lower', extent=[X.min(), X.max(), Y.min(), Y.max()],norm=LogNorm(),#,
                    vmin=1e-10,
                    vmax=1,
                    aspect='auto')#, vmin=-abs(alldata[:,2]).max())

        if istep == nsteps-1:
#            fig.colorbar(a,ax=ax[0][1],fraction=0.146, pad=.04,label='Selectivity')
            if nsteps==1: ax.set_xlabel('U$_{SHE}$ [V]')
            else:
                if cols == 1:
                    ax[istep].set_xlabel('U$_{SHE}$ [V]')
                else:
                    for thisax in ax[istep]:
                        thisax.set_xlabel('U$_{SHE}$ [V]')
        else:
            if cols == 1:
                    ax[istep].set_xticks([])
            else:
                for thisax in ax[istep]:
                    thisax.set_xticks([])

        if cols > 1:
            ax[istep][0].set_ylabel('pH')
            ax[istep][1].set_yticks([])
        else:
            if nsteps == 1:
                ax.set_ylabel('pH')
            else:
                ax[istep].set_ylabel('pH')

    if cols == 1:
        if nsteps == 1: barax=ax
        else: barax=ax[0]
    else: barax = ax[0][0]
    axins1 = inset_axes(barax,
                    width="100%",  # width = 50% of parent_bbox width
                    height="5%",  # height : 5%
                    loc='lower left',
                     bbox_to_anchor=(0, 1.+nsteps/7., 1, 1),
                  bbox_transform=barax.transAxes)
    fig.colorbar(b, cax=axins1,orientation='horizontal',fraction=0.07,anchor=(1.0,1.0))#cbar_ax)

    if cols > 1:
     axins2 = inset_axes(ax[0][1],
                    width="100%",  # width = 50% of parent_bbox width
                    height="5%",  # height : 5%
                    loc='lower left',
                    bbox_to_anchor=(0, 1.2, 1, 1),
                    bbox_transform=ax[0][1].transAxes)
     fig.colorbar(a, cax=axins2,orientaqtion='horizontal',fraction=0.07,anchor=(1.0,1.0))#cbar_ax)
     ax[0][1].annotate('Coverage',(-1.4,14),color='k',bbox=dict(facecolor='w', edgecolor=None))
     ax[0][1].annotate(r'*CO',(-1.59,7.2),color='w',fontweight='bold',fontsize=20,ha='left')
     ax[1][1].annotate(r'*H',(-1.59,7.2),color='w',fontweight='bold',fontsize=20,ha='left')

    barax.annotate('Current density [mA/cm$^{2}$]',(-1.11,14),color='k',ha='right',va='center',bbox=dict(facecolor='w', edgecolor=None),fontsize=16)
    #ax[1].annotate(facet,(-1.5,14),color='k',bbox=dict(facecolor='w', edgecolor=None),fontsize=20,fontweight='bold')

    ax.annotate('CH$_4$',(-1.8,3.2),color='k',fontweight='bold',ha='left',fontsize=20)
    #ax[1].annotate('H2',(-1.8,3.2),color='k',fontweight='bold',ha='left',fontsize=20)
    #fig.subplots_adjust(wspace=-1.2,hspace=0.05)
    fig.savefig('TOF.pdf')


    plt.close()

    plt.rcParams["figure.figsize"] = (7,5)
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rc('axes', labelsize=20)    # fontsize of the x and y labels
    plt.rcParams['xtick.labelsize'] = 18
    plt.rcParams['ytick.labelsize'] = 18
    plt.rcParams['figure.figsize'] = (10,5)
    markersize=10

#sys.path.append("../.")
#from post_catmap import read_catmap_wdir

    pHcolors={3.0:'b',7.0:'k',13.0:'r'}
    #colors = plt.cm.YlGnBu(np.linspace(0,1,len(phs)))
    for iph,pH in enumerate(sorted(phs)):
#        if pH%1: continue
        if pH not in pHcolors: continue
        pltdat=[]
        for pot in data['taf'][pH]:
            pltdat.append([pot,data['taf'][pH][pot]])
            #print(pH,pot,data['taf'][pH][pot])
        pltdat=np.array(pltdat)
        plt.plot(pltdat[:,0],pltdat[:,1]*1000,'-',color=pHcolors[pH])
    #plt.annotate(r'$\frac{60}{\Delta \beta}=200$',(min(pltdat[:,0])-0.05,max(pltdat[:,1])*1000/1.1),fontsize=18)
    #plt.annotate(r'Volmer-Heyrovsky',(max(pltdat[:,0]),max(pltdat[:,1])*1000/1.02),ha='right',fontsize=18,fontweight='bold')
    #plt.annotate(r'$\beta_V - \beta_H$=0.3',(max(pltdat[:,0])-0.05,max(pltdat[:,1])*1000/1.12),ha='right',fontweight='bold',fontsize=18)
    #plt.arrow(-0.7,125,-0.4,0,length_includes_head=True,head_width=4,head_length=0.1,visible=True)
    #plt.annotate(r'$\Delta$pH$\cdot$60mV',(-0.7,130),ha='left',bbox=dict(facecolor='w', edgecolor='w'),xytext=(-0.7,125),fontsize=18)
    plt.ylabel('Tafel slope [mV/dec]')
    plt.xlabel('U$_{SHE}$ [V]')
    plt.tight_layout()
    plt.savefig('Tafel_slopes.pdf')

    plt.close()
    #Plot current density at sepcific pH
    #read experimental result
    #plt.rcParams["figure.figsize"] = (5,5)
    fig,ax=plt.subplots(1,2,sharey=True,figsize=(10,4))
    plt.rcParams["figure.figsize"] = (10,3)
    expdat={}
    pHcolors={3.0:'b',7.0:'k',13.0:'r'}
    for iph,pH in enumerate(pHs_for_xy_plot):
        expdat[pH] = []
        readin = np.loadtxt('data_lei_COR_pH%i.txt'%pH)
        for potline in readin:
            #print(potline)
            if potline[3] > 1e-5:
                expdat[pH].append([potline[1],potline[3]])
        expdat[pH] = np.array(expdat[pH])

        ax[0].plot(expdat[pH][:,0],expdat[pH][:,1],'d--',markeredgecolor='k',color=pHcolors[pH],markersize=8,linewidth=2)


    #plot theoretical result
    plotdat={}
    #plt.plot(np.nan,np.nan,'d',label='Experiment',color='w',markeredgecolor='k')
    #plt.plot(np.nan,np.nan,'o',label='Theory',color='w',markeredgecolor='k')
    for iph,pH in enumerate(pHs_for_xy_plot):
    #    plt.plot(np.nan,np.nan,'s',label='pH %i'%pH,color=pHcolors[pH])
        plotdat[pH]=[]
        for pot in data['rate']:
            plotdat[pH].append([pot,data['rate'][pot][pH][0]])
#            plt.plot(pot,data['rate'][pot][pH],'o-',markeredgecolor='k')
        plotdat[pH]=np.array(sorted(plotdat[pH]))
    for pH in plotdat:
        ax[1].plot(plotdat[pH][:,0],plotdat[pH][:,1]*surface_ratio,'o-',markeredgecolor='k',color=pHcolors[pH],markersize=8)

    ax[0].set_yscale('log')
    ax[0].set_ylim([1e-6,10])
    ax[0].set_xlim([-1.65,-1.05])
    #plt.legend()
    ax[0].set_ylabel('j$_{CH_4}$ [mA/cm$^2$]')
    ax[0].set_xlabel('U$_{SHE}$ [V]')
    ax[1].set_xlabel('U$_{SHE}$ [V]')
    fig.tight_layout()
    fig.savefig('Current_comparison_with_experiment_different_panels.pdf')


    fig,ax=plt.subplots(1,1,figsize=(10,4))
    plt.rcParams["figure.figsize"] = (10,3)
    expdat={}
    pHcolors={3.0:'darkblue',7.0:'lightgreen',13.0:'brown'}

    #plot theoretical result only
    plotdat={}
    for iph,pH in enumerate(pHs_for_xy_plot):
        plotdat[pH]=[]
        for pot in data['rate']:
            plotdat[pH].append([pot,data['rate'][pot][pH][0]])
#            plt.plot(pot,data['rate'][pot][pH],'o-',markeredgecolor='k')
        plotdat[pH]=np.array(sorted(plotdat[pH]))
    for pH in plotdat:
        ax.plot(plotdat[pH][:,0],np.log10(plotdat[pH][:,1]*surface_ratio),'o-',markeredgecolor='k',color=pHcolors[pH],markersize=8)

    ax.annotate('96mV/dec',(-1.58,-0.9),fontsize=20,fontweight='bold',rotation=-12)
    ax.annotate('RLS: H-CO',(-1.59,-2.05),fontsize=20,fontweight='bold',rotation=-12)
    text=ax.annotate('pH 13',(-1.49,-2.8),fontsize=20,fontweight='bold',rotation=-12.5,color='brown')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                       patheffects.Normal()])
    text=ax.annotate('pH 7',(-1.32,-4.5),fontsize=20,fontweight='bold',rotation=-14,color='lightgreen')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                       patheffects.Normal()])
    text=ax.annotate('pH 3',(-1.22,-4.5),fontsize=20,fontweight='bold',rotation=-14,color='darkblue')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                       patheffects.Normal()])
    text=ax.annotate('28 mV/dec',(-1.395,-5.37),fontsize=18,fontweight='bold',rotation=-37,color='brown')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                       patheffects.Normal()])
    text=ax.annotate('RLS: CH-OH',(-1.425,-5.8),fontsize=18,fontweight='bold',rotation=-37,color='brown')
    text.set_path_effects([patheffects.Stroke(linewidth=1, foreground='black'),
                       patheffects.Normal()])
#    ax.set_yscale('log')
    ax.set_ylim([-6,1])
    ax.set_xlim([-1.6,-1.15])
    #plt.legend()
    ax.set_ylabel('log( j$_{CH_4}$ / mA/cm$^2$ )')
    ax.set_xlabel('U$_{SHE}$ / V')
    fig.tight_layout()
#    plt.show()
    #fig.savefig('Currents_from_theory.pdf')
    fig.savefig('Currents_from_theory.png',dpi=1000)


#    exit()
