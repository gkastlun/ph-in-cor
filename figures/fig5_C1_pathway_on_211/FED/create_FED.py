import sys,os
import numpy as np
sys.path.append('../../../tools_for_analysis')
from FED_tools import plot_FED_with_barrier,read_calculated_data
from intermediates_dict import *

facets= ['111','100','211','110']

def main():
    alldata=read_calculated_data(None,facets,True,pklfile='../../../calculated_energetics.pkl')

    plot_FED_with_barrier(ads_and_electron,'211',
                [['CO','CHO','CHOH','CH']],
                potentials=[3.15,2.9,2.65],pH=[13],ylim=[-2.7,1.5],
                view=False,
                annotate_intermediates=False,
                proton_donor='base',
                outdir='.',
                figsize=(15,6),
                fontsize=30,
                title='',
                outformat='png',
                yticks=np.arange(-3,2,1),
                )

    plot_FED_with_barrier(ads_and_electron,'211',
                [['CO','CHO','CHOH','CH']],
                potentials=[3.15],pH=[3,7,13],ylim=[-2.7,1.5],
                view=False,
                annotate_intermediates=False,
                proton_donor={13:'base',7:'base',3:'acid'},
                outdir='.',
                figsize=(15,6),
#                check_barriers_with_line=True,
                fontsize=30,
                yticks=np.arange(-3,2,1),
                outformat='png',
                title=''
                )

main()
