
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  19.8.2b1
 |___|_|             

User:   geokast@c114.nifl.fysik.dtu.dk
Date:   Tue Sep  8 07:53:12 2020
Arch:   x86_64
Pid:    34730
Python: 3.6.6
gpaw:   /home/cat/geokast/opt/gpaw/gpaw (4f0dd10c34)
_gpaw:  /home/cat/geokast/opt/gpaw/build/bin.linux-x86_64-skylake-el7-3.6/
        gpaw-python
ase:    /home/cat/geokast/opt/ase/ase (version 3.19.0b1-a19b3ca009)
numpy:  /home/modules/software/Python/3.6.6-intel-2018b/lib/python3.6/site-packages/numpy-1.15.0-py3.6-linux-x86_64.egg/numpy (version 1.15.0)
scipy:  /home/modules/software/Python/3.6.6-intel-2018b/lib/python3.6/site-packages/scipy-1.1.0-py3.6-linux-x86_64.egg/scipy (version 1.1.0)
libxc:  4.2.3
units:  Angstrom and eV
cores:  40

Preequilibrating the potential after manual change of ['potential']
Target electrode potential: 3.4000 V
Current number of Excess Electrons: 0.70000
Jellium size parameters:
  Lower boundary: 20.21089794833922
  Upper boundary: 23.21089794833922
------------

Initialize ...

Cu-setup:
  name: Copper
  id: f1c4d45d90492f1bbfdcb091e8418fdf
  Z: 29
  valence: 11
  core: 18
  charge: 0.0
  file: /home/modules/software/GPAW-setups/0.9.20000/Cu.PBE.gz
  compensation charges: gauss, rc=0.33, lmax=2
  cutoffs: 2.06(filt), 2.43(core),
  valence states:
                energy  radius
    4s(1.00)    -4.609   1.164
    4p(0.00)    -0.698   1.164
    3d(10.00)    -5.039   1.058
    *s          22.603   1.164
    *p          26.513   1.164
    *d          22.172   1.058

  Using partial waves for Cu as LCAO basis

O-setup:
  name: Oxygen
  id: 32ecd46bf208036f09c70b0ec9a88b78
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /home/modules/software/GPAW-setups/0.9.20000/O.PBE.gz
  compensation charges: gauss, rc=0.21, lmax=2
  cutoffs: 1.17(filt), 0.83(core),
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.688
    2p(4.00)    -9.029   0.598
    *s           3.251   0.688
    *p          18.182   0.598
    *d           0.000   0.619

  Using partial waves for O as LCAO basis

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /home/modules/software/GPAW-setups/0.9.20000/H.PBE.gz
  compensation charges: gauss, rc=0.15, lmax=2
  cutoffs: 0.85(filt), 0.53(core),
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  Using partial waves for H as LCAO basis

Reference energy: -1637298.927285

Spin-paired calculation

Occupation numbers:
  Fermi-Dirac: width=0.1000 eV

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 4e-08 eV^2
  Maximum number of iterations: 1000

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

12 k-points: 4 x 3 x 1 Monkhorst-Pack grid
6 k-points in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.12500000   -0.33333333    0.00000000          2/12
   1:     0.12500000    0.00000000    0.00000000          2/12
   2:     0.12500000    0.33333333    0.00000000          2/12
   3:     0.37500000   -0.33333333    0.00000000          2/12
   4:     0.37500000    0.00000000    0.00000000          2/12
   5:     0.37500000    0.33333333    0.00000000          2/12

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None
  Wavefunction extrapolation:
    Improved wavefunction reuse through dual PAW basis 

Eigensolver
   Davidson(niter=2, smin=None, normalize=True) 

Densities:
  Coarse grid: 32*56*136 grid
  Fine grid: 64*112*272 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.05
  Mixing with 5 old densities
  Damping of long wave oscillations: 100 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 64*112*272 grid
  Using the BEEF-vdW Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: Jacobi weighted FD solver with dielectric and 5 multi-grid levels
    Coarsest grid: 4 x 7 x 17 points
    Stencil: Weighted Finite Difference Operator
      6*3+1=19 point O(h^6) finite-difference Laplacian
      Finite-difference x-derivative with O(h^6) error (6 points)
      Finite-difference y-derivative with O(h^6) error (6 points)
      Finite-difference z-derivative with O(h^6) error (6 points)
    Max iterations: 1000
    Tolerance: 2.000000e-10
Dipole correction along z-axis
  Solvation:
    Cavity: <class 'gpaw.solvation.cavity.EffectivePotentialCavity'>
      Surface Calculator: <class 'gpaw.solvation.cavity.GradientSurface'>
      Volume Calculator: None
      Potential: <class 'gpaw.solvation.sjm.SJMPower12Potential'>
        atomic_radii: <function <lambda> at 0x2b3db8f9f048>
        u0: 0.18
        pbc_cutoff: 1e-06
        tiny: 1e-10
      temperature: 298.15
    Dielectric: <class 'gpaw.solvation.dielectric.LinearDielectric'>
      epsilon_inf: 78.36
    Interaction: <class 'gpaw.solvation.interactions.SurfaceInteraction'>
      subscript: surf
      surface_tension: 0.0011484376791625194

XC parameters: BEEF-vdW with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 97.10 MiB
  Calculator: 256.91 MiB
    Density: 11.45 MiB
      Arrays: 2.40 MiB
      Localized functions: 8.12 MiB
      Mixer: 0.92 MiB
    Hamiltonian: 23.17 MiB
      Arrays: 1.57 MiB
      XC: 0.00 MiB
      Poisson: 1.79 MiB
      vbar: 0.61 MiB
      Solvation: 19.20 MiB
    Wavefunctions: 222.30 MiB
      Arrays psit_nG: 155.59 MiB
      Eigensolver: 64.84 MiB
      Projections: 0.54 MiB
      Projectors: 1.33 MiB

Total number of cores used: 40
Parallelization over k-points: 2
Domain decomposition: 2 x 2 x 5

Number of atoms: 60
Number of atomic orbitals: 372
Number of bands in calculation: 281
Number of valence electrons: 460.7
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Solvation position-dependent initialization:
  Atomic radii for <class 'gpaw.solvation.sjm.SJMPower12Potential'>:
      0 Cu    1.30000
      1 Cu    1.30000
      2 Cu    1.30000
      3 Cu    1.30000
      4 Cu    1.30000
      5 Cu    1.30000
      6 Cu    1.30000
      7 Cu    1.30000
      8 Cu    1.30000
      9 Cu    1.30000
     10 Cu    1.30000
     11 Cu    1.30000
     12 Cu    1.30000
     13 Cu    1.30000
     14 Cu    1.30000
     15 Cu    1.30000
     16 Cu    1.30000
     17 Cu    1.30000
     18 Cu    1.30000
     19 Cu    1.30000
     20 Cu    1.30000
     21 Cu    1.30000
     22 Cu    1.30000
     23 Cu    1.30000
     24 Cu    1.30000
     25 Cu    1.30000
     26 Cu    1.30000
     27 Cu    1.30000
     28 Cu    1.30000
     29 Cu    1.30000
     30 Cu    1.30000
     31 Cu    1.30000
     32 Cu    1.30000
     33 Cu    1.30000
     34 Cu    1.30000
     35 Cu    1.30000
     36 O     1.52000
     37 H     1.20000
     38 H     1.20000
     39 O     1.52000
     40 H     1.20000
     41 H     1.20000
     42 O     1.52000
     43 H     1.20000
     44 H     1.20000
     45 O     1.52000
     46 H     1.20000
     47 H     1.20000
     48 O     1.52000
     49 H     1.20000
     50 H     1.20000
     51 O     1.52000
     52 H     1.20000
     53 H     1.20000
     54 O     1.52000
     55 H     1.20000
     56 H     1.20000
     57 O     1.52000
     58 H     1.20000
     59 H     1.20000
Density initialized from atomic densities
Creating initial wave functions:
  281 bands from LCAO basis set

       .---------------.  
      /|               |  
     / |               |  
    /  |               |  
   /   |               |  
  /    |               |  
 *     |               |  
 |     |               |  
 |     |               |  
 |     |               |  
 |     |       O       |  
 |     |         H   O |  
 |     H    H       HO |  
 |     |   HO H       H|  
 |     |   H     HOH   |  
 |  H  | H             |  
 |     |H   Cu  HCH    |  
 |    CuH  Cu  Cu      |  
 |     |               |  
 |   Cu| Cu CuCuCu     |  
 | Cu  |Cu  Cu         |  
 |   Cu|  Cu   CuCu    |  
 |Cu   | Cu  Cu        |  
 |  CuCu   Cu  Cu      |  
 | Cu  CuCu CuCu       |  
 |   Cu|               |  
 CuCu  |Cu  Cu         |  
 |    Cu               |  
 |Cu   |               |  
 |     |               |  
 |     |               |  
 |     .---------------.  
 |    /               /   
 |   /               /    
 |  /               /     
 | /               /      
 |/               /       
 *---------------*        

Positions:
   0 Cu     4.290614    1.325122   12.897588    ( 0.0000,  0.0000,  0.0000)
   1 Cu     4.289361    3.965226   12.903804    ( 0.0000,  0.0000,  0.0000)
   2 Cu     4.290596    6.611748   12.919454    ( 0.0000,  0.0000,  0.0000)
   3 Cu     4.290781    9.252537   12.897681    ( 0.0000,  0.0000,  0.0000)
   4 Cu     2.160121   -0.000689   12.256017    ( 0.0000,  0.0000,  0.0000)
   5 Cu     2.162783    2.643657   12.242335    ( 0.0000,  0.0000,  0.0000)
   6 Cu     2.167013    5.290901   12.286534    ( 0.0000,  0.0000,  0.0000)
   7 Cu     2.164608    7.932525   12.249343    ( 0.0000,  0.0000,  0.0000)
   8 Cu     0.030345    1.319996   11.558283    ( 0.0000,  0.0000,  0.0000)
   9 Cu     0.032223    3.968898   11.561397    ( 0.0000,  0.0000,  0.0000)
  10 Cu     0.031142    6.611614   11.561428    ( 0.0000,  0.0000,  0.0000)
  11 Cu     0.031247    9.256417   11.546815    ( 0.0000,  0.0000,  0.0000)
  12 Cu     4.318580    0.000000   10.763424    ( 0.0000,  0.0000,  0.0000)
  13 Cu     4.318580    2.644579   10.763424    ( 0.0000,  0.0000,  0.0000)
  14 Cu     4.318580    5.289159   10.763424    ( 0.0000,  0.0000,  0.0000)
  15 Cu     4.318580    7.933738   10.763424    ( 0.0000,  0.0000,  0.0000)
  16 Cu     2.159290    1.322290   10.000000    ( 0.0000,  0.0000,  0.0000)
  17 Cu     2.159290    3.966869   10.000000    ( 0.0000,  0.0000,  0.0000)
  18 Cu     2.159290    6.611448   10.000000    ( 0.0000,  0.0000,  0.0000)
  19 Cu     2.159290    9.256028   10.000000    ( 0.0000,  0.0000,  0.0000)
  20 Cu     0.000000    0.000000    9.236576    ( 0.0000,  0.0000,  0.0000)
  21 Cu     0.000000    2.644579    9.236576    ( 0.0000,  0.0000,  0.0000)
  22 Cu     0.000000    5.289159    9.236576    ( 0.0000,  0.0000,  0.0000)
  23 Cu     0.000000    7.933738    9.236576    ( 0.0000,  0.0000,  0.0000)
  24 Cu     4.318580    1.322290    8.473151    ( 0.0000,  0.0000,  0.0000)
  25 Cu     4.318580    3.966869    8.473151    ( 0.0000,  0.0000,  0.0000)
  26 Cu     4.318580    6.611448    8.473151    ( 0.0000,  0.0000,  0.0000)
  27 Cu     4.318580    9.256028    8.473151    ( 0.0000,  0.0000,  0.0000)
  28 Cu     2.159290    0.000000    7.709727    ( 0.0000,  0.0000,  0.0000)
  29 Cu     2.159290    2.644579    7.709727    ( 0.0000,  0.0000,  0.0000)
  30 Cu     2.159290    5.289159    7.709727    ( 0.0000,  0.0000,  0.0000)
  31 Cu     2.159290    7.933738    7.709727    ( 0.0000,  0.0000,  0.0000)
  32 Cu    -0.000000    1.322290    6.946303    ( 0.0000,  0.0000,  0.0000)
  33 Cu    -0.000000    3.966869    6.946303    ( 0.0000,  0.0000,  0.0000)
  34 Cu    -0.000000    6.611448    6.946303    ( 0.0000,  0.0000,  0.0000)
  35 Cu    -0.000000    9.256028    6.946303    ( 0.0000,  0.0000,  0.0000)
  36 O      6.282689    2.285428   17.210898    ( 0.0000,  0.0000,  0.0000)
  37 H      7.154971    2.437850   16.785308    ( 0.0000,  0.0000,  0.0000)
  38 H      5.781540    3.074209   16.959392    ( 0.0000,  0.0000,  0.0000)
  39 O      5.851742   -0.134774   15.856530    ( 0.0000,  0.0000,  0.0000)
  40 H      5.987300    0.753068   16.252315    ( 0.0000,  0.0000,  0.0000)
  41 H      5.959432   -0.018629   14.899353    ( 0.0000,  0.0000,  0.0000)
  42 O      3.091799   -0.184969   16.692396    ( 0.0000,  0.0000,  0.0000)
  43 H      4.003212   -0.167930   16.340365    ( 0.0000,  0.0000,  0.0000)
  44 H      2.713604    0.658789   16.391064    ( 0.0000,  0.0000,  0.0000)
  45 O      6.269524    7.695361   16.941526    ( 0.0000,  0.0000,  0.0000)
  46 H      7.200141    7.679432   16.633768    ( 0.0000,  0.0000,  0.0000)
  47 H      5.935606    8.529659   16.573809    ( 0.0000,  0.0000,  0.0000)
  48 O      5.842627    5.083859   15.986364    ( 0.0000,  0.0000,  0.0000)
  49 H      5.946509    6.038872   16.184269    ( 0.0000,  0.0000,  0.0000)
  50 H      5.787822    5.030337   15.018342    ( 0.0000,  0.0000,  0.0000)
  51 O      2.434081    7.713387   16.006079    ( 0.0000,  0.0000,  0.0000)
  52 H      2.627863    8.661153   16.166169    ( 0.0000,  0.0000,  0.0000)
  53 H      2.498808    7.609963   15.039968    ( 0.0000,  0.0000,  0.0000)
  54 O      3.222103    4.976498   17.120485    ( 0.0000,  0.0000,  0.0000)
  55 H      4.123166    4.995295   16.739901    ( 0.0000,  0.0000,  0.0000)
  56 H      2.840552    5.824582   16.842171    ( 0.0000,  0.0000,  0.0000)
  57 O      2.248173    2.607175   15.946547    ( 0.0000,  0.0000,  0.0000)
  58 H      2.581159    3.466985   16.287566    ( 0.0000,  0.0000,  0.0000)
  59 H      2.260252    2.694253   14.977400    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    yes    6.477870    0.000000    0.000000    32     0.2024
  2. axis:    yes    0.000000   10.578317    0.000000    56     0.1889
  3. axis:    no     0.000000    0.000000   25.000000   136     0.1838

  Lengths:   6.477870  10.578317  25.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1916

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  07:54:31                -2977.625359    4      55     
iter:   2  07:54:52  +0.10  -1.24  -2986.348174    35     47     
iter:   3  07:55:13  -0.44  -1.11  -2959.704756    36     42     
iter:   4  07:55:34  -0.58  -1.32  -2956.821104    4      28     
iter:   5  07:55:55  -0.40  -1.41  -2951.185167    4      33     
iter:   6  07:56:16  -1.15  -1.67  -2952.099842    3      31     
iter:   7  07:56:36  -0.73  -1.57  -2955.736749    34     15     
iter:   8  07:56:57  -0.66  -1.53  -2951.048830    33     39     
iter:   9  07:57:18  -0.68  -1.66  -2950.386626    35     30     
iter:  10  07:57:38  -1.48  -1.77  -2948.238344    4      24     
iter:  11  07:57:59  -2.19  -1.99  -2948.570609    3      20     
iter:  12  07:58:19  -1.13  -1.97  -2947.531717    35     22     
iter:  13  07:58:40  -2.12  -2.37  -2947.188645    3      25     
iter:  14  07:59:00  -2.69  -2.57  -2947.140786    3      14     
iter:  15  07:59:20  -3.01  -2.66  -2947.084886    3      13     
iter:  16  07:59:40  -3.20  -2.73  -2947.063831    3      9      
iter:  17  08:00:01  -3.65  -2.79  -2947.065096    2      10     
iter:  18  08:00:21  -4.15  -2.86  -2947.069170    3      9      
iter:  19  08:00:41  -3.21  -2.92  -2947.060132    3      17     
iter:  20  08:01:01  -4.14  -3.34  -2947.058382    2      6      
iter:  21  08:01:21  -3.76  -3.45  -2947.057038    3      8      
iter:  22  08:01:41  -4.35  -3.62  -2947.054614    3      6      
iter:  23  08:02:01  -4.79  -3.71  -2947.054932    2      4      
iter:  24  08:02:21  -5.25  -3.77  -2947.055795    2      4      
iter:  25  08:02:41  -5.47  -3.87  -2947.056105    2      4      
iter:  26  08:03:02  -5.56  -4.03  -2947.055180    2      5      
iter:  27  08:03:22  -5.25  -3.99  -2947.055191    2      4      
iter:  28  08:03:42  -5.99  -4.27  -2947.055972    2      3      
iter:  29  08:04:02  -6.31  -4.45  -2947.056212    2      3      
iter:  30  08:04:22  -6.54  -4.49  -2947.055742    2      3      
iter:  31  08:04:42  -6.39  -4.63  -2947.055600    2      3      
iter:  32  08:05:02  -6.77  -4.74  -2947.055440    2      2      
iter:  33  08:05:21  -6.87  -4.79  -2947.055610    2      3      
iter:  34  08:05:42  -7.09  -4.57  -2947.055532    2      3      
iter:  35  08:06:01  -7.77  -4.90  -2947.055521    2      1      

Converged after 35 iterations.

Dipole moment: (1.545269, 10.897119, 3.004984) |e|*Ang

Cavity Surface Area: 106.92170904174321
Cavity Volume: not calculated (no calculator defined)

Solvation Energy Contributions:
surf:            +0.122793
el (free):     -2947.399373
el (extrpol.): -2947.178314
el (free) is composed of:
Energy contributions relative to reference atoms: (reference = -1637298.927285)

Kinetic:       +382.909637
Potential:     -436.373775
External:        +0.000000
XC:            -2898.797444
Entropy (-ST):   -0.442119
Local:           +5.304328
--------------------------
Free energy:   -2947.276580
Extrapolated:  -2947.055521

Dipole-layer corrected work functions: 4.645830, 3.407635 eV

----------------------------------------------------------
Grand Potential Energy (E_tot + E_solv - mu*ne):
Extrpol:    -2944.670176465319
Free:    -2944.8912360938175
-----------------------------------------------------------
Grand canonical energy will be written into results

Fermi level: -4.02673

Showing only first 2 kpts
 Kpt  Band  Eigenvalues  Occupancy
  0   228     -4.45645    0.32886
  0   229     -4.08086    0.21070
  0   230     -3.99343    0.13917
  0   231     -3.66439    0.00867

  1   228     -4.18147    0.27484
  1   229     -4.04477    0.18166
  1   230     -4.04303    0.18022
  1   231     -3.81463    0.03569


Changing to simultaneous potential and geometry equilibration


Forces in eV/Ang:
  0 Cu   -0.00902    0.00027    0.01680
  1 Cu   -0.00074   -0.00340    0.00102
  2 Cu    0.00196   -0.00172    0.01202
  3 Cu   -0.00617    0.00034   -0.00559
  4 Cu   -0.00001    0.00681    0.00459
  5 Cu   -0.00255    0.01226    0.00055
  6 Cu   -0.00796   -0.00751    0.00517
  7 Cu   -0.00162   -0.00192   -0.00019
  8 Cu    0.00983    0.01197   -0.00710
  9 Cu    0.00553    0.00128   -0.01422
 10 Cu    0.00906   -0.01270   -0.01974
 11 Cu    0.00638   -0.00237    0.01323
 12 Cu    0.18512   -0.00038   -0.27552
 13 Cu    0.20583    0.00913   -0.30140
 14 Cu    0.18101    0.01572   -0.23774
 15 Cu    0.19961   -0.02722   -0.26811
 16 Cu    0.02486   -0.01065   -0.09159
 17 Cu    0.02470    0.03402   -0.05758
 18 Cu    0.02174   -0.02772   -0.05518
 19 Cu    0.03133    0.00357   -0.08483
 20 Cu   -0.10025    0.00523   -0.11276
 21 Cu   -0.09753    0.00268   -0.08342
 22 Cu   -0.10047    0.00010   -0.09395
 23 Cu   -0.09943   -0.00788   -0.09454
 24 Cu   -0.14137   -0.00172   -0.07078
 25 Cu   -0.14056   -0.00006   -0.06799
 26 Cu   -0.14195    0.00104   -0.06728
 27 Cu   -0.14101    0.00072   -0.07132
 28 Cu    0.01972   -0.00057    0.11000
 29 Cu    0.02131   -0.00112    0.11086
 30 Cu    0.01751    0.00132    0.11286
 31 Cu    0.02119    0.00032    0.11138
 32 Cu    0.09791   -0.00482    0.43602
 33 Cu    0.09656    0.00374    0.43649
 34 Cu    0.09753   -0.00266    0.43650
 35 Cu    0.09726    0.00377    0.43373
 36 O    -0.00836   -0.01910   -0.01541
 37 H    -0.00475   -0.00804   -0.00227
 38 H    -0.00034   -0.00407   -0.00046
 39 O     0.02124   -0.02728    0.01721
 40 H    -0.00353    0.00765    0.01211
 41 H     0.00527   -0.01724   -0.01865
 42 O     0.00850   -0.00150    0.04547
 43 H    -0.00861   -0.01077    0.02102
 44 H     0.00476   -0.03842    0.02020
 45 O     0.00515    0.01183   -0.00243
 46 H    -0.00183    0.02513    0.01198
 47 H     0.00232    0.01267    0.00535
 48 O    -0.00829   -0.01268   -0.00009
 49 H    -0.00988   -0.01042    0.02473
 50 H     0.01065    0.00260   -0.02238
 51 O     0.00949   -0.00335   -0.00051
 52 H     0.00066    0.00445    0.00639
 53 H     0.00067    0.00025    0.00526
 54 O    -0.00725    0.01286    0.01969
 55 H     0.00367    0.00778   -0.01375
 56 H     0.00151    0.00629   -0.01152
 57 O     0.02040   -0.00969   -0.01508
 58 H    -0.02981    0.00112    0.00301
 59 H     0.00699   -0.00308    0.00029

