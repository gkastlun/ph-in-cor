from ase.io import read,write
from ase.visualize import view

atoms=read('nowater.traj')
water=read('water_111.traj')

atoms.cell[2,2] = 25
atoms.center(axis=2)
atoms.translate([0,0,-4])
#view(atoms+water)
write('init.traj',atoms)
