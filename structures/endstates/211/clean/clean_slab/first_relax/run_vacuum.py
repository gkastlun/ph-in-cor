import os,sys
import numpy as np
from ase.visualize import view
from ase.optimize import BFGS as QN
from ase.build import molecule
from ase.io import read, write
from ase.units import mol, kJ, kcal, Pascal, m, Bohr
from sjm_neb_tools import check_lattice_constant
#Import solvation modules
from ase.data.vdw import vdw_radii
from gpaw import FermiDirac,MixerSum,Mixer,GPAW
from gpaw.utilities import h2gpts

#Solvent parameters

home = os.getcwd()
path_info = home.split('/')
facet,adsorbate = path_info[-3],path_info[-2]

if facet == '100':
    kpts=(3,4,1)
elif facet in ['111','211']:
    kpts=(4,3,1)
else:
    kpts=(4,4,1)

atoms = read('init.traj')
atoms = check_lattice_constant(atoms)

calc= GPAW(
             #Our new keywords
             poissonsolver={'dipolelayer':'xy'},  #Dipole correction

             #Standard gpaw stuff
             gpts = h2gpts(0.18,atoms.get_cell(),idiv=8),
             kpts = kpts,
             xc = 'BEEF-vdW',
             txt='relax_vac.txt',
             maxiter=1000,
             occupations = FermiDirac(0.1),
             mixer = Mixer(beta=0.05,
                   nmaxold=5,
                   weight=100.0),
             spinpol = False,

)
atoms.set_calculator(calc)

qn = QN(atoms,trajectory='relax_vac.traj',logfile='relax_vac.log')
qn.run(fmax = 0.05)

write('vac_relaxed.traj',atoms)
