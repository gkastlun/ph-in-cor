import os,sys
import numpy as np
from ase.visualize import view
from ase.optimize import BFGS as QN
from ase.build import molecule
from ase.io import read, write
from ase.units import mol, kJ, kcal, Pascal, m, Bohr
from sjm_neb_tools import check_lattice_constant
#Import solvation modules
from ase.data.vdw import vdw_radii
from gpaw.solvation.sjm import SJM,SJMPower12Potential
from gpaw import FermiDirac,MixerSum,Mixer
from gpaw.utilities import h2gpts
from gpaw.solvation import (
SolvationGPAW,
Power12Potential,
EffectivePotentialCavity,
LinearDielectric,
GradientSurface,
SurfaceInteraction
)

#Solvent parameters
u0=0.180 #eV
epsinf = 78.36 #dielectric constant of water at 298 K
gamma =18.4*1e-3 * Pascal*m
T=298.15
vdw_radii = vdw_radii.copy ()
vdw_radii[29]*=1.1
atomic_radii = lambda atoms: [vdw_radii[n] for n in atoms.numbers]

home = os.getcwd()
path_info = home.split('/')
facet,adsorbate = path_info[-3],path_info[-2]

if facet == '100':
    kpts=(3,4,1)
elif facet == '111':
    kpts=(4,3,1)

atoms = read('init.traj')
atoms = check_lattice_constant(atoms)
potentials = np.arange(2.9,2.4,-0.2)

calc= SJM(
             #Our new keywords
             doublelayer={'thickness':3},
             dpot=0.01,    #max deviation from set potential
             poissonsolver={'dipolelayer':'xy'},  #Dipole correction
             verbose = True,
             potential_equilibration_mode='sim',
             max_pot_deviation=0.05,
             ne = 1.5,

             #Standard gpaw stuff
             gpts = h2gpts(0.18,atoms.get_cell(),idiv=8),
             kpts = kpts,
             xc = 'BEEF-vdW',
#             txt='relax_%1.2f.txt'%(ne),
             maxiter=1000,
             occupations = FermiDirac(0.1),
             mixer = Mixer(beta=0.05,
                   nmaxold=5,
                   weight=100.0),
             spinpol = False,

             #Solvation stuff
             cavity = EffectivePotentialCavity (
                 effective_potential = SJMPower12Potential (atomic_radii, u0, H2O_layer=True),  #our cavity
                 temperature=T,
                 surface_calculator=GradientSurface ()),
             dielectric = LinearDielectric (epsinf=epsinf),
             interactions = [SurfaceInteraction (surface_tension=gamma)])

atoms.set_calculator(calc)

for potential in potentials:
    if 'Pot_%1.2f_relaxed_q_%1.2f.traj'%(potential,atoms.calc.ne) not in os.listdir():
        if 'pot_%1.2f.txt'%potential in os.listdir():
            if os.path.getsize('pot_%1.2f.txt'%potential):
                os.system('cp pot_%1.2f.txt pot_%1.2f_old.txt'%(potential,potential))
                os.system('cp pot_%1.2f.traj pot_%1.2f_old.traj'%(potential,potential))
        atoms.calc.set(txt='pot_%1.2f.txt'%potential)
        atoms.calc.set(potential=potential)
        qn = QN(atoms,trajectory='pot_%1.2f.traj'%potential,logfile='pot_%1.2f.log'%potential)
        qn.run(fmax = 0.05)

        write('Pot_%1.2f_relaxed_q_%1.2f.traj'%(potential,atoms.calc.ne),atoms)
