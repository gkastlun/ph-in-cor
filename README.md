The package contains all the structures and automated scripts for the paper https://pubs.acs.org/doi/pdf/10.1021/acscatal.1c05520 on the mechanism study of COR on the basis of SJM/DFT and Lei Wang's measurements at varying pH.

In the `structures` directory all reaction intermediate (noted as `endstates`) and minimum energy paths (noted as `barriers`) can be found. They are provided in the atomic simulation environment (ASE) trajectory format.

The structures can be parsed by using:
`python3 analyze_calc.py`

The script will create three files: `calculated_energetics.pkl`, `formation_energies_for_catmap.txt` and `formation_energies_for_catmap_potEn.txt` (already provided)

These files will be used for creating all the figures that apply energetics. All the directories of the respective figures contain their own README's. If ".sla" files are present the python created figures have been extended in scribus.

WARNING: Sadly, in order to parse the trajectory files properly a slightly changed ASE version from trunk is needed, as vanilla ASE does not allow readinig and writing of the number of electrons and potentials in the trajectory format. The necessary changes to ASE are:

In ase/calculators/singlepoint.py change line 25 from
```
if property in ['energy', 'magmom', 'free_energy']:
```
to
```
if property in ['energy', 'magmom', 'free_energy','ne','electrode_potential']:
```


and in ase/calculators/calculator line 98 exchange:
```
all_properties = ['energy', 'forces', 'stress', 'stresses', 'dipole',
                  'charges', 'magmom', 'magmoms', 'free_energy']
```
with
```
all_properties = ['energy', 'forces', 'stress', 'stresses', 'dipole',
                  'charges', 'magmom', 'magmoms', 'free_energy','ne','electrode_potential']
```

We also note that GPAW version 21.1.1b1 was used for the calculations and more recent version changed the names of `ne` and `electrode_potential`. However, ASE can still read the results if above's changes are made.

